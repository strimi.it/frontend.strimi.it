strimi_app.linkNsfw = function () {
    var nsfw = document.getElementsByClassName("link-nsfw")[0];
    if(nsfw) {
        nsfw = nsfw.options[nsfw.selectedIndex].value;
        if(nsfw === '1'){ return 'nsfw'; }
        return '';
    }
    return '';
};