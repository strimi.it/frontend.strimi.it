<?php

namespace AppBundle\Controller\User\Ranking;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RankingController extends Controller {

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {

        $accounts = $this->get('api.elastic.user.ranking')->get();

        return $this->render(
            'AppBundle:User/Ranking:index.html.twig',array(
                'accounts' => $accounts
            )
        );

    }

    /**
     * @param $package
     * @param $sortBy
     * @param $order
     * @return null
     */
    public function sortingByAction($package, $sortBy, $order){
        if(!$package || !$sortBy || !$order) { return null; }

        $code = 400;
        $accounts = $this->get('api.elastic.user.ranking')->get($sortBy, $package, $order, $id = $package);
        $accountsContent = null;
        $nextUri = null;

        if($accounts) {
            $code = 200;
            $nextUri = $this->generateUrl('user_ranking_ajax_sort_by', array(
                'package'   => $package + 40,
                'sortBy'  => $sortBy,
                'order'  => $order
            ));
            $accountsContent = $this->renderView(
                'AppBundle:User/Ranking:trContent.html.twig',
                array(
                    'accounts' => $accounts
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'accounts'    => $accountsContent,
                'next_url' => $nextUri
            ),
            $code
        );

    }

    /**
     * @param $package
     * @param $sortBy
     * @param $order
     * @return null
     */
    public function nextPackageAction($package, $sortBy, $order){
        if(!$package || !$sortBy || !$order) { return null; }

        $code = 400;
        $accounts = $this->get('api.elastic.user.ranking')->get($sortBy, $package, $order);
        $accountsContent = null;
        $nextUri = null;

        if($accounts) {
            $code = 200;
            $nextUri = $this->generateUrl('user_ranking_ajax_sort_by', array(
                'package'   => $package + 99,
                'sortBy'  => $sortBy,
                'order'  => $order
            ));
            $accountsContent = $this->renderView(
                'AppBundle:User/Ranking:trContent.html.twig',
                array(
                    'accounts' => $accounts
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'accounts'    => $accountsContent,
                'next_url' => $nextUri
            ),
            $code
        );

    }

}
