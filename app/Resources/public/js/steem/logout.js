strimi_app.logout = function () {
    var logout = document.getElementById("app-logout");
    if(logout){
        logout.addEventListener('click', function(e) {
            e.preventDefault();
            var account = strimi_app.authorization();
            var username = account.username;
            gtag('event', 'logout', { 'event_category': username });
            localStorage.removeItem("authorization");
            localStorage.removeItem("is_login");
            localStorage.removeItem("notification");
            localStorage.removeItem("notification_count");
            localStorage.removeItem("notification_replies");
            localStorage.removeItem("language");
            strimi_app.active_account();
            location.reload();
            return true;
        });
    }
};
strimi_app.logout();