strimi.language = function () {

    var change = function(data) {
        var elements = document.querySelectorAll('[data-lang]');
        for (var i = 0; i < elements.length; i++) {

            if (elements[i].classList.contains('lang-popover')){
                elements[i].setAttribute('data-content', _.get(data, elements[i].getAttribute('data-lang')));
            } else if (elements[i].classList.contains('lang-input')){
                elements[i].setAttribute('placeholder', _.get(data, elements[i].getAttribute('data-lang')));
            } else if (elements[i].classList.contains('textarea-lang')){
                elements[i].setAttribute('placeholder', _.get(data, elements[i].getAttribute('data-lang')));
            } else {
                elements[i].innerHTML = _.get(data, elements[i].getAttribute('data-lang'));
            }

        }
    };

    var removeActive = function () {
        var elements = document.querySelectorAll('.language-list');
        for (var i = 0; i < elements.length; i++) {
            elements[i].classList.remove('active');
        }
    };

    var setEnglish = function () {
        change( strimi.languageEN()[0] );
        localStorage.setItem("language", "en");
        removeActive();
        var langEn = document.querySelector('#lang-en');
        if(langEn) { langEn.className += ' active'; }
        strimi.momentTimezoneLanguage();

    };

    var setPolish = function () {
        change( strimi.languagePL()[0] );
        localStorage.setItem("language", "pl");
        removeActive();
        var langPl = document.querySelector('#lang-pl');
        if(langPl) { langPl.className += ' active'; }
        strimi.momentTimezoneLanguage();
        strimi.commentsCount();
    };
    
    var getUserLang = localStorage.getItem("language");
    if(getUserLang) {
        if(getUserLang === 'en') { setEnglish(); }
        else if(getUserLang === 'pl') { setPolish() }
    } else {
        var defaultLang = document.getElementsByTagName('html')[0].getAttribute('lang');
        if(defaultLang === 'en') { setEnglish(); }
        else if(defaultLang === 'pl') { setPolish() }
    }

/*
    var defaultLang = document.getElementsByTagName('html')[0].getAttribute('lang');
    var getUserLang = localStorage.getItem("language");
    removeActive();
    if(!getUserLang) {
        localStorage.setItem("language", defaultLang);
        setEnglish();
    } else {
        if(getUserLang === 'en') { setEnglish(); }
        else if(getUserLang === 'pl') { setPolish() }
    }
*/
    var btnLangEn = document.getElementById('lang-en');
    if(btnLangEn) {
        btnLangEn.onclick = function () {
            setEnglish();
            strimi.cashout();
        };
    }

    var btnLangPl = document.getElementById('lang-pl');
    if(btnLangPl) {
        btnLangPl.onclick = function () {
            setPolish();
            strimi.cashout();
        };
    }

};
strimi.language();

strimi.languageText = function (text) {
    if(!text) { return ''; }
    var lang;
    var data;
    var getUserLang = localStorage.getItem("language");
    if(getUserLang) {
        lang = getUserLang;
    } else {
        lang = document.getElementsByTagName('html')[0].getAttribute('lang');
    }
    if(lang === 'pl') {
        data = strimi.languagePL()[0];
    } else if(lang === 'en') {
        data = strimi.languageEN()[0];
    }
    return _.get(data, text);
};
strimi.languageText();