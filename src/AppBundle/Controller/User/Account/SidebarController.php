<?php

namespace AppBundle\Controller\User\Account;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SidebarController extends Controller {

    /**
     * @param $username
     * @param Request $request
     * @return null
     */
    public function leftAction($username, Request $request){
        if(!$username) { return null; }
        $code = 400;
        $account = $this->get('api.users.account')->get($username);
        $content = null;
        if($account) {
            $code = 200;
            $appRoute = $request->get('_route');
            $content = $this->renderView(
                'AppBundle:User/Account:sidebarInformation.html.twig',
                array(
                    'account' => $account,
                    'appRoute' => $appRoute
                )
            );
        }
        return $this->get('app.json.response')->renderDefault(
            $content,
            $code
        );
    }

}
