<?php

namespace ApiBundle\Services\Tags;

class PostsCount {

    protected $posts_counts;

    public function __construct($apiPostsDefaults){
        $this->posts_counts = $apiPostsDefaults;
    }

    /**
     * @param $tag
     * @return float|int|null
     */
    public function count($tag){
        if(!$tag) { return null; }
        $count = array(
            count($this->posts_counts->trendingByTag($tag)),
            count($this->posts_counts->createdByTag($tag)),
            count($this->posts_counts->hotByTag($tag)),
            count($this->posts_counts->promotedByTag($tag))
        );
        return array_sum($count);
    }


}