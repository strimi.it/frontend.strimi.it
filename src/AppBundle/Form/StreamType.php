<?php

namespace AppBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class StreamType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('description', TextareaType::class, array(
                'required' => true,
                'trim' => true,
                'attr' => array(
                    'length_min' => 10,
                    'length_max' => 5000,
                    'autocomplete' => 'off'
                )
            ))
        ;
    }

    public function getBlockPrefix() {
        return '';
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\EntityFake\Stream',
            'validation_groups' => 'comment_text',
            'csrf_protection' => true,
            'csrf_field_name' => '_token'
        ));
    }

}