<?php

namespace AppBundle\Controller\Tag\Ranking;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RankingController extends Controller {

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        return $this->render(
            'AppBundle:Tag/Ranking:index.html.twig',
            array(
                'tags' => $this->get('api.tags.lists')->lists()
            )
        );
    }

}
