strimi.AccountRankingNext = function () {
    var nextButton = document.getElementById('ranking-next-button');
    var loadingButton = document.getElementsByClassName('loading-oval-button')[0];
    if(nextButton){
        nextButton.onclick = function(){
            var status = nextButton.getAttribute('data-status');
            if(status == 0){
                $.ajax({
                    type: 'GET',
                    url: nextButton.getAttribute('data-url'),
                    async: true,
                    beforeSend: function() {
                        nextButton.setAttribute('data-status', 1);
                        nextButton.className += ' active';
                        loadingButton.classList.remove('hidden');
                    },
                    success: function(getResponse) {
                        if (getResponse.code == 200) {

                            var lastTR = document.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
                                lastTR = lastTR[lastTR.length - 1];
                                lastTR.insertAdjacentHTML('afterEnd', getResponse.data.accounts);

                            nextButton.setAttribute('data-url', getResponse.data.next_url);
                            nextButton.setAttribute('data-status', 0);
                            nextButton.classList.remove('active');
                            loadingButton.classList.className += ' hidden';
                            strimi.imageLazyload();
                            strimi.language();

                        } else if (getResponse.code == 400) {
                        }
                    },
                    error: function() {
                    }
                });
            }
        };
    }
};
if(_config.route === 'user_ranking') {
    strimi.AccountRankingNext();
}
