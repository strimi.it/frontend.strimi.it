strimi.embedVideoPlay = function () {

    $('.video-play').click(function() {
        var element  = $(this),
            hash  = element.data('hash'),
            type  = element.data('type'),
            thumb = $(this).find('.video-content-thumbnail'),
            embed = $(this).find('.video-embed');

        console.log(
            hash,
            type,
            thumb
        );

        thumb.addClass("hidden");
        embed.addClass("active");

        if(type === 'm.youtube.com' || type === 'youtube.com' || type === 'www.youtube.com' || type === 'youtu.be') {
            embed.append(
                '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/' + hash + '?rel=0&amp;showinfo=1&autoplay=1&iv_load_policy=3" frameborder="0" allowfullscreen></iframe>'
            ).show();
        } else if(type === 'vimeo.com') {
            embed.append(
                '<iframe src="https://player.vimeo.com/video/'+ hash +'?color=008B5D&;autoplay=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
            ).show();
        } else if(type === 'liveleak.com') {
            embed.append(
                '<iframe src="https://www.liveleak.com/ll_embed?f='+ hash +'&autostart=true" width="490" height="300" frameborder="0" allowfullscreen allowTransparency="true"></iframe>'
            ).show();
        } else if(type === 'twitch.com') {
            embed.append(
                '<iframe src="https://player.twitch.tv/?video=v'+ hash +'&autoplay=true" frameborder="0" allowfullscreen="true" scrolling="no" width="490" height="300"></iframe>'
            ).show();
        } else if(type === 'dailymotion.com' || type === 'dai.ly') {
            embed.append(
                '<iframe src="//www.dailymotion.com/embed/video/'+ hash +'?autoplay=true" frameborder="0" allowfullscreen></iframe>'
            ).show();
        } else if(type === 'www.facebook.com' || type === 'www.fb.com' || type === 'facebook.com' || type === 'fb.com') {
            embed.append(
                '<iframe src="https://www.facebook.com/plugins/video.php?href=https://www.facebook.com/facebook/videos/'+ hash +'/&show_text=false&autoplay=true&mute=0" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>'
            ).show();
        } else if(type === 'soundcloud') {
            embed.append(
                '<iframe src="//w.soundcloud.com/player/?url=https://api.soundcloud.com/tracks/'+ hash +'&amp;auto_play=true&amp;hide_related=true&amp;show_comments=false&amp;show_user=true&amp;show_reposts=false&amp;visual=true" scrolling="no" frameborder="no"></iframe>'
            ).show();
        }
    });
};
if(_config.route === 'app_post') {
    strimi.embedVideoPlay();
}