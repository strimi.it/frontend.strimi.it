<?php

namespace AppBundle\Controller\Stream;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SingleController extends Controller {

    /**
     * @param $category
     * @param $author
     * @param $permlink
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($category, $author, $permlink) {
        if(!$category || !$author || !$permlink) {
            return $this->redirect($this->generateUrl('stream_homepage'), 301);
        }
        $post = $this->get('api.post.single')->get($author, $permlink);
        $isValid = $this->get('api.posts.defaults')->isStrimiPostValid($post);
        if(!$isValid) { return $this->redirect($this->generateUrl('stream_homepage'), 301); }
        $account = $this->get('api.users.account')->get($author);
        return $this->render(
            'AppBundle:Stream/Single:index.html.twig', array(
                'comment'=> $post,
                'account' => $account,
                'tag_name' => $category
            )
        );
    }

}
