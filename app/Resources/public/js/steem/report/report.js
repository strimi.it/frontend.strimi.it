strimi_app.reportPost = function () {

    if(!strimi_app.is_login()) { return; }

    var account = strimi_app.authorization();
    var username = account.username,
        wif = account.private_keys.posting_wif;

    var modalReportPost = document.getElementById('modalReportPost');
    var sliders = modalReportPost.getElementsByClassName('slider-horizontal');
    if(sliders) {
        for (var i = 0; i < sliders.length; i++) {

            if(sliders[i] !== sliders.length) {
                sliders[i].remove()
            }

        }
    }

    var removeVote = function (status) {
        var title = document.getElementsByClassName('vote-strong')[0];
        var titleRemove = document.getElementsByClassName('vote-strong-remove')[0];
        var slider = document.getElementsByClassName('slider-content')[0];
        var submit = document.getElementById('submit-vote-lang');
        if(status === 'active') {
            title.className += ' hidden';
            titleRemove.className += ' active';
            slider.className += ' hidden';
            submit.innerText = strimi.languageText('global.post.votes.slider.undo');
        } else if('remove') {
            if(title.classList.contains('hidden')) {
                title.classList.remove('hidden');
                titleRemove.classList.remove('active');
                slider.classList.remove('hidden');
            }
            submit.innerText = strimi.languageText('global.post.votes.slider.submit');
        }

    };

    var sliderReport = new Slider("#report-slider", {
        formatter: function(value) {
            document.getElementsByClassName('report-value')[0].innerHTML = value + '%';
        },
        min: 1,
        max: 100,
        ticks: [1, 25, 50, 75, 100],
        ticks_labels: ['0%', '25%', '50%', '75%', '100%'],
        value: 100,
        tooltip: "hide",
        step: 1,
        ticks_snap_bounds: 1
    });
    sliderReport.refresh();

    var submit = document.getElementById('submit-report');
    var voteStatus = false;

    $('.post-report').click(function() {
        setTimeout(function(){ sliderReport.refresh(); }, 100);
        submit.setAttribute('data-id', $(this).attr('data-id'));
    });

    var loading = function (submitButton, loadingStatus) {
        var loadingButton = document.getElementById('submit-report-loading');
        if (loadingStatus === 'start') {
            submitButton.className += ' active-loading';
            loadingButton.classList.remove('hidden');
        } else if (loadingStatus === 'end') {
            submitButton.classList.remove('active-loading');
            loadingButton.className += ' hidden';
        }
    };


    submit.onclick = function () {
        var power = document.getElementsByClassName('report-power-slider')[0].value;
        var id = this.getAttribute('data-id');
        var voteContent = document.getElementsByClassName('vote-post-' + id)[0];
        var short = document.getElementById('post-single-' + id);

        var isComment = false;
        if(short && short.hasAttribute('data-short-permlink')) {
            short = short.getAttribute('data-short-permlink');
        } else {
            short = document.getElementById('comment-single-'+ id).getAttribute('data-short-permlink');
            isComment = true;
        }

        short = short.split('/');
        var author = short[0];
        var permlink = short[1];
        var weight = strimi_app.voteWeight('-' + power);
        loading(submit, 'start');

        console.log( weight );

        strimi_app.broadcast.vote(wif, username, author, permlink, weight, function(error, result) {
            if(result && result !== 'undefined') {
                strimi_app.accountVote();
                $('#modalReportPost').modal('hide');
                if(!isComment) {
                    if(voteStatus === 'true') {
                        voteContent.classList.remove('active');
                        voteContent.removeAttribute('vote-status');
                    } else {
                        voteContent.className += ' active';
                        voteContent.setAttribute('vote-status', true);
                    }
                }
                gtag('event', 'report', { 'event_category': username, 'event_label': weight + ' - ' + permlink });
            } else if (error && error !== 'undefined') {
                console.log( error );
            }
            loading(submit, 'end');
        });

    };

};
strimi_app.reportPost();