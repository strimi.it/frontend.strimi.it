<?php

namespace ApiBundle\Services\Users;

class PostsNext {

    protected $defaults;
    protected $json_parser;
    protected $api_connection;
    protected $memcached;
    public function __construct($defaults, $jsonParser, $apiConnection, $cacheMemcached){
        $this->defaults = $defaults;
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
    }

    /**
     * @param $account
     * @param $author
     * @param $permlink
     * @return array|null
     */
    public function content($account, $author, $permlink){
        if(!$account || !$author || !$permlink) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'users/' . $account . '/posts', [
                'query' => [
                    'startPermlink' => $permlink,
                    'startAuthor'   => $author,
                    'limit'         => 10
                ]
            ]);

            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result'];
            if(is_array($result)) {
                foreach($result as &$singleObject) {
                    if(isset($singleObject['json_metadata']) && !empty($singleObject['json_metadata'])) {
                        $singleObject = $this->defaults->parserContent($singleObject);
                    }
                }
            }
            array_shift($result);
            return $result;

        }catch(\Exception $ex){
            //Process the exception
        }
    }

}