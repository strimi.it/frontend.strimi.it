strimi.imageStreamLazy = function () {

    function insertAfter(el, referenceNode) {
        referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
    }

    var images = document.getElementsByClassName('image-strimi');
    var image_host = _config.image_host;

    if(images) {

        for (var i = 0; i < images.length; i++) {

            var image = images[i];
            if(!image.hasAttribute('data-status') || image.getAttribute('data-status') === 0) {
                image.className += ' magnific-image';
                image.setAttribute('data-status', 1);

                var id = image.getAttribute('id');
                var src = image.getAttribute('data-src');
                var width = image.getAttribute('data-width');
                var height = image.getAttribute('data-height');
                var type = image.getAttribute('data-type');
                var domain = image.getAttribute('data-domain');
                var uniq = image.getAttribute('data-uniq');

                var routeStream = [ 'stream_homepage', 'stream_post' ];
                if(routeStream.includes(_config.route) && Number(width) > 400) {
                    image.setAttribute('src', image_host + '400x512/' + src );
                } else if (_config.route === 'app_post') {
                    image.setAttribute('src', image_host + '0x0/' + src );
                }

                /* Create image container */
                var imageContainer = document.createElement('div');
                imageContainer.className = "image-container";
                imageContainer.id = "image-container-" + uniq;
                insertAfter(imageContainer, image);

                /*  Move image */
                var fragment = document.createDocumentFragment();
                fragment.appendChild(image);
                imageContainer.appendChild(fragment);

                /* Create source image container */
                var sourceElement = document.createElement('div');
                sourceElement.className = "image-source";
                imageContainer.appendChild(sourceElement);

                /* Add source name */
                var sourceName = document.createElement('span');
                sourceName.className = "source";
                sourceName.innerText = 'źródło:';
                sourceElement.appendChild(sourceName);

                /* Add source link */
                var sourceLink = document.createElement('a');
                sourceLink.setAttribute('href', src);
                sourceLink.setAttribute('target', '_blank');
                sourceLink.setAttribute('rel', 'nofallow');
                sourceLink.innerText = domain;
                sourceElement.appendChild(sourceLink);

            }

        }
    }

};

var routeList = [ 'app_post', 'stream_homepage', 'stream_post' ];
if(routeList.includes(_config.route)) {
    strimi.imageStreamLazy();
}
