<?php

namespace ApiBundle\Services\Users;

class Transfer {

    protected $api_connection;
    protected $json_parser;
    protected $posts;
    protected $vest;
    protected $properties;
    protected $steem;
    protected $price;
    protected $sbd;
    protected $paginator;
    protected $vest_to_sp;
    protected $memcached;
    public function __construct($apiConnection, $jsonParser, $posts, $vest, $properties, $steem, $price, $sbd, $paginator, $vestToSteem, $memcached){
        $this->api_connection = $apiConnection;
        $this->json_parser = $jsonParser;
        $this->posts = $posts;
        $this->vest = $vest;
        $this->properties = $properties;
        $this->steem = $steem;
        $this->price = $price;
        $this->sbd = $sbd;
        $this->paginator = $paginator;
        $this->vest_to_sp = $vestToSteem;
        $this->memcached = $memcached;
    }

    /**
     * @param $amount
     * @return array|null|string
     */
    private function amountDetect($amount){
        if(!$amount) { return null; }
        try {
            if (strpos($amount, ' STEEM') !== false) {
                return [
                    'steem',
                    $this->price->parser($this->steem->clean($amount))
                ];
            } elseif(strpos($amount, ' SBD') !== false) {
                return [
                    'sbd',
                    $this->price->parser($this->sbd->parser($amount))
                ];
            } else {
                return '?';
            }
        }catch(\Exception $ex){ }
    }

    /**
     * @param $name
     * @return array|null
     */
    private function content($name){
        if(!$name) { return null; }
        try{

            $response = $this->api_connection->init()->request('GET', 'users/' . $name . '/rewards');
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result']['accounts'][$name]['transfer_history'];

            //comment_benefactor_reward ?
            //@ToDo sprawdzić inne pola z in_array()
            //@ToDo czym się różni rewards od history?

            $type = [
                'author_reward', //done
                'curation_reward', //done
                'transfer', //done
                'claim_reward_balance', //done
                'delegate_vesting_shares',
                'return_vesting_delegation',
                'fill_vesting_withdraw',
                'account_update',
                'account_create_with_delegation',
                'delete_comment',
                'comment_options',
                'custom_json',
            ];

            $properties = $this->properties->get();

            $data = [];
            foreach($result as $item) {
                if( $item[1]['op'][0] === 'author_reward') {
                    $data[] = [
                        'type'      => 'author_reward',
                        'timestamp' => $item[1]['timestamp'],
                        'author'    => $item[1]['op'][1]['author'],
                        'permlink'  => $item[1]['op'][1]['permlink'],
                        'payout'    => [
                            'sbd' => $this->price->parser($this->sbd->parser($item[1]['op'][1]['sbd_payout']), 3),
                            'steem' => $this->price->parser($this->steem->clean($item[1]['op'][1]['steem_payout']), 3),
                            'steem_power' => $this->vest_to_sp->parser($properties, $item[1]['op'][1]['vesting_payout'], 3)
                        ]
                    ];
                } elseif( $item[1]['op'][0] === 'curation_reward') {
/*
                    $data[] = [
                        'type'      => 'curation_reward',
                        'timestamp' => $item[1]['timestamp'],
                        'curator'   => $item[1]['op'][1]['curator'],
                        'author'    => $item[1]['op'][1]['comment_author'],
                        'permlink'  => $item[1]['op'][1]['comment_permlink'],
                        'reward'    => [
                            'steem_power',
                            $this->vestToSteemPower($properties, $item[1]['op'][1]['reward'])
                        ],
                    ];
*/
                } elseif( $item[1]['op'][0] === 'transfer' ){
                    $amount = $this->amountDetect($item[1]['op'][1]['amount']);
                    $data[] = [
                        'type'      => 'transfer',
                        'timestamp' => $item[1]['timestamp'],
                        'from'      => $item[1]['op'][1]['from'],
                        'to'        => $item[1]['op'][1]['to'],
                        'amount'   => [$amount[0], $amount[1]],
                        'memo'      => $item[1]['op'][1]['memo']
                    ];
                } elseif( $item[1]['op'][0] === 'claim_reward_balance' ){
                    $data[] = [
                        'type'      => 'claim_reward_balance',
                        'timestamp' => $item[1]['timestamp'],
                        'account'      => $item[1]['op'][1]['account'],
                        'reward'    => [
                            'sbd' => $this->price->parser($this->sbd->parser($item[1]['op'][1]['reward_sbd']), 3),
                            'steem' => $this->price->parser($this->steem->clean($item[1]['op'][1]['reward_steem']), 3),
                            'steem_power' => $this->vest_to_sp->parser($properties, $item[1]['op'][1]['reward_vests'], 3)
                        ]
                    ];
                }

            }

            return array_reverse($data);

        }catch(\Exception $ex){
            //Process the exception
        }

    }

    /**
     * @param $key
     * @param $account
     * @return null
     */
    private function contentCache($key, $account){
        if(!$key || !$account) { return null; }
        $transfers = $this->memcached->get($key);
        if(!$transfers) {
            $value = $this->content($account);
            $this->memcached->set($key, $value, 30, 'seconds');
        }
        return $this->memcached->get($key);
    }

    /**
     * @param $name
     * @return null
     */
    public function get($name) {
        if(!$name) { return null; }
        return $this->paginator->getPackage(
            $this->contentCache('account_' . md5($name) . '_transfer', $name)
        );
    }

    /**
     * @param $name
     * @param $package
     * @return null
     */
    public function getNext($name, $package){
        if(!$name || !$package) { return null; }
        return $this->paginator->getPackageNext(
            $this->contentCache('account_' . md5($name . $package) . '_transfer_package', $name), $package
        );
    }

}