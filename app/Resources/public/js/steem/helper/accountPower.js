strimi_app.headerAccountPower = function () {
    if(!strimi_app.is_login()) { return; }

    var accountPower = document.getElementById('account-power-count');
    if (accountPower) {
        var account = strimi_app.authorization();
        var username = account.username;
        var uri = accountPower.getAttribute('data-url').replace("strimi", username);
        var votingPower = document.getElementById('account-voting-power-count');
        var bandwidthUsed = document.getElementById('account-bandwidth-used-count');
        $.ajax({
            type: 'GET',
            url: uri,
            async: true,
            beforeSend: function () {
            },
            success: function (response) {
                if (response.code == 200) {
                    if( response.data.voting_power !== null){
                        votingPower.innerHTML = response.data.voting_power.voting_power_percent[0] + '%';
                    }
                    // if (response.data.account_bandwidth !== null) {
                    //     bandwidthUsed.innerHTML = response.data.account_bandwidth.percent.remaining + '%';
                    // }
                } else if (response.code == 400) {
                }
            },
            error: function () {
            }
        });

    }

};
strimi_app.headerAccountPower();