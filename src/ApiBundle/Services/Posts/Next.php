<?php

namespace ApiBundle\Services\Posts;

class Next {

    protected $defaults;
    protected $json_parser;
    protected $api_connection;
    protected $memcached;

    public function __construct($defaults, $jsonParser, $apiConnection, $cacheMemcached){
        $this->defaults = $defaults;
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
    }

    /**
     * @param $author
     * @param $permlink
     * @param $path
     * @param null $tag
     * @return array|null
     */
    public function content($author, $permlink, $path, $tag = null){
        if(!$author || !$permlink) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'posts/' . $path, [
                'query' => [
                    'startAuthor'   => $author,
                    'startPermlink' => $permlink,
                    'tag'           => $tag,
                    'limit'         => 35
                ]
            ]);

            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result'];
            if(is_array($result)) {
                foreach($result as &$singleObject) {
                    if(isset($singleObject['json_metadata']) && !empty($singleObject['json_metadata'])) {
                        $singleObject = $this->defaults->parserContent($singleObject);
                    }
                }
            }
            array_shift($result);
            return $result;

        }catch(\Exception $ex){
            //Process the exception
        }
    }


}