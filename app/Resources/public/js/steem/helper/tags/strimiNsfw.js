strimi_app.strimiNsfw = function () {
    var nsfw = document.getElementsByClassName("strimi-nfs-input")[0];
    if(nsfw) {
        nsfw = nsfw.options[nsfw.selectedIndex].value;
        if(nsfw === '1'){ return ['strimi-nsfw', 'nsfw']; }
        return '';
    }
    return '';
};