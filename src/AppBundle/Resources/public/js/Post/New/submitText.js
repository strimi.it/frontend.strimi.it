strimi.postNewTextSubmit = function () {
    var loading = function () {
        var content = document.getElementsByClassName('page-content-new-text-form-submit')[0];
        var url = content.getAttribute('data-url');
        $.ajax({
            type: 'GET',
            url: url,
            async: true,
            beforeSend: function () {
            },
            success: function (response) {
                if (response.code == 200) {
                    content.innerHTML = response.data.form;
                    strimi.language();
                    strimi.inputLimitText();
                    strimi_app.postSubmitText();
                    strimi.tagAutocomplete();
                    strimi.userAutocomplete();
                    strimi.remarkable();
                    strimi.autosize();
                    strimi.inputLimitText();
                    strimi.markdownHashtag();
                    strimi.markdownAccount();
                    //submit();
                } else if (response.code == 400) {
                }
            },
            error: function () {
            }
        });
    };
    loading();

};
if(_config.route === 'app_post_new_text') {
    strimi.postNewTextSubmit();
}
