strimi.tablesorterDefault = function () {
    $(".tablesorter").tablesorter();
};

strimi.tablesorterBySortValue = function () {
    $(".tablesorter").tablesorter({
        textExtraction: function(node) {
            var attr = $(node).attr('data-sort-value');
            if (typeof attr !== 'undefined' && attr !== false) {
                return attr;
            }
            return $(node).text();
        }
    });
};

if(_config.route === 'tag_ranking') {
    //strimi.tablesorterBySortValue();
}