<?php

namespace AppBundle\Controller\Post\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller {

    /**
     * @param $tag
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function trendingAction($tag) {
        if(!$tag) { return $this->redirect($this->generateUrl('app_homepage'), 301); }
        if($tag == $this->getParameter('stream')['category'] || $tag == 'nsfw' | $tag == 'strimi-nsfw') {
            return $this->redirect($this->generateUrl('app_homepage'), 301);
        }
        $result = $this->get('api.posts.defaults')->trendingByTag($tag);
        return $this->render(
            'AppBundle:Post/List:index.html.twig', array(
                'tag_name' => $tag,
                'posts' => $result
            )
        );

    }

    /**
     * @param $tag
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createdAction($tag){
        if(!$tag) { return $this->redirect($this->generateUrl('app_homepage'), 301); }
        if($tag == $this->getParameter('stream')['category'] || $tag == 'nsfw' | $tag == 'strimi-nsfw') {
            return $this->redirect($this->generateUrl('app_homepage'), 301);
        }
        $result = $this->get('api.posts.defaults')->createdByTag($tag);
        return $this->render(
            'AppBundle:Post/List:index.html.twig', array(
                'tag_name' => $tag,
                'posts' => $result
            )
        );
    }

    /**
     * @param $tag
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function hotAction($tag){
        if(!$tag) { return $this->redirect($this->generateUrl('app_homepage'), 301); }
        if($tag == $this->getParameter('stream')['category'] || $tag == 'nsfw' | $tag == 'strimi-nsfw') {
            return $this->redirect($this->generateUrl('app_homepage'), 301);
        }
        $result = $this->get('api.posts.defaults')->hotByTag($tag);
        return $this->render(
            'AppBundle:Post/List:index.html.twig', array(
                'tag_name' => $tag,
                'posts' => $result
            )
        );
    }

    /**
     * @param $tag
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function promotedAction($tag){
        if(!$tag) { return $this->redirect($this->generateUrl('app_homepage'), 301); }
        if($tag == $this->getParameter('stream')['category'] || $tag == 'nsfw' | $tag == 'strimi-nsfw') {
            return $this->redirect($this->generateUrl('app_homepage'), 301);
        }
        $result = $this->get('api.posts.defaults')->promotedByTag($tag);
        return $this->render(
            'AppBundle:Post/List:index.html.twig', array(
                'tag_name' => $tag,
                'posts' => $result
            )
        );
    }

}
