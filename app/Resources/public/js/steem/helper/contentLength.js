strimi_app.contentLength = function (content, length) {
    if(!content || !length) { return ''; }
    if (content.length > length) {
        content = content.substring(content.length - length, content.length);
    }
    return content;
};