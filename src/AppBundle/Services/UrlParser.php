<?php

namespace AppBundle\Services;

use DOMDocument;
use Symfony\Component\Config\Definition\Exception\Exception;

class UrlParser
{
    /**
     * @param null $url
     * @return array
     */
    public function prepareResponse($url = null)
    {

        try {
            $result = [];
            // if image, return basic data
            $image = $this->checkIfImage($url);
            if ($image) {
                $size = 0;
                try {
                    $image = getimagesize($url);
                    $imagick = new \Imagick($url);
                    $size = $imagick->getImageLength();

                } catch (Exception $e) {
                    // TODO: monolog exceptions
                }

                $domain = parse_url($url, PHP_URL_HOST);
                $domain = $this->getDomain($domain);

                //$result['params'] = $image;
                $result['width'] = $image[0];
                $result['height'] = $image[1];
                $result['size'] = $size;
                $result['type'] = ['image', $image['mime']];
                $result['title'] = '';
                $result['description'] = '';
                $result['source'] = $url;
                $result['domain'] = $domain;
                return $result;
            }


            $domain = parse_url($url, PHP_URL_HOST);
            try {
                $domain = $this->getDomain($domain);
            } catch (Exception $e) {

            }
            $metaProperties = $this->getMetaProperties($url);

            // hack na soundcloud, zaputania curlowe/filegetcontenst powodowaly bledy 400 Bad Request
            if (strpos($url, 'soundcloud.com')) {
                $options = array(
                    'http' => array(
                        'ignore_errors' => true,
                        'user_agent' => $_SERVER['HTTP_USER_AGENT']
                    )
                );

                $context = stream_context_create($options);
                $doc = new \DOMDocument();
                @$doc->loadHTML(file_get_contents($url, false, $context));
                $title = $doc->getElementsByTagName("title")->item(0)->nodeValue;
                $link_id = '';
                $startsWith = 'soundcloud://sounds:';
                $description = '';
                foreach ($doc->getElementsByTagName("meta") as $meta) {
                    if ($meta->hasAttribute('property') && $meta->getAttribute('property') === 'twitter:app:url:googleplay') {
                        $link_id = $meta->getAttribute('content');
                        if (substr($link_id, 0, strlen($startsWith))) {
                            $link_id = substr($link_id, strlen($startsWith), strlen($link_id));
                            break;
                        }
                    }
                    if ($meta->hasAttribute('name') && $meta->getAttribute('name') === 'description') {
                        $description = $meta->getAttribute('content');
                    }
                }

                $result['id'] = $link_id;
                $result['title'] = $title;
                $result['description'] = $description;
                $result['type'] = ['video', 'soundcloud'];
                $result['thumb'] = $this->getThumbnail($metaProperties, $domain);
                $result['domain'] = $domain;
                $result['source'] = $url;


            } else {

                $tags = get_meta_tags($url);
                if (empty($tags['title'])) {
                    $title = isset($metaProperties['og:title']) ? $metaProperties['og:title'] : '';
                } else {
                    $title = $tags['title'];
                }
                if (empty($tags['description'])) {
                    $description = isset($metaProperties['og:description']) ? $metaProperties['og:description'] : '';
                } else {
                    $description = $tags['description'];
                }


                $result = [
                    'type' => ['link'],
                    'thumb' => $this->getThumbnail($metaProperties, $domain),
                    'domain' => $domain,
                    'source' => $url,
                    'title' => $title,
                    'description' => $description,

                ];
            }


            $fbArray = ['www.facebook.com', 'www.fb.com', 'facebook.com', 'fb.com'];
            $checkIfFacebook = $this->arrayStrstr($domain, $fbArray);

            if ($checkIfFacebook && $this->strposa($url, ['/post/', '/posts/', '/photo/', '/photos/'])) {
                $result['type'][1] = 'social_fb';
            }

            // warunki dla linków z facebooka, niekoniecznie musza byc one filmami, dlatego jest strpos
            $ifFbMovie = null;
            if ($checkIfFacebook && strpos($url, 'videos')) {
                $checkIfMovie = $checkIfFacebook;
            } else {
                $checkIfMovie = $this->arrayStrstr($domain, ['vimeo.com', 'm.youtube.com', 'youtube.com', 'youtu.be', 'dailymotion.com', 'dai.ly', 'liveleak.com', 'twitch.com', 'twitch.tv']);
            }

            if ($checkIfMovie) {
                $id = $this->getVideoId($checkIfMovie, $url);
                $result['type'] = [
                    'video',
                    $checkIfMovie //viemo, facebook, liveleak, dailymotion, twitch
                ];
                $result['id'] = $id;
            }

            return $result;
            //return json_encode($result);
        } catch (Exception $e) {


        }
    }

    /**
     * Find and prepare thumb url
     *
     * @param null $metaProperties
     * @param string $domain
     * @return string
     */
    private function getThumbnail($metaProperties = null, $domain = null)
    {
        $thumb = '';
        $scheme = '';
        if (isset($metaProperties['og:image'])) {
            $thumb = $metaProperties['og:image'];

            // workaround for og:image without domain
            if ($domain !== null && !strpos($thumb, $domain)) {
                $thumb = $domain . $thumb;
            }

            $scheme = parse_url($metaProperties['og:image'], PHP_URL_SCHEME);
            $scheme = $scheme ? '' : 'http://';
        }
        return $scheme . $thumb;
    }

    private function arrayStrstr($haystack, $needles)
    {
        foreach ($needles as $needle) {
            $str = strstr($haystack, $needle);
            if ($str) {
                return $str;
            }
        }
        return false;
    }

    /**
     * @param $url
     * @return array
     */
    private function getMetaProperties($url)
    {
        $page_content = $this->getHTTPSCurl($url);
        $metaData = [];
        try {
            $dom_obj = new DOMDocument();
            // set error level
            $internalErrors = libxml_use_internal_errors(true);
            // hack for bad encoding by DOMdocument()->loadHTML
            $dom_obj->loadHTML(mb_convert_encoding($page_content, 'HTML-ENTITIES', 'UTF-8'));
            // Restore error level
            libxml_use_internal_errors($internalErrors);
            $meta_val = null;

            foreach ($dom_obj->getElementsByTagName('meta') as $meta) {
                $metaData[$meta->getAttribute('property')] = $meta->getAttribute('content');
            }
        } catch (Exception $e) {

        }

        return $metaData;

    }

    /**
     * @param $service
     * @param $url
     * @return bool|string
     */
    private function getVideoId($service, $url)
    {
        $id = '';

        if (in_array($service, ['m.youtube.com', 'youtube.com', 'youtu.be'])) {
            $id = $this->youtubeIdFromUrl($url);
        }
        if (in_array($service, ['dailymotion.com', 'dai.ly'])) {
            $id = strtok(basename($url), '_');
        }
        if ($service == 'vimeo.com') {
            if (preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $url, $output_array)) {
                $id = $output_array[5];
            }
        }
        if ($service == 'liveleak.com') {
            $content = $this->getHTTPSCurl($url);
            preg_match("/https:\/\/www\.liveleak\.com\/ll_embed\?f\=((\w)+)/", $content, $m);
            $id = $m[1];
        }
        if ($service == 'twitch.com') {
            $id = substr($url, strrpos($url, '/') + 1);
        }

        if (in_array($service, ['www.facebook.com', 'www.fb.com', 'facebook.com', 'fb.com'])) {
            $parsedUrl = parse_url($url);
            $id = explode('/', $parsedUrl['path']);
            $id = array_filter($id);
            $id = end($id);
        }

        return $id;

    }

    /**
     * @param $url
     * @return bool
     */
    private function youtubeIdFromUrl($url)
    {
        $pattern =
            '%^# Match any youtube URL
    (?:https?://)?  # Optional scheme. Either http or https
    (?:www\.)?      # Optional www subdomain
    (?:             # Group host alternatives
      youtu\.be/    # Either youtu.be,
    | youtube\.com  # or youtube.com
      (?:           # Group path alternatives
        /embed/     # Either /embed/
      | /v/         # or /v/
      | .*v=        # or /watch\?v=
      )             # End path alternatives.
    )               # End host alternatives.
    ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
    ($|&).*         # if additional parameters are also in query string after video id.
    $%x';
        $result = preg_match($pattern, $url, $matches);
        if (false !== $result) {
            return $matches[1];
        }
        return false;
    }

    private function getHTTPSCurl($getUrl)
    {
        if (empty($getUrl)) {
            return false;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $getUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "spider");
        $getResult = curl_exec($ch);
        curl_close($ch);
        return $getResult;
    }


    private function getDomain($domain, $debug = false)
    {
        $original = $domain = strtolower($domain);
        if (filter_var($domain, FILTER_VALIDATE_IP)) {
            return $domain;
        }
        $debug ? print('<strong style="color:green">&raquo;</strong> Parsing: ' . $original) : false;
        $arr = array_slice(array_filter(explode('.', $domain, 4), function ($value) {
            return $value !== 'www';
        }), 0); //rebuild array indexes
        if (count($arr) > 2) {
            $count = count($arr);
            $_sub = explode('.', $count === 4 ? $arr[3] : $arr[2]);
            $debug ? print(" (parts count: {$count})") : false;
            if (count($_sub) === 2) // two level TLD
            {
                $removed = array_shift($arr);
                if ($count === 4) // got a subdomain acting as a domain
                {
                    $removed = array_shift($arr);
                }
                $debug ? print("<br>\n" . '[*] Two level TLD: <strong>' . join('.', $_sub) . '</strong> ') : false;
            } elseif (count($_sub) === 1) // one level TLD
            {
                $removed = array_shift($arr); //remove the subdomain
                if (strlen($_sub[0]) === 2 && $count === 3) // TLD domain must be 2 letters
                {
                    array_unshift($arr, $removed);
                } else {
                    // non country TLD according to IANA
//                    $tlds = array(
//                        'aero',
//                        'arpa',
//                        'asia',
//                        'biz',
//                        'cat',
//                        'com',
//                        'coop',
//                        'edu',
//                        'gov',
//                        'info',
//                        'jobs',
//                        'mil',
//                        'mobi',
//                        'museum',
//                        'name',
//                        'net',
//                        'org',
//                        'post',
//                        'pro',
//                        'tel',
//                        'travel',
//                        'xxx',
//                    );

                    $tlds = file('https://data.iana.org/TLD/tlds-alpha-by-domain.txt', FILE_IGNORE_NEW_LINES);


                    if (count($arr) > 2 && in_array($_sub[0], $tlds) !== false) //special TLD don't have a country
                    {
                        array_shift($arr);
                    }
                }
                $debug ? print("<br>\n" . '[*] One level TLD: <strong>' . join('.', $_sub) . '</strong> ') : false;
            } else // more than 3 levels, something is wrong
            {
                for ($i = count($_sub); $i > 1; $i--) {
                    $removed = array_shift($arr);
                }
                $debug ? print("<br>\n" . '[*] Three level TLD: <strong>' . join('.', $_sub) . '</strong> ') : false;
            }
        } elseif (count($arr) === 2) {
            $arr0 = array_shift($arr);
            if (strpos(join('.', $arr), '.') === false
                && in_array($arr[0], array('localhost', 'test', 'invalid')) === false) // not a reserved domain
            {
                $debug ? print("<br>\n" . 'Seems invalid domain: <strong>' . join('.', $arr) . '</strong> re-adding: <strong>' . $arr0 . '</strong> ') : false;
                // seems invalid domain, restore it
                array_unshift($arr, $arr0);
            }
        }
        $debug ? print("<br>\n" . '<strong style="color:gray">&laquo;</strong> Done parsing: <span style="color:red">' . $original . '</span> as <span style="color:blue">' . join('.', $arr) . "</span><br>\n") : false;
        return join('.', $arr);
    }


    /**
     * @param null $url
     * @return bool
     */
    public function checkIfImage($url = null)
    {

        $params = array('http' => array(
            'method' => 'HEAD'
        ));
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);
        if (!$fp)
            return false;  // Problem with url

        $meta = stream_get_meta_data($fp);
        if ($meta === false) {
            fclose($fp);
            return false;  // Problem reading data from url
        }

        if (isset($meta["wrapper_data"]) && is_array($meta["wrapper_data"])) {
        $wrapper_data = $meta["wrapper_data"];
            foreach (array_keys($wrapper_data) as $hh) {
                if (substr($wrapper_data[$hh], 0, 19) == "Content-Type: image" && $this->validImageExtension($url)) // strlen("Content-Type: image") == 19
                {
                    fclose($fp);
                    return true;
                }
            }
        }

        fclose($fp);
        return false;

    }

    /**
     * @param $url
     * @return bool
     */
    private function validImageExtension($url)
    {
        $url_headers = get_headers($url, 1);

        if (isset($url_headers['Content-Type'])) {
            $type = $url_headers['Content-Type'];
            if (is_array($type)) {
                $result = array_values(preg_grep('/^image.*/', $type));
                $type = $result[0];
            }

            $type = strtolower($type);

            $valid_image_type = [
                'image/png' => '1',
                'image/jpg' => '1',
                'image/jpeg' => '1',
                //'image/jpe' => '1',
                'image/gif' => '1',
                //'image/tif' => '1',
                //'image/tiff' => '1',
                //'image/svg' => '1',
                //'image/ico' => '1',
                //'image/icon' => '1',
                //'image/x-icon' => '1'
            ];


            if ($valid_image_type[$type]) {
                return true;
            }
        }

        return false;

    }

    /**
     * String position with array of needles
     * @param $haystack
     * @param array $needles
     * @param int $offset
     * @return bool|mixed
     */
    private function strposa($haystack, $needles = array(), $offset = 0)
    {
        $chr = array();
        foreach ($needles as $needle) {
            $res = strpos($haystack, $needle, $offset);
            if ($res !== false) $chr[$needle] = $res;
        }
        if (empty($chr)) return false;
        return min($chr);
    }
}