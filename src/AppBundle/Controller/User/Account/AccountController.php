<?php

namespace AppBundle\Controller\User\Account;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AccountController extends Controller {

    /**
     * @param $username
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($username) {

        if(!$username) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $account = $this->get('api.users.account')->get($username);
        if(!$account) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $posts = $this->get('api.users.account.posts')->get($account['name']);
        return $this->render(
            'AppBundle:User/Account:index.html.twig',array(
                'account' => $account,
                'posts'   => $posts
            )
        );

    }

    /**
     * @param $account
     * @param $author
     * @param $permlink
     * @return null
     */
    public function nextPostsAction($account, $author, $permlink) {
        if(!$account || !$author | !$permlink) { return null; }

        $code = 400;
        $posts = $this->get('api.users.account.posts.next')->content($account, $author, $permlink);
        $postsContent = null;
        $nextUri = null;

        if($posts) {
            $code = 200;
            $lastPost =  end($posts);
            $nextUri = $this->generateUrl('user_account_ajax_posts_next', array(
                'account'  => $account,
                'author'   => $lastPost['author'],
                'permlink' => $lastPost['permlink']
            ));

            $postsContent = $this->renderView(
                'AppBundle:Post/List:posts.html.twig',
                array(
                    'posts' => $posts,
                    'appRoute' => null
                )
            );

        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'posts'    => $postsContent,
                'next_url' => $nextUri
            ),
            $code
        );
    }

}
