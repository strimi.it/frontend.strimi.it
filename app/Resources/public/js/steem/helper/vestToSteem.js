strimi_app.vestToSteem = function (vesting_shares) {
    if(!vesting_shares) { return ''; }



    strimi_app.api.getDynamicGlobalProperties(function(error, result) {

        if (result && result !== 'undefined') {

            var total_vesting_shares = result.total_vesting_shares.replace(' VESTS', '');
            var total_vesting_fund_steem = result.total_vesting_fund_steem.replace(' STEEM', '');

            var steemPower = total_vesting_fund_steem * (vesting_shares / total_vesting_shares);



            console.log('steemPower: ' + steemPower);


        } else if (error && error !== 'undefined') {

            console.log(error);

        }


    });

};
