var strimi_app = {};
//strimi_app.config = {};

strimi_app.auth = steem.auth;
strimi_app.api = steem.api;
strimi_app.utils = steem.utils;
strimi_app.broadcast = steem.broadcast;
strimi_app.config_steem = steem.config;
strimi_app.api.setOptions({ url: 'https://api.steemit.com' });
setInterval(function(){ strimi_app.api.setOptions({ url: 'https://api.steemit.com' }); }, 30000);



strimi_app.config = {
    base: {
        name: 'strimi',
        version: '2.0',
        compilation: 'strimi/2.0',
        wss: 'wss://notifications.strimi.it/',
        fee: {
            minimum: 1,
            percent: 5 //5%
        }
    },
    domain: {
        domain_pl: [
            'dv.strimi.pl',
            'staging.strimi.pl',
            'strimi.pl'
        ],
        domain_en: [
            'dv.strimi.it',
            'staging.strimi.it',
            'strimi.it'
        ]
    },
    beneficiaries: [
        [0, {
            beneficiaries: [
                { account: 'strimi', weight: 1500 } //(500 = 15%)
            ]
        }]
    ]
};

strimi_app.keyRoles = [
    'owner',
    'active',
    'posting',
    'memo'
];