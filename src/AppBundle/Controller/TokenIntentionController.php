<?php

namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TokenIntentionController extends Controller {

    public function getAction() {
        return $this->get('app.json.response')->renderDefault(
            $this->get('app.token.itention')->get(),
            200
        );
    }

}
