<?php

namespace AppBundle\Services;

class ArrayPaginator {

    public function getPackage($data = [], $from = 0, $limit = 35) {
        if(count($data) <= 0) {
            return null;
        }
        return array_slice($data, $from, $limit);
    }

    public function getPackageNext($data = [], $from = 0, $limit = 35) {
        array_shift($data);
        return array_slice($data, $from, $limit);
    }

}