strimi.markdownHashtag = function () {
    $('.markdown-content').on('input', function() {
        var data = $(this).attr('data-markdown-preview-content');
        var previewContainer = document.getElementById(data);
        if(previewContainer) {
            var previewValue = previewContainer.innerHTML;
            previewContainer.innerHTML = previewValue.replace(/(\B#\w*[-a-z]+\w*)/ig, "<a href='#' class='hash_tag'>$1</a>");
        }
    });
};
strimi.markdownHashtag();