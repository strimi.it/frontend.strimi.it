strimi.cookiesInfo = function () {
    var create = function (name, value, days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
        document.cookie = name + "=" + value + expires + "; path=/";
    };
    var read = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    };
    var check = function () {
      if( read('cookies_accepted') != 'T' ){
          var message_container = document.createElement('div');
            message_container.id = 'cookies-message-container';
          var msg;
          var agree;
          var trans;
          var getUserLang = localStorage.getItem("language");
          if(getUserLang === 'en') {trans = strimi.languageEN()[0] }
          else if(getUserLang === 'pl') { trans = strimi.languagePL()[0] }
          msg = _.get(trans, 'global.cookiesInfo.text');
          agree = _.get(trans, 'global.cookiesInfo.button');
          message_container.innerHTML = '<div id="cookies-message"><span data-lang="global.cookiesInfo.text">' + msg + '</span> <a href="#" id="accept-cookies-checkbox" name="accept-cookies" data-lang="global.cookiesInfo.button">' + agree + '</a></div>';
          document.body.appendChild(message_container);
      }
    };
    window.onload = check();
    var closeCookieInfo = function () {
        var cookiesCheckbox = document.getElementById('accept-cookies-checkbox');
        if(cookiesCheckbox) {
            cookiesCheckbox.addEventListener('click', function(){
                create('cookies_accepted', 'T', 365);
                document.getElementById('cookies-message-container').removeChild(document.getElementById('cookies-message'));
            }, false);
        }
    };
    closeCookieInfo();
};
strimi.cookiesInfo();