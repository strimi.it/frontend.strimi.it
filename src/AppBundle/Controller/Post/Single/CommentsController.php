<?php

namespace AppBundle\Controller\Post\Single;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CommentsController extends Controller {

    /**
     * @param $author
     * @param $permlink
     * @param $sorting
     * @param Request $request
     * @return mixed
     */
    public function indexAction($author, $permlink, $sorting, Request $request) {
        if(!$author || !$permlink || !$sorting) { echo '404'; }

        $code = 400;
        $count = 0;
        $comments = $this->get('api.post.single.comments')->get($author, $permlink, $sorting);
        $commentsContent = null;
        $appRoute = $request->get('_route');

        if($comments) {
            $code = 200;
            $count = count($comments);
            $commentsContent = $this->renderView(
                'AppBundle:Comment:onList.html.twig',
                array(
                    'comments' => $comments,
                    'appRoute' => $appRoute
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'comments'  => $commentsContent,
                'count'     => $count
            ),
            $code
        );

    }

}
