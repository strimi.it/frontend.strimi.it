<?php

namespace ApiBundle\Services\Users;

class FallowCount {

    protected $api_connection;
    protected $json_parser;
    public function __construct($apiConnection, $jsonParser){
        $this->api_connection = $apiConnection;
        $this->json_parser = $jsonParser;
    }

    /**
     * @param $name
     * @return array|null
     */
    public function count($name){
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', '/users/follow', [
                'query' => [
                    'userName' => $name
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents());
            if(!$result) { return null; }
            return array(
                'follower' => $result['result']['follower_count'],
                'following' => $result['result']['following_count']
            );
        }catch(\Exception $ex){
            //Process the exception
        }

    }


}