<?php

namespace AppBundle\Services;
use Symfony\Component\HttpFoundation\RequestStack;

class Blocking {

    protected $requestStack;
    protected $root_dir;
    protected $user_blocking_file;
    public function __construct(RequestStack $requestStack, $root_dir, $user_blocking_file){
        $this->requestStack = $requestStack;
        $this->root_dir = $root_dir;
        $this->user_blocking_file = $user_blocking_file;
    }

    public function isUserBanned($name){
        if(empty($name)) { return false; }
        $uri = $this->root_dir->getProjectDir() . '/src/AppBundle/Resources/public/json/' . $this->user_blocking_file;
        $getJson = file_get_contents($uri);
        $getJson = json_decode($getJson);
        if(in_array($name, $getJson)) {
            return true;
        }
        return false;
    }

}