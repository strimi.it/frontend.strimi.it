<?php

namespace AppBundle\Controller\Post\Homepage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class IndexController extends Controller {

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        return $this->render(
            'AppBundle:Post/List:index.html.twig', array(
                'posts' =>  $this->get('api.posts.defaults')->trending()
            )
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createdAction(){
        return $this->render(
            'AppBundle:Post/List:index.html.twig', array(
                'posts' => $this->get('api.posts.defaults')->created()
            )
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function hotAction(){
        return $this->render(
            'AppBundle:Post/List:index.html.twig', array(
                'posts' => $this->get('api.posts.defaults')->hot()
            )
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function promotedAction(){
        return $this->render(
            'AppBundle:Post/List:index.html.twig', array(
                'posts' => $this->get('api.posts.defaults')->promoted()
            )
        );
    }

}
