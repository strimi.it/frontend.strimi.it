strimi.popover = function () {
    $('[data-toggle="popover"]').popover({
        placement: "bottom",
        html: true
    });
    $('[data-toggle="popover-cashout"]').popover({
        container: 'body',
        placement: "bottom",
        html: true

    });
};
strimi.popover();