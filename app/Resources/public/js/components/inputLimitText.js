strimi.inputLimitText = function (){

    var lists = document.querySelectorAll('.limit-character');
    if(lists) {
        Array.prototype.forEach.call(lists, function(elements, index) {

            /* Count Characeter */
            var element = elements;

            /* Set Status */
            //var status = element.getAttribute('data-status');
            ///var statusData = element.hasAttribute("data-status");

                element.setAttribute('data-status', 1);

                /* Get Min, Max value */
                var length_min = element.getAttribute('length_min');
                var length_max = element.getAttribute('length_max');

                /* Create Counter Content */
                var counterContainer = element.getAttribute('id') + '_counter';
                var containerCurrrentCount = element.getAttribute('id') + '_current_count';
                var containerMinCount = element.getAttribute('id') + '_min_count';
                var containerMaxCount = element.getAttribute('id') + '_max_count';

                /* Counter div */
                var divCounter = document.createElement('div');
                divCounter.id = counterContainer;
                divCounter.className = 'character-container';
                element.parentNode.insertBefore(divCounter, element.nextSibling);

                /* Current counter div */
                var divCurrentCount = document.createElement('div');
                divCurrentCount.className = 'current-count';
                divCurrentCount.id = containerCurrrentCount;
                divCurrentCount.innerText = 0;
                document.getElementById(counterContainer).appendChild(divCurrentCount);

                /* Slash div */
                var divSpace = document.createElement('div');
                divSpace.className = 'space-count';
                divSpace.innerText = '/';
                document.getElementById(counterContainer).appendChild(divSpace);

                /* Min counter div */
                var divMinCount = document.createElement('div');
                divMinCount.className = 'min-count';
                divMinCount.id = containerMinCount;
                divMinCount.innerText = length_min;
                //document.getElementById(counterContainer).appendChild(divMinCount);

                /* Max counter div */
                var divMaxCount = document.createElement('div');
                divMaxCount.className = 'max-count';
                divMaxCount.id = containerMaxCount;
                divMaxCount.innerText = length_max;
                document.getElementById(counterContainer).appendChild(divMaxCount);

                /* Listener Counter */
                $(document).on('change keyup paste', element, function() {
                    var text = element.value;
                    if (text.length >= length_max){
                        element.value =  text.substr(0, length_max);
                    }
                    var listenerCounter = document.getElementById(containerCurrrentCount);
                    if(listenerCounter) {
                        listenerCounter.innerText = text.length;
                    }
                });

                /* Show/Hide Element */
                // element.focusin = function () {
                //     document.getElementById(counterContainer).className += ' active';
                // };
                // element.onfocusout = function () {
                //     document.getElementById(counterContainer).classList.remove('active');
                // };



        });
    }

};
strimi.inputLimitText();