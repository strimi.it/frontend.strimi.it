strimi_app.authorization = function () {
    if(!strimi_app.is_login) { console.log('Authorization - Authentication problem'); return false; }
    var data = localStorage.getItem('authorization');
    if(data) {
        var itemsAuth = document.getElementsByClassName('strimi_app');
        if(itemsAuth) {
            for (var i = 0; i < itemsAuth.length; i++) {
                itemsAuth[i].className = itemsAuth[i].getAttribute('class').replace('strimi_app', 'strimi_app_active');
            }
        }
        var account = JSON.parse(atob(data));
        return {
            "username": account.username,
            "public_keys": {
                "posting_wif": atob(account.public_keys.posting_wif),
                "active_wif": atob(account.public_keys.active_wif)
            },
            "private_keys": {
                "posting_wif": atob(account.private_keys.posting_wif)
            }
        };
    }
};