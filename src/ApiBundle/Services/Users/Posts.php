<?php

namespace ApiBundle\Services\Users;

class Posts {

    protected $api_connection;
    protected $json_parser;
    protected $posts;
    protected $memcached;

    public function __construct($apiConnection, $jsonParser, $posts, $memcached){
        $this->api_connection = $apiConnection;
        $this->json_parser = $jsonParser;
        $this->posts = $posts;
        $this->memcached = $memcached;
    }

    /**
     * @param $name
     * @return null
     */
    private function content($name){
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'users/' . $name . '/posts', [
                'query' => [
                    'limit'   => 35
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result'];
            if(is_array($result)) {
                foreach ($result as &$singleObject) {
                    if (isset($singleObject['json_metadata']) && !empty($singleObject['json_metadata'])) {
                        $singleObject = $this->posts->parserContent($singleObject);
                    }
                }
            }
            return $result;
        }catch(\Exception $ex){
            //Process the exception
        }

    }

    /**
     * @param $key
     * @param $account
     * @return null
     */
    private function contentCache($key, $account){
        if(!$key || !$account) { return null; }
        $posts = $this->memcached->get($key);
        if(!$posts) {
            $value = $this->content($account);
            $this->memcached->set($key, $value, 1, 'minutes');
        }
        return $this->memcached->get($key);
    }

    /**
     * @param $name
     * @return null
     */
    public function get($name){
        if(!$name) { return null; }

        //return $this->content($name);
        return $this->contentCache('account_' . md5($name) . '_posts', $name);
    }


}