strimi.prettyNotification = function (title, message, type) {
    if(!title || !message || !type) { return null; }
    //type: success, error, info, warning
    $.toast({
        heading: title,
        text: message,
        showHideTransition: 'slide',
        hideAfter: 5000,
        stack: 4,
        allowToastClose: true,
        icon: type
    });
};

