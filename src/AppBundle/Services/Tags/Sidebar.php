<?php

namespace AppBundle\Services\Tags;

class Sidebar {

    protected $parserSBD;
    protected $jsonParser;
    protected $apiConnection;
    protected $cacheMemcached;
    public function __construct($parserSBD, $jsonParser, $apiConnection, $cacheMemcached){
        $this->parser_sbd = $parserSBD;
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
    }

}