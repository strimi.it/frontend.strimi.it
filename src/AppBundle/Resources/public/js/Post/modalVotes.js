$('#modalPostVotesList').on('shown.bs.modal', function () {
    $('.lazy_load').each(function(){
        var src = $(this).data('src');
        if (typeof src !== typeof undefined && src !== false) {
            var img = $(this);
                img.attr('style', 'background-image: url("' + img.data('src') + '");');
                img.removeAttr('data-src');
        }
    });
});