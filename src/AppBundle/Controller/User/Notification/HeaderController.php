<?php

namespace AppBundle\Controller\User\Notification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HeaderController extends Controller {

    public function headerAction($username, Request $request){
        if(!$request->isXmlHttpRequest() || !$username ) { die(); };

        $notifications = $this->get('api.users.notification')->get($username);
        $data = $this->renderView(
            'AppBundle:Notification:lists.html.twig',
            array(
                'notifications' => $notifications
            )
        );

        return $this->get('app.json.response')->renderDefault(
            $data,
            200
        );

    }

}