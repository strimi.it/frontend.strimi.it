<?php

namespace ApiBundle\Services\Users;

class vestToSteemPower {

    protected $vest;
    protected $steem;
    protected $price;
    public function __construct($vest, $steem, $price){
        $this->vest = $vest;
        $this->steem = $steem;
        $this->price = $price;
    }

    /**
     * @param $properties
     * @param $vest
     * @param bool $price
     * @return float|int|null
     */
    public function parser($properties, $vest, $price = true){
        if(!$properties || !$vest) { return null; }
        $vest = $this->vest->clean($vest);
        $vesting_shares = $this->vest->clean($properties['total_vesting_shares']);
        $vesting_steem = $this->steem->clean($properties['total_vesting_fund_steem']);
        if($price) {
            return $this->price->parser($vesting_steem * ($vest / $vesting_shares), 3);
        }
        return $vesting_steem * ($vest / $vesting_shares);

    }

}