strimi.nextVotes = function () {
    var nextButton = document.getElementById('votes-next-button');
    var loadingButton = document.getElementsByClassName('loading-oval-button')[0];
    if(nextButton){
        nextButton.onclick = function(){
            var status = nextButton.getAttribute('data-status');
            if(status == 0){
                $.ajax({
                    type: 'GET',
                    url: nextButton.getAttribute('data-url'),
                    async: true,
                    beforeSend: function() {
                        nextButton.setAttribute('data-status', 1);
                        nextButton.className += ' active';
                        loadingButton.classList.remove('hidden');
                    },
                    success: function(getResponse) {
                        if (getResponse.code == 200) {
                            $('.comments').append(getResponse.data.votes);
                            nextButton.setAttribute('data-url', getResponse.data.next_url);
                            nextButton.setAttribute('data-status', 0);
                            nextButton.classList.remove('active');
                            loadingButton.classList.className += ' hidden';
                            strimi.imageLazyload();
                            strimi.momentTimezone();
                            strimi.language();
                        } else if (getResponse.code == 400) {
                        }
                    },
                    error: function() {
                    }
                });
            }
        };
    }
};
if(_config.route === 'user_account_votes') {
    strimi.nextVotes();
}
