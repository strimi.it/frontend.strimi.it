<?php

namespace AppBundle\Services\Pushover;
use AppBundle\Services\Pushover\Pushover as Pushover;

class Send {

    protected $app_key;
    protected $user_key;
    public function __construct($app_key, $user_key){
        $this->app_key = $app_key;
        $this->user_key = $user_key;
    }

    public function send($type, $url, $route = null, $host = null){
        if(empty($type) || empty($url)) { return ''; }
        $push = new Pushover();
        $push->setToken($this->app_key);
        $push->setUser($this->user_key);
        $push->setTitle($type);
        $push->setMessage(
            'Route: ' . $route . '  ' .
            'Uri: ' . $url . '  ' .
            'HOST: ' . $host . '  ' .
            'Time: ' . date('d.m.Y, G:i:s')
        );
        $push->setUrl($url);
        $push->setUrlTitle('');

        $push->setDevice('iPhone');
        $push->setPriority(2);
        $push->setRetry(60); //Used with Priority = 2; Pushover will resend the notification every 60 seconds until the user accepts.
        $push->setExpire(3600); //Used with Priority = 2; Pushover will resend the notification every 60 seconds for 3600 seconds. After that point, it stops sending notifications.
        $push->setCallback('http://chris.schalenborgh.be/');
        $push->setTimestamp(time());
        $push->setDebug(false);
        $push->setSound('bike');
        $push->send();
        $receipt = $push->getReceipt();
    }

}