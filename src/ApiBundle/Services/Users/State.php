<?php

namespace ApiBundle\Services\Users;

class State {

    protected $json_parser;
    protected $api_connection;
    public function __construct($jsonParser, $apiConnection){
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
    }

    /**
     * @param $name
     * @return null
     */
    public function get($name){
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'users/' . $name . '/state');
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents());
            if(!$result) { return null; }
            return $result['result'];
        }catch(\Exception $ex){
            //Process the exception
        }

    }

}