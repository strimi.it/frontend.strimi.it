strimi.tagAutocomplete = function () {
    var uri = _config.tags_autocomplete;
        uri = uri.replace('strimi', '');
    $('.tags_autocomplete').textcomplete([{
        match: /\B#(\w*)$/,
        maxCount: 35,
        search: function(tagname, callback) {
            $.ajax({
                type: 'GET',
                url: uri + tagname,
                async: true,
                beforeSend: function() {},
                success: function(response) {
                    if (response.code == 200) {
                        callback(response.data);
                    } else if (response.code == false) {
                        callback([]);
                    }
                },
                error: function() {
                    callback([]);
                }
            });
        },
        index: 1,
        template: function(callback) {
            return '#' + callback.name + '<span class="count">' + callback.count_posts + ' <i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>';
        },
        replace: function(callback) {
            return '#' + callback.name + ' ';
        }
    }]);
};

var routeList = [
    'app_post_new',
    'app_post_new_text'
];
if(routeList.includes(_config.route)) {
    strimi.tagAutocomplete();
}