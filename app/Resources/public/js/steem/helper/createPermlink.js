strimi_app.createPermlink = function (content) {
    if(!content) { return false; }
    var time = new Date().toISOString().replace(/[^a-zA-Z0-9]+/g, '').toLowerCase();
    var permlink = content.split(' ').join('-').toLowerCase().replace(/[^a-z0-9-]+/g, '').trim();
        permlink = 'strimi-' + permlink + '-' + time;
    if (permlink.length > 255) {
        permlink = permlink.substring(permlink.length - 255, permlink.length);
    }
    permlink = permlink.replace(/[-]+/g, '-');
    return permlink;
};