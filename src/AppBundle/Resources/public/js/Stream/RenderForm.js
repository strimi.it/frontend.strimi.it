strimi.StreamRenderForm = function () {
    if (!strimi_app.is_login()) { return null; }

    var loading = function () {
        var content = document.getElementsByClassName('stream-form')[0];
        var url = content.getAttribute('data-url');
        $.ajax({
            type: 'GET',
            url: url,
            async: true,
            beforeSend: function () {
            },
            success: function (response) {
                if (response.code == 200) {
                    content.innerHTML = response.data.form;
                    //contentForm.className += ' active';
                    strimi.language();
                    strimi.autosize();
                    strimi.inputLimitText();
                    strimi.tagAutocomplete();
                    strimi.userAutocomplete();
                    strimi_app.streamSubmit();
                } else if (response.code == 400) {
                }
            },
            error: function () {
            }
        });
    };
    loading();

};
if(_config.route === 'stream_homepage') {
    strimi.StreamRenderForm();
}