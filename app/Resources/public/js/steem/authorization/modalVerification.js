strimi_app.modalVerification = function () {

    if(!strimi_app.is_login()) { return ''; }

    var account = strimi_app.authorization();
    var username = account.username;
    var hashID;
    var hash;
    var type;

    $('[data-verification="true"]').click(function() {
        $('#modalAuthorization').modal('show');
        //document.getElementById('authorization-login').value = username + '/active';
        hashID = $(this).data('hash');
        hash = document.getElementById(hashID);
        type = $(this).data('type');
    });

    $('#modalAuthorization').on('hidden.bs.modal', function () {
        document.getElementById('authorization-key').value = '';
    });

    var loading = function (submitButton, loadingStatus) {
        var loadingButton = document.getElementById('authorization-key-loading');
        if (loadingStatus === 'start') {
            submitButton.className += ' active-loading';
            loadingButton.classList.remove('hidden');
        } else if (loadingStatus === 'end') {
            submitButton.classList.remove('active-loading');
            loadingButton.className += ' hidden';
        }
    };

    $("#AuthorizationKeyForm").submit(function(e){
        e.preventDefault();
        var submitButton = document.getElementById('submit-modal-authorization');
        var formContent = document.getElementById('AuthorizationKeyForm');
        var successContent = document.getElementById('submit-modal-authorization-success');
        var error = document.getElementsByClassName('alert-active-verification-content')[0];
        loading(submitButton, 'start');
        var isValid = strimi_app.ActiveVerification(document.getElementById('authorization-key').value);
        if(isValid !== false) {
            hash.value = isValid;
            formContent.className += ' hidden';
            successContent.className += ' active';
            setTimeout(function(){
                $('#modalAuthorization').modal('hide');
                formContent.classList.remove('hidden');
                successContent.classList.remove('active');
                error.classList.remove('active');
                error.innerHTML = '';
                /* User Settings */
                if(type === 'profileUpdate'){
                    $('#profileUpdate').submit();
                }
            }, 1000);

        }
        loading(submitButton, 'end');
        return false;
    });



};
strimi_app.modalVerification();