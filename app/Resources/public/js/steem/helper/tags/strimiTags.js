strimi_app.strimiTags = function (type, domain) {
    //if(!nsfwDomain || !contentType) { return '' };

    var tags = document.getElementsByClassName('strimi-tags-input')[0].value;
    var nfs = strimi_app.strimiNsfw();

    if(tags) {
        tags = tags.split(" ");
        tags.forEach(function(value, index) {
            value = value.replace(/#/g, '');
            value = value.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "");
            value = value.replace(/strimi/g, '');
            if(value) {
                value = value.replace(/^-/, '').replace(/-\s*$/, "");
                value = value.toLowerCase();
            }
            tags[index] = value;
        });

        var tagList = [];
        var userTags = tags.slice(0, 4);

        /* Conversion to strimi tag */
        var tagStrimi = tags.slice(0, 4);
        tagStrimi.forEach(function (value, index) {
            if (value) {
                tagStrimi[index] = 'strimi-' + value;
            }
        });
        tagList = tagList.concat( tagStrimi );

        // if(type === 'text') {
        //     tagList = tagList.concat( userTags );
        //     tagList = _.compact(tagList);
        // }

        /* Adding NSFW */
        if(nfs) {
            tagList = ['strimi-nsfw'].concat( tagList );
        }

        /* Content PL */
        if(_config.locale === 'pl') {
            tagList = ['strimi-pl'].concat( tagList );
            tagList = _.compact(tagList);
        }

        tagList = ['strimi'].concat( tagList );
        tagList = _.compact(tagList);


        /* Adding Content Type */
        //tagList = tagList.concat( 'strimi-' + type );
        return _.compact(tagList);
    }
    return '';
};