var strimi = {};
with ((console && console._commandLineAPI) || {}) {
    console.log("%c Warning message ", "font: 2em sans-serif; color: red; background-color: yellow;");
    console.log("%cThis is a developer console, you must read and understand anything you paste or type here or you could compromise your account and your private keys.", "font: 18px sans-serif;");
    console.log("%c Wiadomość ostrzegawcza ", "font: 2em sans-serif; color: red; background-color: yellow;");
    console.log("%cJesteś w konsoli programisty, musisz zrozumieć wszystko co tutaj wkleisz lub wpiszesz! Pamiętaj, że możesz narazić swoje konto i klucze prywatne.", "font: 18px sans-serif;");
}