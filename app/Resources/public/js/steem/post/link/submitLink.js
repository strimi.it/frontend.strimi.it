strimi_app.postLinkSubmit = function () {

    if (!strimi_app.is_login()) {
        return;
    }

    var account = strimi_app.authorization();
    var username = account.username,
        wif = account.private_keys.posting_wif;

    $("#clear-strimi-link").click(function () {
        var newLink = document.getElementsByClassName('page-content-new')[0];
        newLink.classList.remove('hidden');
        var preview = document.getElementsByClassName('page-content-preview')[0];
        preview.classList.remove('active');
        preview.innerHTML = '';
        document.getElementById('link').value = '';
        localStorage.removeItem("link_preview");
    });

    var loading = function (submitButton, loadingStatus) {
        var loadingButton = document.getElementById('submit-strimi-link-loading');
        if (loadingStatus === 'start') {
            submitButton.className += ' active-loading';
            loadingButton.classList.remove('hidden');
        } else if (loadingStatus === 'end') {
            submitButton.classList.remove('active-loading');
            loadingButton.className += ' hidden';
        }
    };

    $("#formLinkPreview").submit(function (e) {
        e.preventDefault();

        var submitButton = document.getElementById('submit-strimi-link');
        if (submitButton.getAttribute('data-status') === '0') {
            submitButton.setAttribute('data-status', 1);
            loading(submitButton, 'start');

            var title = document.getElementsByClassName('link-title')[0];
                title = strimi_app.contentLength(title.value, title.getAttribute('length_max'));
            var body = document.getElementsByClassName('link-description')[0];
                body = strimi_app.contentLength(body.value, body.getAttribute('length_max'));
            const permlink = strimi_app.createPermlink(title);
            var strimi_metadata = localStorage.getItem("link_preview");
            var payout = strimi_app.linkPayout();

            //Tuhumb
            var thumb;
            var parseStrimiMetadata = JSON.parse(strimi_metadata);
                parseStrimiMetadata.body = body;
                parseStrimiMetadata.title = title;
            var contentType = parseStrimiMetadata.type.type;

            var domain = JSON.parse(strimi_metadata);
                domain = domain.domain;

            var tagsList = strimi_app.strimiTags(contentType, domain);

            if (parseStrimiMetadata.thumb) {
                thumb = parseStrimiMetadata.thumb
            } else if (contentType === 'image') {
                thumb = parseStrimiMetadata.source
            }

            //See the full content
            var body_footer = '<p>' + body + '</p>';
                body_footer += '<p><center><a href="https://strimi.it/strimi/@' + username + '/' + permlink + '"><img  src="' + thumb + '"></a></center></p>';
                body_footer += '<hr><p><center><em>Posted on <a href="https://strimi.it"><strong>Strimi.it</strong></a></em></p>';
                body_footer += '<p><em>See the full content <a href="' + 'https://strimi.it/strimi/@' + username + '/' + permlink + '">' + title + '</a></em></center></p>';
                body_footer += '<hr>';

            var operations = [
                ['comment',
                    {
                        parent_author: '',
                        parent_permlink: 'strimi',
                        author: username,
                        permlink: permlink,
                        title: title,
                        body: body_footer,
                        json_metadata : JSON.stringify({
                            tags: tagsList,
                            format: "html",
                            app: strimi_app.config.base.compilation,
                            strimi_metadata: JSON.stringify(parseStrimiMetadata)
                        })
                    }
                ],
                ['comment_options', {
                    author: username,
                    permlink: permlink,
                    max_accepted_payout: payout[0],
                    percent_steem_dollars: payout[1],
                    allow_votes: true,
                    allow_curation_rewards: true,
                    extensions: strimi_app.config.beneficiaries
                }]
            ];

            strimi_app.broadcast.sendAsync(
                { extensions: [], operations: operations  },
                { posting: wif }, function(error, result){
                    if (result && result !== 'undefined') {
                        loading(submitButton, 'end');
                        gtag('event', 'submit-link', { 'event_category': username, 'event_label': permlink });
                        window.location.href = '/strimi/@' + username + '/' + permlink;
                    } else if (error && error !== 'undefined') {
                        console.log( error );
                        loading(submitButton, 'end');
                        submitButton.setAttribute('data-status', 0);
                        Raven.captureException(error);
                    }
                });
        }

    });

};