strimi.userAutocomplete = function () {
    var uri = _config.accounts_autocomplete;
        uri = uri.replace('strimi', '');
    $('.user_autocomplete').textcomplete([{
        match: /\B@(\w*)$/,
        search: function(name, callback) {
            $.ajax({
                type: 'GET',
                url: uri + name,
                async: true,
                beforeSend: function() {},
                success: function(response) {
                    if (response.code == 200) {
                        callback(response.data);
                    } else if (response.code == false) {
                        callback([]);
                    }
                },
                error: function() {
                    callback([]);
                }
            });
        },
        index: 1,
        template: function(callback) {
            return '<img src="https://steemitimages.com/u/' + callback.name + '/avatar">' + callback.name + '<span class="reputation">' + callback.reputation + '</span>';
        },
        replace: function(callback) {
            return '@' + callback.name + ' ';
        }
    }]);
};

var routeList = [
    'app_post_new',
    'app_post_new_text'
];
if(routeList.includes(_config.route)) {
    strimi.nextAccountFollows();
}