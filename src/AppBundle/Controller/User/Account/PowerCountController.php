<?php

namespace AppBundle\Controller\User\Account;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PowerCountController extends Controller {

    /**
     * @param $username
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($username) {
        if(!$username) { die; };
        $account = $this->get('api.users.account')->get($username);
        return $this->get('app.json.response')->renderDefault(
            array(
                'voting_power'      => $account['voting_power'],
                'vote_worth'        => $account['vote_worth'],
                //'account_bandwidth' => $account['account_bandwidth']
            ),
            200
        );
    }

}
