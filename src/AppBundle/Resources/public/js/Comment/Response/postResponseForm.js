strimi.postCommentResponseForm = function () {
    if (!strimi_app.is_login()) { return null; }

    var remove = function () {
        $(".clear-post-response-comment").click(function() {
            document.getElementById('response-comment-form-content').remove();
        });
    };

    var get = function (id, element) {
        var url = document.getElementById('post-comment-form').getAttribute('data-url');
            url = url.replace(/false/, 'true');
        $.ajax({
            type: 'GET',
            url: url,
            async: true,
            beforeSend: function () {
            },
            success: function (response) {
                if (response.code == 200) {
                    $( '.comments-single-footer-' + id ).after( response.data.form );
                    element.data('status', 0);
                    remove();
                    strimi.language();
                    strimi.autosize();
                    strimi.tagAutocomplete();
                    strimi.userAutocomplete();
                    strimi.inputLimitText();
                    strimi_app.commentResponse(id);
                    strimi.postCommentResponseForm();
                } else if (response.code == 400) {
                }
            },
            error: function () {
            }
        });
    };

    $(".commentResponse").click(function() {
        var old = document.getElementById('response-comment-form-content');
        if(old) {
            old.remove();
        }
        if($(this).data('status') === 0){
            $(this).data('status', 1);
            var id = $(this).attr('data-id');
            get(id, $(this));
        }
    });

};