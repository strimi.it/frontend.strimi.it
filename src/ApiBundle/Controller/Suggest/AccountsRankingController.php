<?php

namespace ApiBundle\Controller\Suggest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AccountsRankingController extends Controller {


    /**
     * @return mixed
     */
    public function resetAction(){
        $code = 200;
        $accounts = $this->get('api.elastic.suggest.accounts')->get(null);
        $accountsContent = null;
        if($accounts) {
            $accountsContent = $this->renderView(
                'AppBundle:User/Ranking:trContent.html.twig',
                array(
                    'accounts' => $accounts
                )
            );
        }
        return $this->get('app.json.response')->renderDefault(
            $accountsContent,
            $code
        );
    }

    /**
     * @param $name
     * @param bool $sidebar
     * @return null
     */
    public function indexAction($name, $sidebar = false) {
        if(!$name) { return null; }
        $code = 200;

        if($sidebar) {
            $accounts = $this->get('api.elastic.suggest.accounts')->get($name, 10);
        } else {
            $accounts = $this->get('api.elastic.suggest.accounts')->get($name);
        }

        $accountsContent = null;

        if($accounts && $sidebar) {
            $accountsContent = $this->renderView(
                'AppBundle:User/Ranking:liContent.html.twig',
                array(
                    'accounts' => $accounts
                )
            );
        } elseif($accounts) {
            $accountsContent = $this->renderView(
                'AppBundle:User/Ranking:trContent.html.twig',
                array(
                    'accounts' => $accounts
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            $accountsContent,
            $code
        );
    }


}
