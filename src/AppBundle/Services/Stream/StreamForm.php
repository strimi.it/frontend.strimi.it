<?php

namespace AppBundle\Services\Stream;
use AppBundle\EntityFake\Stream;
use AppBundle\Form\StreamType;

class StreamForm {

    protected $form_factory;
    protected $router;
    protected $templating;
    protected $request_stack;
    public function __construct($form_factory, $router, $templating, $request_stack) {
        $this->form_factory = $form_factory;
        $this->router = $router;
        $this->templating = $templating;
        $this->request_stack = $request_stack;
    }

    public function createForm(){
        return $this->form_factory->create(StreamType::class, new Stream(), array(
            'action' => '',
            'method' => 'POST',
        ));
    }

    public function renderForm(){
        return $this->templating->render(
            'AppBundle:Stream:form.html.twig',
            array(
                'form' => $this->createForm()->createView()
            )
        );
    }

}