<?php

namespace ApiBundle\Services\Users;

class vestingDelegations {

    protected $api_connection;
    protected $vest_to_sp;
    protected $global_properties;
    protected $memcached;

    public function __construct($apiConnection, $vestToSteem, $global_properties, $memcached){
        $this->api_connection = $apiConnection;
        $this->vest_to_sp = $vestToSteem;
        $this->global_properties = $global_properties;
        $this->memcached = $memcached;
    }

    /**
     * @param $name
     * @return null
     */
    private function content($name) {
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'users/' . $name . '/vestingDelegations');
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result'];
            $properties = $this->global_properties->get();
            if(is_array($result) && count($result) > 0) {
                foreach($result as &$single) {
                    $single['steem_power'] = $this->vest_to_sp->parser($properties, $single['vesting_shares']);
                }
            }
            return $result;
        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @param $name
     * @return null
     */
    public function get($name){
        if(!$name) { return null; }
        $key = 'vestingDelegations_' . (md5($name));
        $content = $this->memcached->get($key);
        if(!$content) {
            $value = $this->content($name);
            $this->memcached->set($key, $value, 3, 'minutes');
        }
        return $this->memcached->get($key);
    }

}