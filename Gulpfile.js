var gulp = require('gulp');
var yarn = require('gulp-yarn');
var less = require('gulp-less');
var path = require('path');
var cleanCSS = require('gulp-clean-css');
var stripCssComments = require('gulp-strip-css-comments');
var runSequence = require('run-sequence');
//var del = require('del');
//var fs = require('fs');
var concat = require('gulp-concat');
var htmlmin = require('gulp-htmlmin');
var merge = require('merge-stream');
var minify = require('gulp-minify');
var clean = require('gulp-clean');

var data = require('gulp-data');
var fs = require('fs');

var app = {
    version: require('./package.json').version,
    app: { path: 'app' },
    src: { path: 'src' },
    web: { path: 'web' },
    tmp: { path: '.tmp' }
};

gulp.task('yarn', function() {
    return gulp.src(['./package.json', './yarn.lock'])
        .pipe(gulp.dest('./dist'))
        .pipe(yarn({
            production: true
        }));
});

/* Clean Folder */
gulp.task('clean_prod', function () {
    return merge(
        gulp.src(app.web.path +  '/images', {read: false }).pipe(clean()),
        gulp.src(app.web.path +  '/fonts', {read: false }).pipe(clean()),
        gulp.src(app.web.path +  '/js', {read: false }).pipe(clean()),
        gulp.src(app.web.path +  '/css', {read: false }).pipe(clean()),
        gulp.src(app.web.path +  '/app_dev.php', {read: false }).pipe(clean())
    );
});

gulp.task('clean_dev', function () {
    return merge(
        gulp.src(app.web.path +  '/images', {read: false }).pipe(clean()),
        gulp.src(app.web.path +  '/fonts', {read: false }).pipe(clean()),
        gulp.src(app.web.path +  '/js', {read: false }).pipe(clean()),
        gulp.src(app.web.path +  '/css', {read: false }).pipe(clean())
    );
});

gulp.task('clean_css', function () {
    return gulp.src(app.web.path +  '/css', {read: false }).pipe(clean());
});

gulp.task('clean_js', function () {
    return merge(
        gulp.src(app.web.path +  '/js/' + app.version + '/languageData.js', {read: false }).pipe(clean()),
        gulp.src(app.web.path +  '/js', {read: false }).pipe(clean())
    );
});

gulp.task('clean_js_prod', function () {
    return merge(
        gulp.src(app.web.path +  '/js/' + app.version + '/app.js', {read: false }).pipe(clean()),
        gulp.src(app.web.path +  '/js/' + app.version + '/vendor.js', {read: false }).pipe(clean())
    );
});

/* LESS App - Compile */
gulp.task('app-less-compile', function () {
    return gulp.src( app.app.path + '/Resources/public/less/app.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest( app.web.path + '/css/' + app.version ));
});

/* CSS - Compress */
gulp.task('web-minify-css', function () {
    return gulp.src( app.web.path + '/css/' + app.version + '/*.css')
        .pipe(cleanCSS({
            debug: true,
            compatibility: 'ie8'
        }, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(stripCssComments())
        .pipe(gulp.dest( app.web.path + '/css/' + app.version ));
});

/* JavaScript Vendor - Compile */
gulp.task('vendor-scripts-compile', function() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/steem/dist/steem.min.js',
        './node_modules/autosize/dist/autosize.min.js',
        './node_modules/bootstrap/js/dropdown.js',
        './node_modules/bootstrap/js/modal.js',
        './node_modules/bootstrap/js/tooltip.js',
        './node_modules/bootstrap/js/popover.js',
        './node_modules/crypto-js/crypto-js.js',
        './node_modules/jquery.ns-autogrow/dist/jquery.ns-autogrow.min.js',
        './node_modules/jquery_lazyload/jquery.lazyload.min.js',
        './node_modules/jquery-lazy/jquery.lazy.min.js',
        './node_modules/jquery-slimscroll/jquery.slimscroll.min.js',
        './node_modules/jquery-textcomplete/dist/jquery.textcomplete.min.js',
        './node_modules/jquery-toast-plugin/dist/jquery.toast.min.js',
        './node_modules/lodash/lodash.min.js',
        './node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
        './node_modules//bootstrap-slider/dist/bootstrap-slider.min.js',

        './node_modules/moment/min/moment-with-locales.min.js',
        './node_modules/moment-timezone/builds/moment-timezone.min.js',
        './node_modules/moment-timezone/builds/moment-timezone-with-data.min.js',

        './node_modules/push.js/bin/push.min.js',
        './node_modules/raven-js/dist/raven.js',
        './node_modules/remarkable/dist/remarkable.min.js',
        './node_modules/tablesorter/dist/jquery.tablesorter.min.js',
        './node_modules/jquery-slimscroll/jquery.slimscroll.min.js'
    ])
        .pipe(concat({
            path: 'vendor.js',
            stat: {
                mode: 0666 }
        }))
        .pipe(gulp.dest( app.web.path + '/js/' + app.version ));
});

/* Make Language */
gulp.task('app-language', function () {
    var en = [];
    var pl = [];
    en.push( fs.readFileSync( './' + app.app.path  + '/Resources/translations/messages.en.json' ) );
    pl.push( fs.readFileSync( './' + app.app.path  + '/Resources/translations/messages.pl.json' ) );
    var data = 'strimi.languagePL = function () { "use strict"; return ' + pl + '}; strimi.languageEN = function () { "use strict"; return ' + en + ' }; ';
    fs.writeFile( app.app.path + '/Resources/public/js/languageData.js', data );
});

/* JavaScript App - Compile */
gulp.task('app-scripts-compile', function() {
    return gulp.src([

        app.app.path + '/Resources/public/js/steem/_config.js',
        app.app.path + '/Resources/public/js/steem/authorization/authorization.js',
        app.app.path + '/Resources/public/js/steem/login.js',

        app.app.path + '/Resources/public/js/app.js',
        app.app.path + '/Resources/public/js/components/sentry.js',
        app.app.path + '/Resources/public/js/components/momentTimezone.js',
        app.app.path + '/Resources/public/js/components/commentsCount.js',
        app.app.path + '/Resources/public/js/languageData.js',
        app.app.path + '/Resources/public/js/language.js',

        app.app.path + '/Resources/public/js/**/*.js',
        app.src.path + '/**/*.js'
    ])
        .pipe(concat({
            path: 'app.js',
            stat: {
                mode: 0666 }
        }))
        .pipe(gulp.dest( app.web.path + '/js/' + app.version ));
});

/* JavaScript Files - Compress */
gulp.task('scripts-compress-prod', function() {
    return gulp.src( app.web.path + '/js/' + app.version + '/*.js') .pipe(minify({ compress: true })).pipe(gulp.dest( app.web.path + '/js/' + app.version  ))
});

gulp.task('scripts-compress-dev', function() {
    gulp.src( app.web.path + '/js/' + app.version + '/*.js')
        .pipe(minify({
            ext:{
                src:'-debug.js',
                min:'.min.js'
            },
            compress: true
        }))
        .pipe(gulp.dest( app.web.path + '/js/' + app.version  ))
});

/* Base.html.twig - Compress */
gulp.task('minify-base-twig', function() {
    return gulp.src( app.app.path + '/Resources/views/source/base.html.twig' )
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest( app.app.path + '/Resources/views' ));
});

/* Copy Files */
gulp.task('copy-files', function () {
    return merge(
        gulp.src( app.app.path + '/Resources/public/images/**' ).pipe(gulp.dest( app.web.path +  '/images' )),
        gulp.src( app.app.path + '/Resources/public/fonts/**' ).pipe(gulp.dest( app.web.path +  '/css/fonts' )),
        gulp.src( './node_modules/bootstrap/fonts/**' ).pipe(gulp.dest( app.web.path +  '/css/fonts' )),
        gulp.src( './node_modules/font-awesome/fonts/**' ).pipe(gulp.dest( app.web.path +  '/css/fonts' ))
    );

});


/*
 * Gulp Run Task */

/* Development */
gulp.task('make_dev', function () {
    runSequence(
        'clean_dev',
        'minify-base-twig',
        'app-less-compile',
        'app-language',
        'app-scripts-compile',
        'vendor-scripts-compile',
        'scripts-compress-dev',
        'copy-files',
        'app-language'
    )
});

/* Development - Less */
gulp.task('make_less', function () {
    runSequence(
        'clean_css',
        'app-less-compile',
        'copy-files'
    )
});

/* Development - JavaScript */
gulp.task('make_js', function () {
    runSequence(
        'clean_js',
        'vendor-scripts-compile',
        'app-language',
        'app-scripts-compile'
    )
});

/* Production */
gulp.task('make_prod', function () {
    runSequence(
        'clean_prod',
        'app-less-compile',
        'web-minify-css',
        'app-language',
        'app-scripts-compile',
        'vendor-scripts-compile',
        'scripts-compress-prod',
        'minify-base-twig',
        'copy-files',
        'app-language',
        'clean_js_prod'
    )
});

