strimi_app.textTags = function (nsfw) {
    var tagsValue = document.getElementsByClassName('text-tags')[0].value;
        tagsValue = tagsValue.toLowerCase();
    if(tagsValue) {
        var tags = tagsValue.split(" ");
        var userTags = tagsValue.replace(/\W+/g, " ")
            userTags = userTags.split(" ");
            userTags = userTags.slice(0, 4);
        var tagsList;
        tags.forEach(function(value, index) {
            value = value.replace(/#/g, '');
            value = value.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "");
            value = value.replace(/strimi/g, '');
            tags[index] = value;
            if(value) {
                value = value.replace(/^-/, '').replace(/-\s*$/, "");
                value = value.toLowerCase();
                tags[index] = 'post-' + value;
            }
        });

        tags = tags.slice(0, 4);
        tags = _.compact(tags);
        userTags = ['strimi'].concat( userTags );
        tagsList = userTags.concat( tags );
        return  _.compact(tagsList);
    }
    return '';
};