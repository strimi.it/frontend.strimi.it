<?php
namespace AppBundle\Services\TwigExtended;

class OnlyMarkdownExtension extends \Twig_Extension {

    protected $sanitizer;
    public function __construct( $sanitizer){
        $this->sanitizer = $sanitizer;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('onlyMarkdown', array($this, 'content'), array('is_safe' => array('html'))),
        );
    }

    public function content($content) {
        if(!$content) { return null; }
        return $this->sanitizer->allowOnlyMarkdown($content);
    }

    public function getName() {
        return 'onlyMarkdown';
    }

}