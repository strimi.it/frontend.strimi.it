<?php

namespace AppBundle\Controller\User\Account;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class VotesController extends Controller {

    /**
     * @param $username
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($username) {
        if(!$username) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $account = $this->get('api.users.account')->get($username);
        if(!$account) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $votes = $this->get('api.users.account.votes')->get($account['name']);
        return $this->render(
            'AppBundle:User/Account:index.html.twig',array(
                'account' => $account,
                'votes'   => $votes
            )
        );
    }

    /**
     * @param $username
     * @param $package
     * @return null
     */
    public function nextVotesAction($username, $package) {
        if(!$username || !$package) { return null; }

        $code = 400;
        $account = $this->get('api.users.account')->get($username);
        $votes = $this->get('api.users.account.votes')->getNext($account['name'], $package);
        $votesContent = null;
        $nextUri = null;

        if($votes) {
            $code = 200;

            $nextUri = $this->generateUrl('user_account_ajax_votes_next', array(
                'username'  => $account['name'],
                'package'   => $package + 35
            ));

            $votesContent = $this->renderView(
                'AppBundle:User/Account/Votes:votesLists.html.twig',
                array(
                    'votes' => $votes,
                    'appRoute' => null
                )
            );

        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'votes'    => $votesContent,
                'next_url' => $nextUri
            ),
            $code
        );
    }

}
