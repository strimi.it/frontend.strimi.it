<?php

namespace ApiBundle\Services;

class RewardFund {

    protected $json_parser;
    protected $api_connection;
    protected $memcached;
    public function __construct($jsonParser, $apiConnection, $memcached){
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $memcached;
    }

    /**
     * @return null
     */
    public function content(){
        try{
            $response = $this->api_connection->init()->request('GET', '/getRewardFund');
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents())['result'];
            if(!$result) { return null; }

            $result['reward_balance'] = array(
                'value' => $result['reward_balance'],
                'convert' => (float)str_replace(' STEEM', '', $result['reward_balance']) / (float)$result['recent_claims']
            );

            return $result;
        }catch(\Exception $ex){
            //Process the exception
        }

    }

    /**
     * @return mixed
     */
    public function get(){
        $key = 'getRewardFund';
        $content = $this->memcached->get($key);
        if(!$content) {
            $value = $this->content();
            $this->memcached->set($key, $value, 15, 'seconds');
        }
        return $this->memcached->get($key);
    }

}