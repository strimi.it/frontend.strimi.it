<?php

namespace AppBundle\Controller\Stream;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller {

    /**
     * @param Request $request
     * @return mixed
     */
    public function renderFormAction(Request $request){
        if(!$request->isXmlHttpRequest()) { die(); };
        return $this->get('app.json.response')->renderDefault(
            array(
                'form' => $this->get('app.stream.form')->renderForm()
            ),
            200
        );
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function renderSubmitPostAction($author, $permlink, Request $request){
        if(!$request->isXmlHttpRequest() || !$author || !$permlink ) { die(); };

        $post = $this->get('api.post.single')->get($author, $permlink);
        if(!$post){ return null; }
        $appRoute = $request->get('_route');
        $data = $this->renderView(
            'AppBundle:Stream/List:single.html.twig',
            array(
                'comment' => $post,
                'appRoute' => $appRoute
            )
        );

        return $this->get('app.json.response')->renderDefault(
            $data,
            200
        );

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function renderNewPostAction($limit, Request $request){
        if(!$limit ) { die(); };
        $posts = $this->get('api.post.stream')->getNewest($limit);
        if(!$posts){ return null; }
        $data = $this->renderView(
            'AppBundle:Stream/List:index.html.twig',
            array(
                'posts' => $posts,
                'render_new' => true,
                'appRoute' => null
            )
        );

        return $this->get('app.json.response')->renderDefault(
            $data,
            200
        );

    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(){
        //cron cachce 30s
        $tag = 'strimi-stream';
        $result = $this->get('api.posts.defaults')->createdByTag($tag);
        return $this->render(
            'AppBundle:Stream:index.html.twig', array(
                'posts' => $result,
                'tag_name' => $tag
            )
        );
    }

}
