strimi.momentTimezone = function () {
    moment.locale(localStorage.getItem("language"));
    var timeZone =  document.getElementsByClassName('time-zone');
    if(timeZone !== 123) {
        for (var i = 0; i < timeZone.length; i++) {
            var time = timeZone[i].getAttribute('data-time');
            var setTime = moment(time + 'Z').format('YYYY/MM/DD HH:mm:ss');
            var fromNow = moment(time + 'Z').fromNow();
                //timeZone[i].setAttribute('data-time', setTime);
                timeZone[i].setAttribute('data-content', setTime);
                timeZone[i].innerHTML = fromNow;
        }
    }
};
strimi.momentTimezone();

strimi.momentTimezoneLanguage = function () {
    moment.locale(localStorage.getItem("language"));
    var timeZone =  document.getElementsByClassName('time-zone');
    if(timeZone) {
        for (var i = 0; i < timeZone.length; i++) {
            var time = timeZone[i].getAttribute('data-time');
            timeZone[i].innerHTML = moment(time + 'Z').fromNow();
        }
    }
};
strimi.momentTimezoneLanguage();