<?php

namespace ApiBundle\Services\Users;

class Replies {

    protected $api_connection;
    protected $api_connection_steem;
    protected $json_parser;
    protected $posts;
    protected $memcached;
    public function __construct($apiConnection, $apiConnectionSteemjs, $jsonParser, $posts, $memcached){
        $this->api_connection = $apiConnection;
        $this->api_connection_steem = $apiConnectionSteemjs;
        $this->json_parser = $jsonParser;
        $this->posts = $posts;
        $this->memcached = $memcached;
    }

    /**
     * @param $name
     * @param $permlink
     * @param int $limit
     * @return array|null
     */
    public function replies($name, $permlink, $limit = 35){
        if(!$name) { return null; }

        try{
            $response = $this->api_connection->init()->request('GET', 'users/' . $name . '/replies', [
                'query' => [
                    'startPermlink' => $permlink,
                    'limit' => $limit
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result'];
            if(is_array($result)) {
                foreach ($result as &$singleObject) {
                    $singleObject = $this->posts->parserContent($singleObject);
                }
            }

            if(!empty($permlink)) {
                array_shift($result);
            }

            return $result;
        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @param $key
     * @param $account
     * @return null
     */
    private function contentCache($key, $account){
        if(!$key || !$account) { return null; }
        $comments = $this->memcached->get($key);
        if(!$comments) {
            $value = $this->replies($account, null);
            $this->memcached->set($key, $value, 1, 'minutes');
        }
        return $this->memcached->get($key);
    }

    /**
     * @param $name
     * @return null
     */
    public function get($name){
        if(!$name) { return null; }
        return $this->contentCache('account_' . md5($name) . '_replies', $name);
    }




}