<?php

namespace AppBundle\Controller\Stream;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NextController extends Controller {

    /**
     * @param $author
     * @param $permlink
     * @param $path
     * @return null
     */
    public function index1Action($package, Request $request){
        if(!$package) { return null; }

        $code = 400;
        $posts = $this->get('api.post.stream')->getNext($package, 'new');
        $postsContent = null;
        $nextUri = null;
        if($posts) {

            $code = 200;
            $nextUri = $this->generateUrl('stream_ajax_next_post', array(
                'package'   => $package + $this->getParameter('stream')['limit']
            ));
            $appRoute = $request->get('_route');
            $postsContent = $this->renderView(
                'AppBundle:Stream/List:index.html.twig',
                array(
                    'posts' => $posts,
                    'appRoute' => $appRoute
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'posts'    => $postsContent,
                'next_url' => $nextUri
            ),
            $code
        );
    }

    /**
     * @param $author
     * @param $permlink
     * @param $path
     * @param $tag
     * @return null
     */
    public function indexAction($author, $permlink, $path, $tag, Request $request){
        if(!$author || !$permlink || !$path || !$tag) { return null; }

        $code = 400;
        $posts = $this->get('api.posts.next')->content($author, $permlink, $path, $tag);
        $postsContent = null;
        $nextUri = null;
        if($posts) {
            $code = 200;
            $lastPost =  end($posts);
            $appRoute = $request->get('_route');
            $nextUri = $this->generateUrl('app_ajax_posts_by_tag_next', array(
                'author'   => $lastPost['author'],
                'permlink' => $lastPost['permlink'],
                'path'     => $path,
                'tag'      => $tag
            ));

            $postsContent = $this->renderView(
                'AppBundle:Stream/List:index.html.twig',
                array(
                    'posts' => $posts,
                    'appRoute' => $appRoute
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'posts'    => $postsContent,
                'next_url' => $nextUri
            ),
            $code
        );
    }

}
