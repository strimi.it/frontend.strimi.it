<?php

namespace AppBundle\Services;

use Parsedown;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Sanitizer
{

    private $_htmlPurifier;
    private $_parsedown;
    private $_movieThumbnail;
    private $container;
    private $router;
    private $allowedTags;
    public function __construct(MovieThumbnailServices $movieThumbnailServices, $container, UrlGeneratorInterface $router = null)
    {
        $this->_movieThumbnail = $movieThumbnailServices;
        $this->allowedTags = [
            'div',
            'del',
            'a[href|title]', 'p', 'b', 'i', 'q', 'br',
            'ul', 'li', 'ol',
            'img[src|title|alt|class]',
            'blockquote', 'pre', 'code', 'em', 'strong', 'center',
            'table', 'thead', 'tbody', 'tr', 'th', 'td',
            'strike', 'sup', 'sub'
        ];
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('Cache.DefinitionImpl', null);
        $config->set('HTML.Allowed', implode(',', $this->allowedTags));
        $config->set('HTML.SafeIframe', true);
        $this->_htmlPurifier = new \HTMLPurifier($config);
        $this->_parsedown = new Parsedown();
        $this->_parsedown->setMarkupEscaped(true); # escapes markup (HTML)
        $this->_parsedown->setUrlsLinked(false); # disable autolink
        $this->router = $router;

        $this->container = $container;
        //$this->templating = $templating;
    }


    /**
     * Clear text from markdonw and html tags
     * @param $content
     * @return mixed
     */
    public function clearContent($content)
    {
        $content = htmlspecialchars_decode($this->_parsedown->text($content));
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('Cache.DefinitionImpl', null);
        $config->set('HTML.Allowed', '');

        $htmlPurifier = new \HTMLPurifier($config);
        $content = strip_tags($htmlPurifier->purify($content));
        $pattern = '~[a-z]+://\S+~';
        $content = preg_replace($pattern, '', $content);
        $pattern = '/<([^>\s]+)[^>]*>(?:\s*(?:<br \/>|&nbsp;|&thinsp;|&ensp;|&emsp;|&#8201;|&#8194;|&#8195;)\s*)*<\/\1>/';
        $content = preg_replace($pattern, '', $content);
        $pattern = '/\s+/';
        $content = trim(preg_replace($pattern, ' ', $content));
        return $content;

    }

    /**
     * Czyszczenie contentu z tagow, parsowanie markdown, podmiana iframeow
     *
     * @param $content
     * @return string
     *
     */
    public function sanitize($content)
    {
        $content = htmlspecialchars_decode($this->_parsedown->text($content));
        $content = $this->hashtag($content);
        $content = $this->image($content);
        $content = $this->_htmlPurifier->purify($content);
        $content = $this->linkUserNames($content);
        //$content = $this->_parseMedia($content);

        return $this->replaceIframes($content);

    }

    /**
     * Clear text from HTML tags, allowed only markdown
     *
     * @param $content
     * @return string
     */
    public function allowOnlyMarkdown($content)
    {
        if (!$content || !is_string($content)) return false;
        $content = htmlspecialchars_decode($content);

        $parseDown = new Parsedown();
        $content = $parseDown->text($content);
        $content = $this->hashtag($content);
        $content = $this->_parsedown->text($content);
        $content = htmlspecialchars_decode($content);
        $content = $this->linkUserNames($content);
        // need of second purifying to remove all non-supported tags after markdown parsing
        //$content = $purifier->purify($content);
        $content = $this->_htmlPurifier->purify($content);
        $content = $this->image($content);
        $content = $this->orphanLink($content);
        return htmlspecialchars_decode($content);

    }

    private function replaceOuter($content){
        if(empty($content)) { return $content; }
        preg_match("~<body.*?>(.*?)<\/body>~is", $content, $match);
        if(!empty($match[1])) {
            return trim($match[1]);
        }
        return $content;
    }

    /**
     * Clear text from HTML tags, allowed only markdown
     *
     * @param $content null
     * @return string
     */
    public function outerSanitizer($content = null)
    {
        if (!$content || !is_string($content)) return false;
        $content = htmlspecialchars_decode($content);

        // need another instance of purifier to add some special tags to config
        $config = \HTMLPurifier_Config::createDefault();
        array_push($this->allowedTags, 'h1','h2', 'h3', 'h4', 'h5', 'h6', 'center', 'hr', 'br', 'div');
        $config->set('HTML.Allowed', implode(',', $this->allowedTags));
        $config->set('Cache.DefinitionImpl', null);
        $config->set('HTML.SafeIframe', true);
        $config->set('AutoFormat.RemoveEmpty', true);

        // first instance of purifier to clear all html tags
        $purifier =  new \HTMLPurifier($config);
        $content = $this->hashtag($content);
        $content = $this->_parsedown->text($content);
        $content = htmlspecialchars_decode($content);
        $content = $this->linkUserNames($content);
        // need of second purifying to remove all non-supported tags after markdown parsing
        $content = htmlspecialchars_decode($content);
        $content = $purifier->purify($content);
        $content = $this->image($content);
        //$content = $this->_parseMedia($content);
        $content = $this->replaceOuter($content);
        return htmlspecialchars_decode($content);

    }



    /**
     * @param $content
     * @return mixed|null
     */
    public function _parseMedia($content)
    {
        // parsing empty elements
        $pattern = '/<([^>\s]+)[^>]*>(?:\s*(?:<br \/>|&nbsp;|&thinsp;|&ensp;|&emsp;|&#8201;|&#8194;|&#8195;)\s*)*<\/\1>/';
        $content = preg_replace($pattern, '', $content);
        // parsing youtbe links
        $pattern = '#((?:www\.)?(?:youtube\.com\/(?:watch\?v=|embed\/|v\/)|youtu\.be\/|youtube\-nocookie\.com\/embed\/)([a-zA-Z0-9-]*))#i';
        $content = $this->toIframe($content, $pattern, 'youtube');
        // parsing facebook photos
        $pattern = '/https?:\/\/(www.)?facebook\.com\/([a-zA-Z0-9_\- ]*)\/([a-zA-Z0-9_\- ]*)\/([a-zA-Z0-9_\.\-]*)\/([a-zA-Z0-9_\-]*)(\/\?type=1&theater\/)?/i';
        $content = $this->toIframe($content, $pattern, 'facebook-photo');
        // parsing facebook posts and movies
        $pattern = '/(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?([\w\-]*)?\/(?:[\w\-]*)?\/([\w\-]*)/m';
        $content = $this->toIframe($content, $pattern, 'facebook');
        // vimeo
        $pattern = '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/';
        $content = $this->toIframe($content, $pattern, 'vimeo');
        // instagram
        $pattern = '/(https?:\/\/(www\.)?)?instagram\.com(\/p\/\w+\/?)/';
        $content = $this->toIframe($content, $pattern, 'instagram');

        // twitter
        $pattern = '#https?://twitter\.com/(?:\#!/)?(\w+)/status(es)?/(\d+)#is';
        $content = $this->toIframe($content, $pattern, 'twitter');

        return $content;
    }

    /**
     * @param null $content
     * @param null $pattern
     * @param null $type
     * @return mixed|null
     */
    private function toIframe($content = null, $pattern = null, $type = null)
    {

        if ($content !== null || $pattern === !null || $type !== null) {
            $content = preg_replace_callback($pattern,
                function ($matches) use ($type) {

                    switch ($type) {
                        case 'facebook':
                            $id = $matches[2];
                            break;
                        case 'facebook-photo':
                            $type = 'facebook';
                            $id = $this->fbvideoid($matches[0]);
                            break;
                        case 'instagram':
                        case 'twitter':
                            $id = $matches[3];
                            break;
                        case 'youtube';
                            $id = $matches[2];
                            $matches[0] = 'https://' . $matches[0];
                            break;
                        default:
                            $id = $matches[1];
                            break;
                    }


                    $domain = parse_url($matches[0], PHP_URL_HOST);
                    $imageUrl = $this->_movieThumbnail->getThumbnailUrl($id, $type);
                    $templating = '';
                    if($type === 'youtube' || $type === 'vimeo') {
                        $templating = $this->container->get('templating')->render(
                            'AppBundle:Post/Single/Structure/content:video.html.twig',
                            array(
                                'outerVideoDomain' => $domain,
                                'outerVideoThumbnail' => $imageUrl,
                                'outerVideoID' => $id,
                                'outerVideoTypeFrom' => $domain,
                                'outerVideoSource' => $matches[0]
                            )
                        );
                    }
                    return $templating;
                }, $content);
        }
        return $content;
    }


    /**
     * Get video id from any facebook video URL
     * funcion by Mohamed Elbahja
     */
    function fbvideoid($url)
    {
        // delete space in url
        $url = str_replace(' ', '', $url);
        // parse url
        $pars = parse_url($url);
        $path = $pars['path'];

        // delete end slashe
        if ($path[strlen($path) - 1] == '/'):
            $path = rtrim($path, '/');
        endif;

        $count = count(explode("/", $path));
        // type url
        $urltype = "";
        if ($pars['path'] == "/photo.php" || $pars['path'] == "/video.php" || $pars['path'] == "/"):
            $urltype = 2;
        elseif ($count == 4):
            $urltype = 3;
        elseif ($count == 5):
            $urltype = 1;
        endif;

        // get id
        if ($urltype == 1) {

            $ex = explode("/", $path);
            $videoid = $ex[4];

        } else if ($urltype == 2) {

            parse_str($pars['query'], $e);
            $videoid = $e['v'];

        } else if ($urltype == 3) {

            $ex = explode("/", $path);
            $videoid = $ex[3];

        } else {
            $videoid = null;
        }

        return $videoid;
    }

    /**
     * Change solo image links to <img src>
     *
     * @param $content
     * @return mixed|null
     *
     */
    private function image($content)
    {
        if (!$content) {
            return null;
        }
        $content = preg_replace("/(?<!src=\"|href=\"|alt=\"|\/)(https?:\/\/[^ ]+?(?:\.jpg|\.jpeg|\.png|\.gif|\.JPG|\.JPEG|\.PNG|\.GIF))/", '<img class="lazy" data-src="$1" />', $content);

        $domContent = new \DOMDocument();
        $domContent->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
        $imgs = $domContent->getElementsByTagName('img');

        foreach ($imgs as $img) {
            try {
                $href = $img->getAttribute('src');

                $isImage = new UrlParser();
                $isImage = $isImage->checkIfImage($href);
                if(!empty($href) && $isImage) {
                    //$imageData = getimagesize($href);
                    $imageDomain = parse_url($href);
                    $imageDomain = $imageDomain['host'];
                    $uniq = md5($href);
                    $img->setAttribute('class','image-strimi');
                    $img->setAttribute('data-src', $href);
                    $img->setAttribute('id', 'image-strimi-' . $uniq);
                    if($imageDomain) {
                        $img->setAttribute('data-domain', $imageDomain);
                    }
                    //$img->setAttribute('data-width', $imageData[0]);
                    //$img->setAttribute('data-height', $imageData[1]);
                    //$img->setAttribute('data-type', str_replace('mime/', '', $imageData['mime']));
                    $img->setAttribute('data-uniq', $uniq);
                    $img->removeAttribute('src');
                }

            } catch (Exception $e) {
                // TODO: monolog exceptions
            }
        }
        $content = $domContent->saveHTML($domContent->documentElement);

        return $content;
    }

    /**
     * Find and return array of image links in string
     * @param $content
     * @return null
     */
    public function imageArrayFromText($content) {
        if (!$content) {
            return null;
        }
        preg_match_all("/(?<!src=\"|href=\"|alt=\"|\/)(https?:\/\/[^ ]+?(?:\.jpg|\.jpeg|\.png|\.gif|\.JPG|\.JPEG|\.PNG|\.GIF))/", $content, $matches);
        return $matches[0];
    }

    /**
     * @param $content
     * @return mixed|null
     */
    private function hashtag($content)
    {
        if (!$content) {
            return null;
        }
        return preg_replace_callback('/(?<!\S)#([0-9a-zA-Z-_]+)/', function ($match) {
            $url = $this->router->generate("app_posts_by_tag_trending", array("tag" => $match[1]));
            return '&nbsp;#<a href="'. $url .'" title="'. $match[1] .'">'. $match[1] .'</a>';
        }, $content);
    }

    /**
     * Change https://* or www.* to <a href=''/>
     *
     * @param $content
     * @return null|string|string[]
     * @credits Krasimir Tsonev
     */
    public function orphanLink($content)
    {

        // first we change the www to urls
        $pattern = '/(?<!\S)((www)[^,\\n ]+)/i';
        $content = preg_replace_callback($pattern, function ($match) {
            return "<a href='http://$match[0]' target='_blank' rel='nofollow'>$match[0]</a>";
        }, $content);

        // then we found urls and parse them
        $reg_exUrl = "/(?<!\S)(http|https|ftp|ftps|www)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/i";
        $urls = array();
        $urlsToReplace = array();
        if (preg_match_all($reg_exUrl, $content, $urls)) {
            $numOfMatches = count($urls[0]);
            $numOfUrlsToReplace = 0;
            for ($i = 0; $i < $numOfMatches; $i++) {
                $alreadyAdded = false;
                $numOfUrlsToReplace = count($urlsToReplace);
                for ($j = 0; $j < $numOfUrlsToReplace; $j++) {
                    if ($urlsToReplace[$j] == $urls[0][$i]) {
                        $alreadyAdded = true;
                    }
                }
                if (!$alreadyAdded) {
                    array_push($urlsToReplace, $urls[0][$i]);
                }
            }
            $numOfUrlsToReplace = count($urlsToReplace);
            for ($i = 0; $i < $numOfUrlsToReplace; $i++) {
                $content = str_replace($urlsToReplace[$i], "<a href=\"" . $urlsToReplace[$i] . "\" target=\"_blank\" rel=\"nofollow\">" . $urlsToReplace[$i] . "</a> ", $content);
            }

        }

        return ' ' . $content;

    }


    /**
     * Linking user names
     * @param $content
     * @return mixed
     */
    private function linkUserNames($content)
    {
        try {
            $pattern = '/(?<!\/)@(\w+)/';
            return preg_replace_callback($pattern, function ($match) {
                $url = $this->router->generate('user_account', array('username' => $match[1])) ;
                return '&nbsp;@<a href="' . $url . '" title="'. $match[1] .'">' . $match[1] . '</a>';
            }, $content);
        } catch (Exception $e) {
        }
    }


    private function replaceIframes($content)
    {
        //TODO: wytestować na 10 stronę
        $findIframes = preg_match_all('%^(<iframe[^>]*>.*?</iframe>)$%im', $content, $matches, PREG_PATTERN_ORDER);

        if ($findIframes) {

            foreach ($matches[0] as $key => $iframe) {
                $foundUrl = preg_match('/src="([^"]+)"/', $iframe, $match);
                if ($foundUrl) {
                    $url = $match[1];

                    if (stripos('player.vimeo.com', $iframe)) {
                        $parsedUrl = parse_url($url);
                        $id = $parsedUrl[2];
                        $newIframe = '<iframe src="https://player.vimeo.com/video/' . $id . '" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                        str_replace($iframe, $newIframe, $content);

                    } elseif (stripos('www.youtube.com', $iframe)) {

                        preg_replace('/\?.+$/, \'\'', '', $url);
                        $newIframe = '<iframe width="640" height="480" frameborder="0" src="' . $url . '" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                        str_replace($iframe, $newIframe, $content);

                    } elseif (stripos('w.soundcloud.com', $iframe)) {
                        preg_match('/url=(.+?)&/)', $url, $match);
                        $newIframe = '<iframe width="640" height="480" frameborder="0" src="https://w.soundcloud.com/player/?url=' . $match[1] . '&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                        str_replace($iframe, $newIframe, $content);

                    } else {
                        str_replace($iframe, '', $content);
                    }
                }


            }

        }

        return $content;

    }

}