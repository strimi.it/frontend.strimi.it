strimi.postCommentForm = function () {
    if (!strimi_app.is_login()) { return null; }

    var loading = function () {
        var content = document.getElementById('post-comment-form');
        var contentForm = document.getElementById('post-comment-form-content');
        var url = content.getAttribute('data-url');
        $.ajax({
            type: 'GET',
            url: url,
            async: true,
            beforeSend: function () {
            },
            success: function (response) {
                if (response.code == 200) {
                    content.innerHTML = response.data.form;
                    contentForm.className += ' active';
                    strimi.language();
                    strimi.autosize();
                    strimi.inputLimitText();
                    strimi.tagAutocomplete();
                    strimi.userAutocomplete();
                    strimi.postCommentResponseForm();
                    strimi_app.commentPost();
                } else if (response.code == 400) {
                }
            },
            error: function () {
            }
        });
    };
    loading();

};

var routeList = [ 'app_post', 'stream_post' ];
if(routeList.includes(_config.route)) {
    strimi.postCommentForm();
}