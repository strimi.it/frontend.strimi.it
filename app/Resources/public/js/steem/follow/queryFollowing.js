strimi_app.queryFollowing = function (username, account, type) {
    if(!username || !account || !type) { return }
    var following = [];
    var followingStop = false;
    var queryFollowing = function (next_account) {
        var messages = JSON.stringify({
            "id": 0,
            "jsonrpc": "2.0",
            "method": "call",
            "params": ["follow_api", "get_following", [username, next_account, type, 100]]
        });
        $.ajax({
            url: 'https://api.steemit.com',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json',
            data: messages,
            success: function(response) {
                var result = response.result;
                if(result && result.length === 100) {
                    var last_account = result[result.length-1].following;
                    queryFollowing(last_account);
                } else {
                    followingStop = true;
                }
                following.push.apply(following, result);
            },
            error: function(error) {
                console.log(error)
            }
        });
    };
    queryFollowing('');

    var followingWait = setInterval(function () {
        if(followingStop) {
            clearInterval(followingWait);
            var buttonFollow = document.getElementsByClassName('follow-user')[0];
            var spanFollow = document.getElementsByClassName('follow-user-text')[0];
            var buttonMute = document.getElementsByClassName('mute-user')[0];
            var spanMute = document.getElementsByClassName('mute-user-text')[0];
            var status = _.find(following, { following: account });
            if(status !== null && typeof status === 'object'){

                if(type === 'blog'){
                    buttonFollow.setAttribute('data-status', 1);
                    spanFollow.innerHTML = strimi.languageText( spanFollow.getAttribute('data-unfollow') );
                    if( !buttonFollow.classList.contains('active') ) {
                        buttonFollow.className += ' active';
                    }
                } else if(type === 'ignore') {
                    buttonMute.setAttribute('data-status', 1);
                    spanMute.innerHTML = strimi.languageText( spanMute.getAttribute('data-unmute') );
                    if( !buttonMute.classList.contains('active') ) {
                        buttonMute.className += ' active';
                    }
                }

            } else {

                if(type === 'blog'){
                    buttonFollow.setAttribute('data-status', 0);
                    buttonFollow.classList.remove('active');
                    spanFollow.innerHTML = strimi.languageText( spanFollow.getAttribute('data-follow') );
                } else if(type === 'ignore') {
                    buttonMute.setAttribute('data-status', 0);
                    buttonMute.classList.remove('active');
                    spanMute.innerHTML = strimi.languageText( spanMute.getAttribute('data-mute') );
                }

            }
        }
    }, 100);

};


var routeList = [
    'user_account', 'user_account_feed', 'user_account_comments',
    'user_account_replies', 'user_account_votes', 'user_account_followers',
    'user_account_following', 'user_account_transfers', 'user_account_settings',
    'stream_post', 'app_post'
];
if(routeList.includes(_config.route) && strimi_app.is_login()) {

    var account = strimi_app.authorization();
    var username = account.username,
        wif = account.private_keys.posting_wif;

    var getAccount = document.getElementById('account-username');
    if(getAccount) {
        var currentAccount = getAccount.getAttribute('data-username');

        if(username !== currentAccount) {
            strimi_app.queryFollowing(username, currentAccount, 'blog');
            strimi_app.queryFollowing(username, currentAccount, 'ignore');
        } else {
            document.getElementsByClassName('strimi_app_active follow')[0].remove();
            document.getElementsByClassName('strimi_app_active mute')[0].remove()
        }




    }



}