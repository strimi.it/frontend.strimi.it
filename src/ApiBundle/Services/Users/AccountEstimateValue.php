<?php

namespace ApiBundle\Services\Users;

class AccountEstimateValue {

    protected $price_format;
    protected $api_connection;
    protected $state;
    protected $open_orders;
    protected $savings_withdraw;
    protected $dynamic_properties;
    protected $format_bytes;
    public function __construct($price_format, $api_connection, $state, $open_orders, $savings_withdraw, $dynamic_properties, $format_bytes){
        $this->price_format = $price_format;
        $this->api_connection = $api_connection;
        $this->state = $state;
        $this->open_orders = $open_orders;
        $this->savings_withdraw = $savings_withdraw;
        $this->dynamic_properties = $dynamic_properties;
        $this->format_bytes = $format_bytes;
    }

    //cache

    /**
     * @param $account
     * @param $gprops
     * @return float|int
     */
    private function vestingSteem($account, $gprops) {
        $accountExplodedVests = explode(' ', $account['vesting_shares']);
        $vests = (float)$accountExplodedVests[0];

        $gpropsExplodedVests = explode(' ', $gprops['total_vesting_shares']);
        $totalVests = $gpropsExplodedVests[0];

        $gpropsExplodedTotalVests = explode(' ', $gprops['total_vesting_fund_steem']);
        $totalVestSteem = $gpropsExplodedTotalVests[0];

        return $totalVestSteem * ($vests / $totalVests);
    }

    /**
     * @param $openOrders
     * @param $assetPrecision
     * @return array
     */
    private function processOrders($openOrders, $assetPrecision) {
        $o = 0;
        if ($openOrders) {
            foreach ($openOrders as $order) {
                if (!empty($order['sell_price']) && strpos($order['sell_price']['base'], "SBD")) {
                    $o += $order['for_sale'];
                }
            }
        }
        $sbdOrders = $o / $assetPrecision;
        $p = 0;
        if ($openOrders) {
            foreach ($openOrders as $order) {
                if (!empty($order['sell_price']) && strpos($order['sell_price']['base'], "STEEM")) {
                    $p += $order['for_sale'];
                }
            }
        }
        $steemOrders = $p / $assetPrecision;
        return [$sbdOrders, $steemOrders];
    }

    /**
     * @param $savingWithdraws
     * @return array
     */
    private function calculateSaving($savingWithdraws) {
        $savingsPending = 0;
        $savingsSbdPending = 0;
        foreach ($savingWithdraws as $withdraw) {
            $explode = explode(' ', $withdraw['amount']);
            $amount = $explode[0];
            $asset = $explode[1];
            if ($asset === 'STEEM') {
                $savingsPending += (float)$amount;
            } elseif ($asset === 'SBD') {
                $savingsSbdPending += (float)$amount;
            }

        }
        return [$savingsPending, $savingsSbdPending];
    }


    /**
     * Calculates users steem bandwidth
     *
     * @param $account
     * @return array
     */
    public function bandwidthUsed($account)
    {

        $totalSeconds = 60*60*24*7;

        try {

            $now = new \DateTime();
            $lastBandwidthUpdate = new \DateTime($account['last_bandwidth_update']);

            $secondsSinceLastUpdate = $now->getTimestamp() - $lastBandwidthUpdate->getTimestamp();
            $newBandwidth = 0;

            if ($secondsSinceLastUpdate < $totalSeconds) {
                $newBandwidth = (($totalSeconds - $secondsSinceLastUpdate) * $account['average_bandwidth']) / $totalSeconds;
                $newBandwidth = round($newBandwidth / 1000000, 2);
            }

            $dynamicGlobalProperties = $this->dynamic_properties->get();
            $steemBandwidth = (float)$dynamicGlobalProperties['max_virtual_bandwidth'] / 1000000;


            $gpropsExplodedVests = explode(' ', $dynamicGlobalProperties['total_vesting_shares']);
            $totalVestingShares = (float)$gpropsExplodedVests[0];

            $accountExplodedVests = explode(' ', $account['vesting_shares']);
            $userVestingShares = (float)$accountExplodedVests[0];

            $receivedVestingShares = explode(' ', $account['received_vesting_shares']);
            $userReceivedVestingShares = (float)$receivedVestingShares[0];

            $delegatedVestingShares = explode(' ', $account['delegated_vesting_shares']);
            $userdelegatedVestingShares = (float)$delegatedVestingShares[0];

            $userShares = $userVestingShares + $userReceivedVestingShares - $userdelegatedVestingShares;

            $bandwidthAllocated = ($userShares * $steemBandwidth) / $totalVestingShares;

            $percentUsed = 100 * $newBandwidth / $bandwidthAllocated;
            $percentRemaining = 100 - (100 * $newBandwidth) / $bandwidthAllocated;
            $percentUsedFormat = $this->format_bytes->formatBandwidth($percentUsed);

            return [
                'size' => [
                    'use' =>  ($percentUsedFormat) ? $percentUsedFormat : 0,
                    'total' => $this->format_bytes->formatBandwidth($bandwidthAllocated),

                ],
                'percent' => [
                    'use' => round($percentUsed, 0),
                    'remaining' => round($percentRemaining, 0)
                ],
            ];
        } catch (\Exception $e) {
        }

    }
    /**
     * @param $account
     * @return mixed
     */
    public function get($account){
        if(!$account) { return null; }

        $username = $account['name'];
        $assetPrecision = 1000;
        $orders = 0;
        $savings = 0;
        $gprops = null;
        $feedPrice = null;
        $openOrders = null;
        $savingsWithdraws = null;
        $vestingSteem = null;

        if (isset($account['gprops'])) { $gprops = $account['gprops']; }
        if (isset($account['feed_price'])) { $feedPrice = $account['feed_price']; }
        if (isset($account['open_orders'])) { $openOrders = $account['open_orders']; }
        if (isset($account['saving_withdraws'])) { $savingsWithdraws = $account['saving_withdraws']; }
        if (isset($account['vesting_steem'])) { $vestingSteem = $account['vesting_steem']; }

        if (!$vestingSteem || !$feedPrice) {
            if (!$gprops || !$feedPrice) {
                $state = $this->state->get($username);
                $gprops = $state['props'];
                $feedPrice = $state['feed_price'];
                $vestingSteem = $this->vestingSteem($account, $gprops);
            }
        } else {
            $vestingSteem = $this->vestingSteem($account, $gprops);
        }

        if (!$openOrders) {
            $getOpenOrders = $this->open_orders->get($username);
            $orders = $this->processOrders($getOpenOrders, $assetPrecision);
        } else {
            $orders = $this->processOrders($openOrders, $assetPrecision);
        }

        if (!$savingsWithdraws) {
            $getSavingsWithdraw = $this->savings_withdraw->get($username);
            $savings = $this->calculateSaving($getSavingsWithdraw);
        } else {
            $savings = $this->calculateSaving($savingsWithdraws);
        }

        $letPricePerSteem = null;
        $base = $feedPrice['base'];
        $quote = $feedPrice['quote'];

        $sbdStrpos = strpos($base, ' SBD');
        $steemStrpos = strpos($quote, ' STEEM');

        $pricePerSteem = 0;
        if ($sbdStrpos && $steemStrpos) {
            $explodeBase = explode(' ', $base);
            $pricePerSteem = (float)$explodeBase[0];
        }

        $savingsBalance = $account['savings_balance'];
        $savingsSbdBalance = $account['savings_sbd_balance'];
        $explodeBalance = explode(' ', $account['balance']);
        $balanceSteem = (float)$explodeBalance[0];

        $explodeSavingsBalanceSteem = explode(' ', $savingsBalance);
        $savingsBalanceSteem = (float)$explodeSavingsBalanceSteem[0];
        $sbdBalance = (float)$account['sbd_balance'];

        $explodeSbdBalanceSavings = explode(' ', $savingsSbdBalance);
        $sbdBalanceSavings = (float)$explodeSbdBalanceSavings[0];

        $conversionValue = 0;
        $currentTime = time();

        $prevValue = null;
        foreach ($account['other_history'] as $key => $value) {

            if ($value[1]['op'][0] !== "convert") {
                break;
            }

            $datetime = \DateTime::createFromFormat('U', $value['timestamp']);
            $timeStamp = $datetime->getTimestamp();
            $finishTime = $timeStamp + 86400000 * 3.5; // add 3.5day conversion delay

            if ($finishTime < $currentTime) {
                break;
            }

            $itemAmount = $value[1]['op'][1]['amount'];
            $amount = (float)str_replace('_SBD', '', $itemAmount);
            $conversionValue += $amount;

            $prevValue = $value;
        }

        $totalSbd = $sbdBalance + $sbdBalanceSavings + $conversionValue;
        if (!empty($savings['savings_sbd_pending'])) {
            $totalSbd += $savings['savings_sbd_pending'];
        }
        if (!empty($orders['sbdOrders'])) {
            $totalSbd += $orders['sbdOrders'];
        }

        $totalSteem = $vestingSteem + $balanceSteem + $savingsBalanceSteem;
        if (!empty($savings['savings_pending'])) {
            $totalSteem += $savings['savings_pending'];
        }

        if (!empty($orders['steemOrders'])) {
            $totalSteem += $orders['steemOrders'];
        }

        return $this->price_format->parser($totalSteem * $pricePerSteem + $totalSbd, 2);
    }

}