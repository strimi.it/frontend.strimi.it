strimi.nextAccountFollows = function () {
    var nextButton = document.getElementById('follows-next-button');
    var loadingButton = document.getElementsByClassName('loading-oval-button')[0];
    if(nextButton){
        nextButton.onclick = function(){
            var status = nextButton.getAttribute('data-status');
            if(status == 0){
                $.ajax({
                    type: 'GET',
                    url: nextButton.getAttribute('data-url'),
                    async: true,
                    beforeSend: function() {
                        nextButton.setAttribute('data-status', 1);
                        nextButton.className += ' active';
                        loadingButton.classList.remove('hidden');
                    },
                    success: function(getResponse) {
                        if (getResponse.code == 200) {

                            var content = document.getElementsByClassName('user-follow-lists')[0];
                            var lastVote = content.getElementsByClassName('col-lg-6');
                            lastVote = lastVote[lastVote.length - 1];

                            lastVote.insertAdjacentHTML('afterEnd', getResponse.data.follows);
                            nextButton.setAttribute('data-url', getResponse.data.next_url);
                            nextButton.setAttribute('data-status', 0);
                            nextButton.classList.remove('active');
                            loadingButton.classList.className += ' hidden';
                            strimi.imageLazyload();
                            strimi.momentTimezone();
                            strimi.language();

                        } else if (getResponse.code == 400) {
                        }
                    },
                    error: function() {
                    }
                });
            }
        };
    }
};
var routeList = [
    'user_account_followers',
    'user_account_following'
];
if(routeList.includes(_config.route)) {
    strimi.nextAccountFollows();
}