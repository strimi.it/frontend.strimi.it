<?php

namespace ApiBundle\Services\Posts\Stream;

class Post {

    protected $json_parser;
    protected $api_connection;
    protected $memcached;
    protected $defaults;
    protected $stream;
    protected $paginator;
    protected $app_blocking;
    public function __construct($jsonParser, $apiConnection, $cacheMemcached, $defaults, $stream, $paginator, $app_blocking){
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
        $this->defaults = $defaults;
        $this->stream = $stream;
        $this->paginator = $paginator;
        $this->app_blocking = $app_blocking;
    }

    /**
     * @param $author
     * @param $permlink
     * @return null
     */
    private function query($author, $permlink){
        if(!$author || !$permlink) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'comments', [
                'query' => [
                    'authorName' => $author,
                    'permlink'   => $permlink
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents())['result'];

            return $result;

        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @param $author
     * @param $permlink
     * @return null
     */
    private function content($author, $permlink){
        if(!$author || !$permlink) { return null; }
        $result = $this->query($author, $permlink);
        if(is_array($result) && count($result) > 0) {
            foreach($result as &$single){
                $single = $this->defaults->parserContent($single);
            }
        }
        return $this->defaults->isStrimiPost($result);
    }

    /**
     * @param $key
     * @param $author
     * @param $permlink
     * @return null
     */
    private function contentCache(){

        //md5(microtime().rand());
        $key = 'stream_posts_cache';
        $author = $this->stream['author'];
        $permlink = $this->stream['permlink'];

        $comments = $this->memcached->get($key);
        if(!$comments) {
            $value = $this->content($author, $permlink);
            $comments = $this->memcached->set($key, $value, 15, 'seconds'); // 60s
        }
        if(!$comments) {
            return $this->content($author, $permlink);
        }
        $posts = $this->memcached->get($key);
        foreach($posts as $elementKey => &$element) {
            $isUserBanned = $this->app_blocking->isUserBanned($element['author']);
            if($isUserBanned) {
                unset($posts[$elementKey]);
            }
        }
        return $posts;
    }

    private function sorting($content, $sorting){
        if(!$content || !$sorting) { return null; }
        if(!empty($content) && count($content) > 0) {
            if($sorting == 'price') {
                foreach ($content as $key => $row) {
                    $price[$key] = $row['pending_payout_value'];
                }
                array_multisort($price, SORT_NUMERIC, $content);
                $content = array_reverse($content, true);

            } elseif($sorting == 'new') {
                usort($content, function($a, $b) {
                    $first = new \DateTime($a['created']);
                    $second = new \DateTime($b['created']);
                    if ($first == $second) { return null; }
                    return $first > $second ? -1 : 1;
                });
            } elseif($sorting == 'old') {
                usort($content, function($a, $b) {
                    $first = new \DateTime($a['created']);
                    $second = new \DateTime($b['created']);
                    if ($first == $second) { return null; }
                    return $first < $second ? -1 : 1;
                });
            }
        }
        return $content;
    }

    /**
     * @param $author
     * @param $permlink
     * @param $sorting
     * @return array|null
     */
    public function get(){
        $posts = $this->contentCache();
        $content = $this->sorting($posts, 'new');
        $result = $this->paginator->getPackage($content, 0, $this->stream['limit']);
        return $result;
    }

    public function getPackageNext($data = [], $from = 0, $limit = 35) {
        array_shift($data);
        return array_slice($data, $from, $limit);
    }

    /**
     * @param $name
     * @param $package
     * @return null
     */
    public function getNext($package, $sorting){
        if(!$package) { return null; }
        $posts = $this->contentCache();
        $content = $this->sorting($posts, $sorting);
        return $this->paginator->getPackageNext(
            $content, $package, $this->stream['limit']
        );
    }

    /**
     * @param $name
     * @param $package
     * @return null
     */
    public function getNewest($limit){
        if(!$limit) { return null; }
        $posts = $this->defaults->createdByTag('strimi-stream');
        $isStream = $this->defaults->isStrimiPost($posts);
        return $this->paginator->getPackage($isStream, 0, $limit);
    }


}