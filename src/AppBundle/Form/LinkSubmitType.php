<?php

namespace AppBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LinkSubmitType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('link', UrlType::class, array(
                'required' => true,
                //'mapped' => false,
                'trim' => true,
                'attr' => array(
                    'length_min' => 10,
                    'length_max' => 180,
                    'autocomplete' => 'off'
                )
            ))
        ;
    }

    public function getBlockPrefix() {
        return '';
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\EntityFake\Link',
            'validation_groups' => 'adding_link',
            'csrf_protection' => true,
            'csrf_field_name' => '_token'
        ));
    }

}