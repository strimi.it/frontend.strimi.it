strimi_app.accountVote = function () {
    if(!strimi_app.is_login()) { return; }
    var account = strimi_app.authorization();
    strimi_app.api.getAccountVotes(account.username, function(err, result) {
        if(result && result !== 'undefined') {
            if (result.length > 0) {
                for (var i = 0; i < result.length; i++) {


                    var vote = document.querySelector('[data-short-permlink="' + result[i].authorperm + '"]');
                    //if(vote && result[i].weight > 0) {
                    if(vote ) {

                        var buttonVote = vote.getAttribute('data-id');
                        var buttonPost = document.getElementsByClassName('vote-post-' + buttonVote)[0];
                        var buttonComment = document.getElementsByClassName('vote-comment-' + buttonVote)[0];
                        if(buttonPost && !buttonPost.classList.contains('active')) {

                            var weight = result[i].weight;
                            if(weight > 0) {
                                buttonPost.className += ' active';
                            } else {
                                buttonPost.className += ' active_down';
                            }


                            buttonPost.setAttribute('vote-status', true)
                        }
                        if(buttonComment && !buttonComment.classList.contains('active')) {
                            buttonComment.className += ' active';
                        }

                    }
                }
            }
        }
    });
};
strimi_app.accountVote();