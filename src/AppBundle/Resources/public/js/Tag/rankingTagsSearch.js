strimi.RankingTagsSearch = function () {

    var content = document.getElementsByTagName('tbody')[0];
    var nextButtonContent = document.getElementsByClassName('tags-next')[0];
    var nextButton = document.getElementById('tags-next-button');

    var url = function () {
        var uri = document.getElementsByClassName('page-tags-search-content')[0].getAttribute('data-url');
            return uri.replace("strimi", "");
    };

    document.getElementById('resetTagsSearch').onclick = function(){

        $.ajax({
            type: 'GET',
            url: this.getAttribute('data-url'),
            async: true,
            beforeSend: function() {},
            success: function(getResponse) {
                if (getResponse.code == 200) {
                    content.innerHTML = getResponse.data;

                    if(nextButtonContent.classList.contains('hidden')){
                        nextButtonContent.classList.remove('hidden');
                    }
                    nextButton.setAttribute('data-url', nextButton.getAttribute('data-url-default'));

                } else if (getResponse.code == 400) {
                }
            },
            error: function() {
            }
        });

    };

    $(".suggest_tags_raking_page_autocomplete").on("change keyup paste", function(){

        var name = $(this).val();
        var nameLength = name.length;

        if(nameLength > 0){

            if(!nextButtonContent.classList.contains('hidden')){
                nextButtonContent.className += ' hidden';
            }

            $.ajax({
                type: 'GET',
                url: url() + $(this).val(),
                async: true,
                beforeSend: function() {},
                success: function(getResponse) {
                    if (getResponse.code == 200) {
                        content.innerHTML = getResponse.data;
                    } else if (getResponse.code == 400) {
                    }
                },
                error: function() {
                }
            });
        }

    });

};

if(_config.route === 'tag_ranking') {
    strimi.RankingTagsSearch();
}

