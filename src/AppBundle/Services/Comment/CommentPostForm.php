<?php

namespace AppBundle\Services\Comment;
use AppBundle\EntityFake\Comment;
use AppBundle\Form\CommentType;


class CommentPostForm {

    protected $form_factory;
    protected $router;
    protected $templating;
    protected $request_stack;
    public function __construct($form_factory, $router, $templating, $request_stack) {
        $this->form_factory = $form_factory;
        $this->router = $router;
        $this->templating = $templating;
        $this->request_stack = $request_stack;
    }

    public function createForm(){
        return $this->form_factory->create(CommentType::class, new Comment(), array(
            'action' => '',
            'method' => 'POST',
        ));
    }

    public function renderForm($response){
        return $this->templating->render(
            'AppBundle:Comment/Add:form.html.twig',
            array(
                'form' => $this->createForm()->createView(),
                'response' => $response
            )
        );
    }

}