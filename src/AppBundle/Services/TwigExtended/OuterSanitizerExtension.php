<?php

namespace AppBundle\Services\TwigExtended;

class OuterSanitizerExtension extends \Twig_Extension {

    protected $container;
    public function __construct($container){
        $this->container = $container;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('outerSanitizer', array($this, 'content'), array('is_safe' => array('html'))),
        );
    }

    public function content($content) {
        if(!$content) { return null; }
        return $this->container->get('app.sanitizer')->outerSanitizer($content);
    }

    public function getName() {
        return 'outerSanitizer';
    }

}