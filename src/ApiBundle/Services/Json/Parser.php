<?php

namespace ApiBundle\Services\Json;

class Parser {

    /**
     * @param $object
     * @return mixed|null
     */
    public function singleObject($object) {
        if(!$object) { return null; }
        try{
            return json_decode($object, true);
        }catch(\Exception $ex){
        }
    }

}