<?php

namespace AppBundle\Controller\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ContactController extends Controller {

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        return $this->render(
            'AppBundle:Page:contact.html.twig'
        );
    }

}
