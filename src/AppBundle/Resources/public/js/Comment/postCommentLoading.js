strimi.PostCommentLoading = function () {
    var commentsContent = document.getElementById('post-comments-content');
    var buttons = document.getElementsByClassName('button-sorting-comments');
    var setCommentsContent = function (url) {
        $.ajax({
            type: 'GET',
            url: url,
            async: true,
            beforeSend: function() {
            },
            success: function(getResponse) {

                if(getResponse.data.count === 0){
                    document.getElementsByClassName('comments-loading')[0].remove();
                    document.getElementsByClassName('comments-null')[0].className += ' active';
                } else if (getResponse.code == 200) {
                    commentsContent.innerHTML = getResponse.data.comments;
                    strimi.imageLazyload();
                    strimi.momentTimezone();
                    strimi.commentsCount();
                    strimi.language();
                    strimi_app.reloaded();
                    strimi_app.commentPost();
                    strimi.postCommentResponseForm();
                    strimi_app.scrollToComment();
                    strimi.imageStreamLazy();
                    strimi.outerSanitizerComment();
                } else if (getResponse.code == 400) {
                }
            },
            error: function() {
            }
        });
    };
    /* Set Default */
    setCommentsContent(buttons[0].getAttribute('data-url'));
    /* Change Sorting */
    $('.button-sorting-comments').click(function() {
        /* Get Comments */
        setCommentsContent($(this).attr('data-url'));
        /* Clean Active */
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].classList.remove('active');
        }
        /* Current Active */
        $(this).addClass('active');
    });
};

var routeList = [ 'app_post', 'stream_post' ];
if(routeList.includes(_config.route)) {
    strimi.PostCommentLoading();
}
