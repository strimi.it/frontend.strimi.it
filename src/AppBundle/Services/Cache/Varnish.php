<?php

namespace AppBundle\Services\Cache;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class Varnish {

    protected $requestStack;
    public function __construct(RequestStack $requestStack){
        $this->requestStack = $requestStack;
    }

    private function reverseExpires($setResponse){
        if(empty($getDate)) {
            return false;
        }
        $getDate = new \DateTime();
        $getDate->modify('-100 years');
        $setResponse->setExpires($getDate);
        return $setResponse;
    }

    private function disabledCache($setResponse){
        if(empty($setResponse)) {
            return false;
        }
        $setResponse->headers->addCacheControlDirective('no-cache', true);
        $setResponse->headers->addCacheControlDirective('must-revalidate', true);
        $setResponse->setPrivate();
        $this->reverseExpires($setResponse);
        return $setResponse;
    }

    /**
     * @param $setResponse
     * @param $getDate
     * @param int $getValue
     * @param string $getType
     * @return bool
     */
    private function enbaledCache($setResponse, $getDate, $getValue = 60, $getType = 'seconds'){
        if(empty($setResponse) || empty($getDate)){
            return false;
        }
        $date = ($getType === 'seconds') ? $getDate->modify('+' . $getValue . ' seconds') : $getDate->modify('+' . $getValue . ' minutes');

        $setResponse->setExpires($date);
        $setResponse->setPublic();
        return $setResponse;
    }

    private function developHost($event){

        $requestStack = clone $this->requestStack;
        if(!empty($requestStack)) {
            $pop = $requestStack->pop();
            if($pop) {
                $host = $pop->getHost();
                $hostLists = [
                    'dv.strimi.pl',
                    'rc.strimi.pl'
                ];
                if($host === $hostLists[0]) {
                    $setResponse = $event->getResponse();
                    $setResponse->headers->set("X-isHit", "develop_mode");
                    $this->disabledCache($setResponse);
                    return $event->setResponse($setResponse);
                }
            }

        }

    }

    public function onKernelResponse(FilterResponseEvent $event) {

        /* Prepare Response */
        $setResponse = $event->getResponse();
        $setResponse->headers->set('Connection', 'keep-alive');
        $setResponse->headers->set('Vary', 'Cookie');

        $setResponse->headers->set("X-isHit", "true");

        /* Get Current Route */
        $getCurrentRequest = $this->requestStack->getCurrentRequest();

        if($getCurrentRequest) {
            $routeName = $getCurrentRequest->get('_route');

            /* Get Current Date */
            $getDate = new \DateTime();

            /* Default Posts */
            $defaultPosts = array(
                'app_homepage',
                'app_created',
                'app_hot',
                'app_promoted',
                'app_posts_by_tag_trending',
                'app_posts_by_tag_created',
                'app_posts_by_tag_hot',
                'app_posts_by_tag_promoted'
            );

            if(in_array($routeName, $defaultPosts) && $routeName != 'app_created') {
                $this->enbaledCache($setResponse, $getDate, 3, 'minutes');
            } elseif ($routeName === 'app_created' || $routeName === 'app_posts_by_tag_created') {
                $this->enbaledCache($setResponse, $getDate, 15);
            } elseif ($routeName === 'app_post' || $routeName === 'app_ajax_post_comments') {
                $this->enbaledCache($setResponse, $getDate, 15, 'seconds');
            } elseif ($routeName === 'stream_homepage'|| $routeName === 'stream_post') {
                $this->enbaledCache($setResponse, $getDate, 15, 'seconds');
            }

            $rankingPage = array(
                'user_ranking',
                'tag_ranking'
            );
            /* Ranking Page */
            if(in_array($routeName, $rankingPage)) {
                $this->enbaledCache($setResponse, $getDate, 15, 'minutes');
            }

            /* User Page */
            $userPage = array(
                'user_account',
                'user_account_comments',
                'user_account_replies',
                'user_account_votes',
                'user_account_followers',
                'user_account_following',
                'user_account_transfers'
            );
            if(in_array($routeName, $userPage)) {
                $this->enbaledCache($setResponse, $getDate, 30, 'seconds');
            }

            /* Static Page */
            if(preg_match("/app_page_/i", $routeName)) {
                $this->enbaledCache($setResponse, $getDate, 60, 'minutes');
            }

        }

        /* Disabled Varnish */
        //$this->developHost($event);

        return $event->setResponse($setResponse);

    }
}