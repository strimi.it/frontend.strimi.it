strimi_app.postPreview = function (content) {
    if(!strimi_app.is_login || !content) { return false; }

    var data = JSON.parse(content);

    /* Type */
    var type = data.type;
    if( type !== null && typeof type === 'object' ){
        type =  {
            type: data.type[0],
            type_from: data.type[1],
            width: data.width,
            height: data.height,
            size: data.size
        }
    } else {
        type = data.type;
    }

    /* Video ID */
    var videoID;
    if( type.id !== null ){
        videoID = data.id;
    }

    var strimi_metadata = JSON.stringify({
        thumb: data.thumb,
        source: data.source,
        domain: data.domain,
        type: type,
        video: videoID
    });

    localStorage.setItem("link_preview", strimi_metadata);
};