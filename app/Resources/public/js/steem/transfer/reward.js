strimi_app.rewardAccount = function () {

    if(!strimi_app.is_login()) { return; }

    var account = strimi_app.authorization();
    var username = account.username,
        wif = account.private_keys.posting_wif;

    var contentCount = function (count) {
        if(!count) { return '' };
        return '<span class="count">' + count + '</span>';
    };

    var content = document.getElementById('receive-rewards');

    var reward_sbd_balance;
    var reward_sbd_balance_format;
    var reward_steem_balance;
    var reward_steem_balance_format;
    var reward_vesting_balance;
    var reward_steem_power;
    var reward_steem_power_format;

    strimi_app.api.getAccounts([username], function(error, result) {
        if (result && result !== 'undefined') {
            var account = result[0];
            var limit =  0.005;
            reward_sbd_balance = account.reward_sbd_balance;
            reward_sbd_balance_format = Number(reward_sbd_balance.replace(' SBD', ''));
            reward_steem_balance = account.reward_steem_balance;
            reward_steem_balance_format = Number(reward_steem_balance.replace(' STEEM', ''));
            reward_vesting_balance = account.reward_vesting_balance;
            reward_steem_power = account.reward_vesting_steem.replace(' STEEM', ' SP');
            reward_steem_power_format = Number(reward_steem_power.replace(' SP', ''));

            var sum = reward_sbd_balance_format + reward_steem_balance_format + reward_steem_power_format;
            if(sum > limit) {
                if(reward_sbd_balance_format > limit) {
                    $('#receive-rewards').append(contentCount(reward_sbd_balance));
                }
                if(reward_steem_balance_format > limit) {
                    $('#receive-rewards').append(contentCount(reward_steem_balance));
                }
                if(reward_steem_power_format > limit) {
                    $('#receive-rewards').append(contentCount(reward_steem_power));
                }
                content.className += ' active';
            }
        } else if (error && error !== 'undefined') {
            console.log(error);
        }
    });

    content.onclick = function () {

        var currentSBD = document.getElementById('balance-transfer-sbd');
        var currentSBDbalance = currentSBD.getAttribute('data-value');
        var newSBD = Number(currentSBDbalance) + Number(reward_sbd_balance_format);
        currentSBD.setAttribute('data-value', newSBD);
        currentSBD.innerHTML = newSBD + ' SBD';

        var currentSP = document.getElementById('balance-transfer-steem-power');
        var currentSPbalance = currentSP.getAttribute('data-value');
        var newSP = Number(currentSPbalance) + Number(reward_steem_power_format);
        currentSP.setAttribute('data-value', newSP);
        currentSP.innerHTML = newSP;

        strimi_app.broadcast.claimRewardBalance(wif, username, reward_steem_balance, reward_sbd_balance, reward_vesting_balance, function(error, result) {
            if (result && result !== 'undefined') {
                console.log('ok');
            } else if (error && error !== 'undefined') {
            }
        });

        content.classList.remove('active');
        var count = content.querySelectorAll('.count');
        for (var i = 0; i < count.length; i++) {
            count[i].remove();
        }

    };

};
var routeList = [ 'user_account_transfers' ];
if(routeList.includes(_config.route)) {
    var account = strimi_app.authorization();
    var username = account.username;
    var uri = window.location.href;
    if(uri.search("@" + username) !== -1) {
        strimi_app.rewardAccount();
    }
}