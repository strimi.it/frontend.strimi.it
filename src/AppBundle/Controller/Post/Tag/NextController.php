<?php

namespace AppBundle\Controller\Post\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NextController extends Controller {

    /**
     * @param $author
     * @param $permlink
     * @param $path
     * @param $tag
     * @return null
     */
    public function indexAction($author, $permlink, $path, $tag) {
        if(!$author || !$permlink || !$path || !$tag) { return null; }

        $code = 400;
        $posts = $this->get('api.posts.next')->content($author, $permlink, $path, $tag);
        $postsContent = null;
        $nextUri = null;
        if($posts) {
            $code = 200;
            $lastPost =  end($posts);
            $nextUri = $this->generateUrl('app_ajax_posts_by_tag_next', array(
                'author'   => $lastPost['author'],
                'permlink' => $lastPost['permlink'],
                'path'     => $path,
                'tag'      => $tag
            ));

            $postsContent = $this->renderView(
                'AppBundle:Post/List:posts.html.twig',
                array(
                    'posts' => $posts,
                    'appRoute' => null
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'posts'    => $postsContent,
                'next_url' => $nextUri
            ),
            $code
        );
    }




}
