strimi.RankingAccountsSearch = function () {

    var content = document.getElementsByTagName('tbody')[0];
    var nextButtonContent = document.getElementsByClassName('account-ranking-next')[0];
    var nextButton = document.getElementById('ranking-next-button');

    var url = function () {
        var uri = document.getElementsByClassName('page-accounts-search-content')[0].getAttribute('data-url');
            return uri.replace("strimi", "");
    };

    document.getElementById('resetRankingAccountsSearch').onclick = function(){
        $.ajax({
            type: 'GET',
            url: this.getAttribute('data-url'),
            async: true,
            beforeSend: function() {},
            success: function(getResponse) {
                if (getResponse.code == 200) {
                    content.innerHTML = getResponse.data.accounts;
                    if(nextButtonContent.classList.contains('hidden')){
                        nextButtonContent.classList.remove('hidden');
                    }
                    nextButton.setAttribute('data-url', nextButton.getAttribute('data-url-default'));
                    document.getElementsByClassName('suggest_accounts_raking_page_autocomplete')[0].value = null;
                    strimi.imageLazyload();
                    strimi.language();
                } else if (getResponse.code == 400) {
                }
            },
            error: function() {
            }
        });

    };

    $(".suggest_accounts_raking_page_autocomplete").on("change keyup paste", function(){
        var name = $(this).val();
        var nameLength = name.length;
        if(nameLength > 0){
            if(!nextButtonContent.classList.contains('hidden')){
                nextButtonContent.className += ' hidden';
            }
            $.ajax({
                type: 'GET',
                url: url() + $(this).val(),
                async: true,
                beforeSend: function() {},
                success: function(getResponse) {
                    if (getResponse.code == 200) {
                        content.innerHTML = getResponse.data;
                        strimi.imageLazyload();
                        strimi.language();
                    } else if (getResponse.code == 400) {
                    }
                },
                error: function() {
                }});
        }
    });
};

if(_config.route === 'user_ranking') {
    strimi.RankingAccountsSearch();
}

