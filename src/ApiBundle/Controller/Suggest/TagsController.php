<?php

namespace ApiBundle\Controller\Suggest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TagsController extends Controller {

    public function indexAction($name) {
        if(!$name) { return null; }
        $code = 200;
        $tags = $this->get('api.elastic.suggest.tags')->get($name, 25, false);
        $tagsContent = null;
        if($tags) {
            $tagsContent = $this->renderView(
                'AppBundle:Tag:tags_list.html.twig',
                array(
                    'tags' => $tags
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            $tagsContent,
            $code
        );
    }
}
