<?php

namespace ApiBundle\Services\Posts;

class Defaults {

    protected $api_connection;
    protected $memcached;
    protected $parser_sbd;
    protected $json_parser;
    protected $reputation;
    protected $format_bytes;
    protected $vote_worth;
    protected $stream_tag;
    protected $core_domain;
    public function __construct($api_connection, $memcached, $parser_sbd, $json_parser, $reputation, $format_bytes, $vote_worth, $stream_tag, $core_domain){
        $this->api_connection = $api_connection;
        $this->memcached = $memcached;
        $this->parser_sbd = $parser_sbd;
        $this->json_parser = $json_parser;
        $this->reputation = $reputation;
        $this->format_bytes = $format_bytes;
        $this->vote_worth = $vote_worth;
        $this->stream_tag = $stream_tag['category'];
        $this->core_domain = $core_domain;
    }

    /**
     * @param $vote
     * @param bool $account
     * @return array|null
     */
    public function votePercent($vote, $account = false){
        if(!$vote) { return null; }

        $convert = (int)number_format($vote['percent'] / 100, 0);

        $vote['percent'] = [
            'percent' => $vote['percent'],
            'convert' => $convert,
        ];

        if(!$account) {
            return $vote;
        }
        return $vote['percent'];

    }

    /**
     * @param $vote
     * @return null
     */
    private function voteReputation($vote){
        if(!$vote) { return null; }
        $vote['reputation'] = [
            'reputation' => $vote['reputation'],
            'convert'    => $this->reputation->convert($vote['reputation'])
        ];
        return $vote;
    }

    /**
     * @param $votes
     * @return array|null
     */
    private function postVotes($votes){
        if(!$votes) { return null; }
        $plusCount = 0;
        $plusPercent = 0;
        $minusCount = 0;
        $minusPercent = 0;

        if(is_array($votes)) {
            foreach($votes as &$vote) {
                $vote = $this->votePercent($vote);
                $vote = $this->voteReputation($vote);
                if($vote['percent']['convert'] > 0) {
                    $plusCount = $plusCount + 1;
                    $plusPercent = $plusPercent + $vote['percent']['convert'];
                } elseif($vote['percent']['convert'] < 0) {
                    $minusCount = $minusCount + 1;
                    $minusPercent = $minusPercent - $vote['percent']['convert'];
                }
            }
        }

        return [
            'plus' => [
                'count'   => $plusCount,
                'percent' => $plusPercent
            ],
            'minus' => [
                'count'   => $minusCount,
                'percent' => $minusPercent
            ],
            'votes' => $votes
        ];
    }

    /**
     * @param $content
     * @return array|null
     */
    private function payout($content){
        if(!$content) { return null; }

        $createdDatetime = new \DateTime($content['created']);
        $cashoutDatatime = new \DateTime($content['created']);
        $cashoutDatatime = $cashoutDatatime->modify('+7 day');
        $nowDataTime = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone('Europe/Warsaw'));
        $status = false;

        if( $cashoutDatatime->getTimestamp() < $nowDataTime->getTimestamp() ) {
            $status = true;
        }

        return [
            'createdDatetime' => $createdDatetime,
            'cashoutDatatime' => $cashoutDatatime,
            'nowDataTime' => $nowDataTime,
            'createdDatetime_tp' => $createdDatetime->getTimestamp(),
            'cashoutDatatime_tp' => $cashoutDatatime ->getTimestamp(),
            'nowDataTime_tp' => $nowDataTime ->getTimestamp(),
            'status' => $status
        ];

    }

    /**
     * @param $content
     * @return array()
     */
    private function rootUrl($content){
        if(!$content) { return null; }
        $url = preg_replace('/@/', '', ltrim($content, '/'));
        if (strpos($url, '#') !== false) {
            $url = substr($url, 0, strpos($url, "#"));
        }
        $url = explode('/', $url);
        return [
            'category' => $url[0],
            'author'   => $url[1],
            'permlink' => $url[2],
            'url'      => $content
        ];
    }

    public function isStrimiPostValid($post){
        if(empty($post)) { return null; }
        if(empty($post['strimi_metadata']) ||
            $post['json_metadata']['app'] !== 'strimi/2.0' ||
            $post['beneficiaries'][0]['account'] !== 'strimi') {
            return false;
        }
        return true;
    }

    public function isStrimiPost($posts) {
        if(empty($posts)) { return null; }
        foreach( $posts as $key => $post) {
            $isValid = $this->isStrimiPostValid($post);
            if(!$isValid) {
                unset($posts[$key]);
            }
        }
        return $posts;
    }

    /**
     * @param $content
     * @return null
     */
    public function parserContent($content){
        if(!$content) { return null; }

        if(!empty($content['json_metadata'])){
            $content['json_metadata'] = $this->json_parser->singleObject($content['json_metadata']);
            if(!empty($content['json_metadata']['strimi_metadata']) ){
                $content['strimi_metadata'] = $this->json_parser->singleObject($content['json_metadata']['strimi_metadata']);
                if(!empty($content['strimi_metadata']['type']) && !empty($content['strimi_metadata']['type']['size'])) {
                    $content['strimi_metadata']['type']['size'] = $this->format_bytes->format($content['strimi_metadata']['type']['size']);
                }
                unset($content['json_metadata']['strimi_metadata']);
            }
        }
        if(!empty($content['author_reputation'])) {
            $content['author_reputation'] = [
                'author_reputation' => $content['author_reputation'],
                'author_reputation_format'    => $this->reputation->convert($content['author_reputation'])
            ];
        }

        if(!empty($content['pending_payout_value'])){
            $content['pending_payout_value'] = $this->parser_sbd->parser($content['pending_payout_value']);
        }
        if(!empty($content['total_payout_value'])){
            $content['total_payout_value'] = $this->parser_sbd->parser($content['total_payout_value']);
        }
        if(!empty($content['curator_payout_value'])){
            $content['curator_payout_value'] = $this->parser_sbd->parser($content['curator_payout_value']);
        }
        if(!empty($content['max_accepted_payout'])){
            if($content['max_accepted_payout'] === '0.000 SBD') {
                $content['max_accepted_payout'] = 'declined';
            } else {
                $content['max_accepted_payout'] = $this->parser_sbd->parser($content['curator_payout_value']);
            }
        }
        if(!empty($content['active_votes'])) {
            $content['active_votes'] = $this->postVotes($content['active_votes']);
        }
        if(!empty($content['url'])) {
            $content['root_url_parser'] = $this->rootUrl($content['url']);
        }
        if($this->isStrimiPostValid($content)) {
            $content['is_strimi_post'] = true;
        } else {
            $content['is_strimi_post'] = false;
        }
        return $content;
    }

    /**
     * @param $path
     * @return null
     */
    private function get($path){
        if(!$path) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', $path, [
                'query' => [
                    'limit'    => 35
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result'];
            if(is_array($result)) {
                foreach ($result as &$singleObject) {
                    $singleObject = $this->parserContent($singleObject);
                }
            }
            return $result;
        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @param $path
     * @param $tag
     * @return null
     */
    public function getByTag($path, $tag, $limit = 35){
        if(!$path || !$tag) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', $path, [
                'query' => [
                    'tag' => $tag,
                    'limit'    => $limit
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result'];
            if(is_array($result)) {
                foreach($result as &$singleObject) {
                    $singleObject = $this->parserContent($singleObject);
                }
            }

            $posts = $result;
            if($tag == 'strimi' || (strpos($tag, 'strimi-') !== false)) {
                $posts = $this->isStrimiPost($result);
            }
            return $posts;

        }catch(\Exception $ex){
        }
    }

    /**
     * @param $name
     * @return null
     */
    private function posts($name){
        if(!$name) { return null; }
        return $this->get('posts/' . $name);
    }

    /**
     * @param $name
     * @param $tag
     * @return null
     */
    private function postsTag($name, $tag){
        if(!$name || !$tag) { return null; }
        return $this->getByTag('posts/' . $name, $tag);
    }

    /**
     * @param $key
     * @param $postsName
     * @return null
     */
    private function contentCache($key, $postsName){
        if(!$key || !$postsName) { return null; }
        $posts = $this->memcached->get($key);
        if(!$posts) {
            $value = $this->posts($postsName);
            if($postsName === 'created') {
                $this->memcached->set($key, $value, 25, 'seconds');
            } else {
                $this->memcached->set($key, $value, 3, 'minutes');
            }
            $posts = $this->memcached->get($key);
        }
        if(!$posts) {
            return $this->posts($postsName);
        }
        return $this->memcached->get($key);
    }

    /**
     * @param $key
     * @param $postsName
     * @param $tag
     * @return null
     */
    private function contentCacheTags($key, $postsName, $tag){
        if(!$key || !$postsName || !$tag) { return null; }



        $posts = $this->memcached->get($key);
        if(!$posts) {
            $value = $this->postsTag($postsName, $tag);
            if($postsName === 'created') {
                $this->memcached->set($key, $value, 25, 'seconds');
            } else {
                $this->memcached->set($key, $value, 3, 'minutes');
            }
            $posts = $this->memcached->get($key);
        }
        if(!$posts) {
            return $this->posts($postsName);
        }
        return $this->memcached->get($key);
    }

    /**
     * @return null
     */
    public function trending(){
        $domain = $this->core_domain->currentDomainLocale();
        if($domain == 'en') {
            return $this->contentCache('posts_trending', 'trending');
        } elseif ($domain == 'pl') {
            return $this->contentCacheTags('posts_trending_' . 'strimi', 'trending', 'strimi');
        }
        return $this->contentCache('posts_trending', 'trending');
    }

    /**
     * @param $tag
     * @return null
     */
    public function trendingByTag($tag){
        if(!$tag) { return null; }
        return $this->contentCacheTags('posts_trending_' . $tag, 'trending', $tag);
    }

    /**
     * @return null
     */
    public function created(){
        $domain = $this->core_domain->currentDomainLocale();
        if($domain == 'en') {
            return $this->contentCache('posts_created', 'created');
        } elseif ($domain == 'pl') {
            return $this->contentCacheTags('posts_created_' . 'strimi', 'created', 'strimi');
        }
        return $this->contentCache('posts_created', 'created');
    }

    /**
     * @param $tag
     * @return null
     */
    public function createdByTag($tag){
        if(!$tag) { return null; }
        return $this->contentCacheTags('posts_created_' . $tag, 'created', $tag);
    }

    /**
     * @return null
     */
    public function hot(){
        $domain = $this->core_domain->currentDomainLocale();
        if($domain == 'en') {
            return $this->contentCache('posts_hot', 'hot');
        } elseif ($domain == 'pl') {
            return $this->contentCacheTags('posts_hot_' . 'strimi', 'hot', 'strimi');
        }
        return $this->contentCache('posts_hot', 'hot');
    }

    /**
     * @param $tag
     * @return null
     */
    public function hotByTag($tag){
        if(!$tag) { return null; }
        return $this->contentCacheTags('posts_hot_' . $tag, 'hot', $tag);
    }

    /**
     * @return null
     */
    public function promoted(){
        $domain = $this->core_domain->currentDomainLocale();
        if($domain == 'en') {
            return $this->contentCache('posts_promoted', 'promoted');
        } elseif ($domain == 'pl') {
            return $this->contentCacheTags('posts_promoted_' . 'strimi', 'promoted', 'strimi');
        }
        return $this->contentCache('posts_promoted', 'promoted');
    }

    /**
     * @param $tag
     * @return null
     */
    public function promotedByTag($tag){
        if(!$tag) { return null; }
        return $this->contentCacheTags('posts_promoted_' . $tag, 'promoted', $tag);
    }

    /**
     * @param $username
     * @return null
     */
    public function accountFeed($username){
        if(!$username) { return null; }
        return $this->contentCacheTags('account_posts_feed_' . md5($username), 'feed', $username);
    }

    /**
     * @param $name
     * @param $limit
     * @return null
     */
    public function renderPostsStream($name, $limit){
        if(!$name || !$limit) { return null; }
        $posts = $this->getByTag('posts/' . $name, $this->stream_tag, $limit);
        return $this->isStrimiPost($posts);
    }


}