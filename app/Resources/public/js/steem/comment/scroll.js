strimi_app.scrollToComment = function () {
    var hash = window.location.hash.substr(1);
    var check = hash.substring(0, hash.indexOf('-'));
    if(check === 'comment') {
        $('html, body').animate({
            scrollTop: $("#" + hash).offset().top - 70
        }, 500);
    }
};