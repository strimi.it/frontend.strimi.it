<?php

namespace ApiBundle\Services\Price;

class SteemDollars {

    protected $parser;
    public function __construct($parser){
        $this->parser = $parser;
    }

    /**
     * @param $content
     * @return null
     */
    public function parser($content){
        if(!$content) { return null; }
        $price = preg_replace('/ SBD/', '', $content);
        return $this->parser->parser($price);
    }

}
