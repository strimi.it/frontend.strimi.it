<?php

namespace ApiBundle\Services\Users;

class Account {

    protected $parser_sbd;
    protected $price_format;
    protected $json_parser;
    protected $api_connection;
    protected $memcached;
    protected $reputation;
    protected $ranking;
    protected $dynamic_properties;
    protected $reward_found;
    protected $parser_steem;
    protected $estimate_value;
    protected $vote_worth;
    protected $fallow_count;
    protected $vest_to_sp;
    protected $properties;
    protected $vesting_delegations;
    protected $app_blocking;

    public function __construct($parserSBD, $priceFormat, $jsonParser, $apiConnection, $cacheMemcached, $reputation, $ranking, $dynamicProperties, $rewardFound, $parserSteem, $estimateValue, $voteWorth, $fallowCount, $vestToSteem, $properties, $vesting_delegations, $app_blocking) {
        $this->parser_sbd = $parserSBD;
        $this->price_format = $priceFormat;
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
        $this->reputation = $reputation;
        $this->ranking = $ranking;
        $this->dynamic_properties = $dynamicProperties;
        $this->reward_found = $rewardFound;
        $this->parser_steem = $parserSteem;
        $this->estimate_value = $estimateValue;
        $this->vote_worth = $voteWorth;
        $this->fallow_count = $fallowCount;
        $this->vest_to_sp = $vestToSteem;
        $this->properties = $properties;
        $this->vesting_delegations = $vesting_delegations;
        $this->app_blocking = $app_blocking;
    }

    /**
     * @param $votingPower
     * @return array|int
     */
    private function votingPowerPercent($votingPower){
        if(!$votingPower) { return 0; }
        if (strlen($votingPower) == 5) {
            return array(
                substr($votingPower, 0, 3),
                substr($votingPower, 0, 3) . '.' . substr($votingPower, 3, 2)
            );
        } elseif (strlen($votingPower) == 4) {
            return array(
                substr($votingPower, 0, 2),
                substr($votingPower, 0, 2) . '.' . substr($votingPower, 2, 4)
            );
        }
        return 0;
    }

    /**
     * @param $account
     * @return null
     */
    private function steemPowerValue($account){
        if(!$account) { return null; }

        $dynamicGlobalProperties = $this->dynamic_properties->get();
        if (!$dynamicGlobalProperties) { return null; }

        $vestingShares = $account['vesting_shares'];
        $vestingShares = explode(' ', $vestingShares)[0];
        $totalVestingShares = $dynamicGlobalProperties['total_vesting_shares'];
        $totalVestingShares = explode(' ', $totalVestingShares)[0];
        $totalVestingFundSteem = $dynamicGlobalProperties['total_vesting_fund_steem'];
        $totalVestingFundSteem = explode(' ', $totalVestingFundSteem)[0];
        $steemPowerValue = $totalVestingFundSteem * ($vestingShares / $totalVestingShares);

        return $this->price_format->parser($steemPowerValue, 3);
    }

    //@ToDo Delegations List -- to later
    private function vestingDelegations($name, $properties) {
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'users/' . $name . '/vestingDelegations');
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result'];
            $properties = $this->properties->get();
            if(is_array($result) && count($result) > 0) {
                foreach($result as &$single) {
                    $single['steem_power'] = $this->vest_to_sp->parser($properties, $single['vesting_shares'], 3);
                }
            }
            return $result;
        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @param $result
     * @param $properties
     * @return null
     */
    private function parserResult($result, $properties) {
        if(!$result) { return null; }

        if(isset($result['json_metadata']) && !empty($result['json_metadata'])) {
            $json_metadata = $this->json_parser->singleObject($result['json_metadata']);
            $result['json_metadata'] = $json_metadata;
            if(isset($json_metadata['profile']) && !empty($json_metadata['profile'])) {
                if(isset($json_metadata['profile']['website']) && !empty($json_metadata['profile']['website'])) {
                    $result['json_metadata']['profile']['website'] = array(
                        'url' => $json_metadata['profile']['website'],
                        'host' => parse_url($json_metadata['profile']['website'])['host']
                    );
                }
            }
        }

        $voting_power_percent = $this->votingPowerPercent($result['voting_power']);
        $result['voting_power'] = [
            'voting_power'         => $result['voting_power'],
            'voting_power_percent' => $voting_power_percent
        ];
        $result['reputation'] = [
            'reputation' => $result['reputation'],
            'convert'    => $this->reputation->convert($result['reputation'])
        ];
        $result['ranking'] = $this->ranking->get($result['name']);
        $result['steem_power'] = $this->steemPowerValue($result);
        $result['steem'] = $this->parser_steem->parser($result['balance']);
        $result['sbd_balance'] = $this->parser_sbd->parser($result['sbd_balance'], 3);
        $result['savings_sbd_balance'] = $this->parser_sbd->parser($result['savings_sbd_balance'], 3);
        $result['account_estimate_value'] = $this->estimate_value->get($result);
        $result['account_bandwidth'] = $this->estimate_value->bandwidthUsed($result);
        $result['vote_worth'] = array(
            'total' => $this->vote_worth->get($result, 100),
            'current' => $this->vote_worth->get($result, $voting_power_percent[0])
        );
        //$result['bandwidth_used'] = $this->estimate_value->bandwidthUsed($result);
        $result['fallow_count'] = $this->fallow_count->count($result['name']);

        $delegated = $this->vest_to_sp->parser($properties, $result['delegated_vesting_shares'], false);
        $received = $this->vest_to_sp->parser($properties, $result['received_vesting_shares'], false);

        $result['vesting_delegations'] = [
            'delegated' => $this->vest_to_sp->parser($properties, $result['delegated_vesting_shares'], 3),
            'delegated_lists' => $this->vesting_delegations->get($result['name']),
            'received' => $this->vest_to_sp->parser($properties, $result['received_vesting_shares'], 3),
            'sum' => $this->price_format->parser($received - $delegated, 3)
        ];

        return $result;
    }

    /**
     * @param $result
     * @return null
     */
    private function followsParser($result) {
        if(!$result) { return null; }
        if(isset($result['json_metadata']) && !empty($result['json_metadata'])) {
            $json_metadata = $this->json_parser->singleObject($result['json_metadata']);
            $result['json_metadata'] = $json_metadata;
            if(isset($json_metadata['profile']) && !empty($json_metadata['profile'])) {
                if(isset($json_metadata['profile']['website']) && !empty($json_metadata['profile']['website'])) {
                    $result['json_metadata']['profile']['website'] = array(
                        'url' => $json_metadata['profile']['website'],
                        'host' => parse_url($json_metadata['profile']['website'])['host']
                    );
                }
            }
        }
        $result['ranking'] = $this->ranking->get($result['name']);
        $result['reputation'] = [
            'reputation' => $result['reputation'],
            'convert'    => $this->reputation->convert($result['reputation'])
        ];
        return $result;
    }

    /**
     * @param $name
     * @param bool $follows
     * @return null
     */
    public function content($name, $follows = false){
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'users/' . $name);
            if($response->getStatusCode() != 200) { return null; }

            if($follows == true) {
                $result = json_decode($response->getBody()->getContents(), true)['result'];
                foreach($result as &$account) {
                    $account = $this->followsParser($account);
                }
            } else {
                $result = json_decode($response->getBody()->getContents(), true)['result'][0];
                $properties = $this->properties->get();
                $result = $this->parserResult($result, $properties);
            }

            return $result;
        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @param $key
     * @param $name
     * @param $follows
     * @return null
     */
    private function contentCache($key, $name, $follows){
        if(!$key || !$name) { return null; }
        $account = $this->memcached->get($key);
        if(!$account) {
            $value = $this->content($name, $follows);
            $this->memcached->set($key, $value, 1, 'minutes');
        }
        return $this->memcached->get($key);
    }

    /**
     * @param $name
     * @param bool $follows
     * @return null
     */
    public function get($name, $follows = false){
        if(!$name) { return null; }
        $isUserBanned = $this->app_blocking->isUserBanned($name);
        if(!$isUserBanned) {
            return $this->contentCache(md5( 'account_single_data_' . $name ), $name, $follows);
        }
        return null;
    }

}