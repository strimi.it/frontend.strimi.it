<?php
namespace ApiBundle\Services\Connection\Steemd;


class Method {

    public function getAccounts(array $accounts){
        return [
            'id'     => 6,
            'method' => 'call',
            'api'    => 'database_api',
            'query'  => 'get_accounts',
            'params' => [ $accounts ]
        ];
    }

    public function getDiscussionsBy(string $name = '', string $tag = '', int $limit = 35){
        /* trending, created, active, payout, votes, children, hot, promoted */
        return [
            'id'     => 0,
            'method' => 'call',
            'api'    => 'database_api',
            'query'  => 'get_discussions_by_' . $name,
            'params' => [ ['tag' => $tag, 'limit' => $limit] ]
        ];
    }

    public function getContent(string $author, string $permlink){
        return [
            'id'     => 0,
            'method' => 'call',
            'api'    => 'database_api',
            'query'  => 'get_content',
            'params' => ['author' => $author, 'permlink' => $permlink]
        ];
    }



}