<?php

namespace AppBundle\Controller\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PrivacyController extends Controller {

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        return $this->render(
            'AppBundle:Page:privacy.html.twig'
        );
    }

}
