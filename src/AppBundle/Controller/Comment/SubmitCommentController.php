<?php

namespace AppBundle\Controller\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SubmitCommentController extends Controller {

    /**
     * @param $author
     * @param $permlink
     * @param Request $request
     * @return mixed
     */
    public function commentRenderAction($author, $permlink, Request $request){
        if(!$request->isXmlHttpRequest() || !$author || !$permlink) { die(); };
        $code = 400;
        $data = null;
        $comment = $this->get('api.post.single')->get($author, $permlink);
        if($comment) {
            $code = 200;
            $appRoute = $request->get('_route');
            $data = array(
                $this->renderView(
                    'AppBundle:Comment:index.html.twig',
                    array(
                        'comment'  => $comment,
                        'appRoute' => $appRoute
                    )
                )
            );

        }

        return $this->get('app.json.response')->renderDefault(
            $data,
            $code
        );

    }



}
