<?php

namespace ApiBundle\Services;

class DynamicGlobalProperties {

    protected $json_parser;
    protected $api_connection;
    protected $memcached;
    public function __construct($jsonParser, $apiConnection, $memcached){
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $memcached;
    }

    /**
     * @return null
     */
    private function content(){
        try{
            $response = $this->api_connection->init()->request('GET', '/dynamicGlobalProperties');
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents());
            if(!$result) { return null; }
            return $result['result'];
        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @return mixed
     */
    public function get(){
        $key = 'dynamicGlobalProperties';
        $content = $this->memcached->get($key);
        if(!$content) {
            $value = $this->content();
            $this->memcached->set($key, $value, 15, 'seconds');
            $content = $value;
        }

        return $content;
    }

}