<?php

namespace AppBundle\Services;
use Symfony\Component\HttpFoundation\JsonResponse;

class JsonResponseDefault {

    public function renderDefault($data, $code){
        if(!$code) {
            return null;
        }
        return new JsonResponse(
            array(
                'data' => $data,
                'code' => $code
            ),
        200
        );
    }

}