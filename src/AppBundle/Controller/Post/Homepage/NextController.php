<?php

namespace AppBundle\Controller\Post\Homepage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NextController extends Controller {

    /**
     * @param $author
     * @param $permlink
     * @param $path
     * @return null
     */
    public function indexAction($author, $permlink, $path) {
        if(!$author || !$permlink || !$path) { return null; }

        $code = 400;

        $domain = $this->get('core.domain')->currentDomainLocale();
        if($domain == 'en') {
            $posts = $this->get('api.posts.next')->content($author, $permlink, $path);
        } elseif ($domain == 'pl') {
            $posts = $this->get('api.posts.next')->content($author, $permlink, $path, 'polish');
        }

        $postsContent = null;
        $nextUri = null;
        if($posts) {
            $code = 200;
            $lastPost =  end($posts);
            $nextUri = $this->generateUrl('app_ajax_posts_next', array(
                'author'   => $lastPost['author'],
                'permlink' => $lastPost['permlink'],
                'path'     => $path
            ));

            $postsContent = $this->renderView(
                'AppBundle:Post/List:posts.html.twig',
                array(
                    'posts' => $posts,
                    'appRoute' => null
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'posts'    => $postsContent,
                'next_url' => $nextUri
            ),
            $code
        );
    }




}
