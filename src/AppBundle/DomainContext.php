<?php

namespace AppBundle;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RequestStack;

class DomainContext implements EventSubscriberInterface {

    protected $requestStack;
    protected $domain_en;
    protected $domain_pl;
    public function __construct(RequestStack $requestStack, array $domain_en, array $domain_pl){
        $this->requestStack = $requestStack;
        $this->domain_en = $domain_en;
        $this->domain_pl = $domain_pl;
    }

    private function host(){
        $host = $this->requestStack->getMasterRequest()->getHost();
        if(!empty($host)) {
            $host = $this->requestStack->getCurrentRequest()->getHost();
        }
        return $host;
    }

    public function currentDomainLocale(){
        $host = $this->host();
        if(in_array($host, $this->domain_en)) {
            return 'en';
        } elseif (in_array($host, $this->domain_pl)) {
            return 'pl';
        }
        return 'en';
    }

    /**
     * Set default locale
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event) {
        $request = $event->getRequest();
        $locale = $this->currentDomainLocale();
        $request->setLocale($locale);
    }

    /**
     * {@inheritdoc}
     */
    static public function getSubscribedEvents(){
        return array(
            KernelEvents::REQUEST => array(array('onKernelRequest', 17)),
        );
    }
}


