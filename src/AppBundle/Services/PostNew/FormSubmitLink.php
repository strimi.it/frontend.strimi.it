<?php

namespace AppBundle\Services\PostNew;
use AppBundle\EntityFake\Link;
use AppBundle\Form\LinkSubmitType;
use AppBundle\Form\LinkSubmitPreviewType;

class FormSubmitLink {

    protected $form_factory;
    protected $router;
    protected $templating;
    protected $request_stack;
    protected $url_parser;
    public function __construct($form_factory, $router, $templating, $request_stack, $url_parser) {
        $this->form_factory = $form_factory;
        $this->router = $router;
        $this->templating = $templating;
        $this->request_stack = $request_stack;
        $this->url_parser = $url_parser;
    }

    /* Step 1 */

    public function createFormSubmit(){
        return $this->form_factory->create(LinkSubmitType::class, new Link(), array(
            'action' => $this->router->generate(
                'app_ajax_post_new_link_submit'
            ),
            'method' => 'POST',
        ));
    }

    public function renderFormSubmitLink(){
        return $this->templating->render(
            'AppBundle:Post/Add/Link:submitForm.html.twig',
            array(
                'form' => $this->createFormSubmit()->createView(),
            )
        );
    }

    /* Step 2 */

    public function createFormPreview(){
        return $this->form_factory->create(LinkSubmitPreviewType::class, new Link(), array(
            'action' => $this->router->generate(
                'app_ajax_post_new_link_submit'
            ),
            'method' => 'POST',
        ));
    }

    public function renderFormPreviewLink($form, $parser){
        if(!$form) {
            $form = $this->createFormPreview();
        }
        return $this->templating->render(
            'AppBundle:Post/Add/Link:previewForm.html.twig',
            array(
                'parser' => $parser,
                'form'   => $form->createView(),
            )
        );
    }

    public function submitFormSubmitLink($form, $request){
        $form->handleRequest($request);
        //$code = 400;
        $parser = null;
        $form_preview = null;
        //if ($form->isSubmitted() && $form->isValid()) {
            $link = $form["link"]->getData();
            $parser = $this->url_parser->prepareResponse($link);
            if(isset($parser) && !empty($parser)) {
                $form_preview = $this->createFormPreview();
                $form_preview['title']->setData($parser['title']);
                $form_preview['description']->setData(substr($parser['description'],0,500));
                $form_preview = $this->renderFormPreviewLink($form_preview, $parser);
            }
            //$code = 200;
        //}

        return array(
            'form'   => $form,
            'parser' => $parser,
            'form_preview' => $form_preview,
            'code'   => 200
        );

    }

}