strimi_app.postSubmitText = function () {

    if (!strimi_app.is_login()) { return; }

    var account = strimi_app.authorization();
    var username = account.username,
        wif = account.private_keys.posting_wif;

    $("#clear-strimi-text").click(function () {
        document.getElementsByClassName('text-title')[0].value = '';
        document.getElementsByClassName('text-description')[0].value = '';
        document.getElementsByClassName('text-tags')[0].value = '';
        document.getElementsByClassName('markdown-preview-text')[0].innerHTML = '';
    });

    var loading = function (submitButton, loadingStatus) {
        var loadingButton = document.getElementById('submit-strimi-text-loading');
        if (loadingStatus === 'start') {
            submitButton.className += ' active-loading';
            loadingButton.classList.remove('hidden');
        } else if (loadingStatus === 'end') {
            submitButton.classList.remove('active-loading');
            loadingButton.className += ' hidden';
        }
    };

    $("#formText").submit(function (e) {
        e.preventDefault();

        var submitButton = document.getElementById('submit-strimi-text');
        if (submitButton.getAttribute('data-status') === '0') {
            submitButton.setAttribute('data-status', 1);

            loading(submitButton, 'start');

            var title = document.getElementsByClassName('text-title')[0];
                title = strimi_app.contentLength(title.value, title.getAttribute('length_max'));
            var body = document.getElementsByClassName('text-description')[0];
                body = strimi_app.contentLength(body.value, body.getAttribute('length_max'));
            var bodyMarkdown = document.getElementsByClassName('markdown-preview-text')[0].innerHTML;
            var permlink = strimi_app.createPermlink(title);
            var contentType = 'text';
            var strimiMetadata = {
                title: title,
                body: body,
                type: {
                    type: contentType
                }
            };

            var payout = strimi_app.textPayout();
            var tagsList = strimi_app.strimiTags(contentType);

            var thumb;

            //Tuhumb
            // var thumb;
            // var parseStrimiMetadata = JSON.parse(strimi_metadata);
            //     parseStrimiMetadata.body = body;
            //     parseStrimiMetadata.title = title;
            // var contentType = parseStrimiMetadata.type.type;
            //
            // if (parseStrimiMetadata.thumb) {
            //     thumb = parseStrimiMetadata.thumb
            // } else if (contentType === 'image') {
            //     thumb = parseStrimiMetadata.source
            // }

            //See the full content

            var body_footer = '<p>' + bodyMarkdown + '</p>';
                //body_footer += '<p><center><a href="https://strimi.it/' + category + '/@' + username + '/' + permlink + '"><img  src="' + thumb + '"></a></center></p>';
                body_footer += '<hr><p><center><em>Posted on <a href="https://strimi.it"><strong>Strimi.it</strong></a></em></p>';
                body_footer += '<p><em>via <a href="https://strimi.it/' + 'strimi' + '/@' + username + '/' + permlink + '">' + title + '</a></em></center></p>';
                body_footer += '<hr>';

            var operations = [
                ['comment',
                    {
                        parent_author: '',
                        parent_permlink: 'strimi',
                        author: username,
                        permlink: permlink,
                        title: title,
                        body: body,
                        json_metadata : JSON.stringify({
                            tags: tagsList,
                            format: "html",
                            app: strimi_app.config.base.compilation,
                            strimi_metadata: JSON.stringify(strimiMetadata)
                        })
                    }
                ],
                ['comment_options', {
                    author: username,
                    permlink: permlink,
                    max_accepted_payout: payout[0],
                    percent_steem_dollars: payout[1],
                    allow_votes: true,
                    allow_curation_rewards: true,
                    extensions: strimi_app.config.beneficiaries
                }]
            ];

            strimi_app.broadcast.sendAsync(
                { extensions: [], operations: operations  },
                { posting: wif }, function(error, result){
                    if (result && result !== 'undefined') {
                        loading(submitButton, 'end');
                        gtag('event', 'submit-text', { 'event_category': username, 'event_label': permlink });
                        window.location.href = '/strimi/@' + username + '/' + permlink;
                    } else if (error && error !== 'undefined') {
                        console.log( error );
                        submitButton.setAttribute('data-status', 0);
                        loading(submitButton, 'end');
                        Raven.captureException(error);
                    }
                });

        }

    });

};