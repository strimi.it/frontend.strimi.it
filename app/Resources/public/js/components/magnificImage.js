strimi.magnificImage = function () {
    $('.magnific-image').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        //mainClass: 'mfp-img-mobile',
        tLoading: 'Loading...',
        image: {
            verticalFit: true
        },
        mainClass: 'mfp-with-zoom',
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function(openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        }
    });
};
strimi.magnificImage();