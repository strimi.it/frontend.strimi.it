<?php

namespace ApiBundle\Services\Users;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Notification  {

    protected $api_connection;
    protected $memcached;
    protected $jsonParser;
    protected $paginator;
    protected $post;
    protected $router;
    public function __construct($apiConnection, $cacheMemcached, $jsonParser, $paginator, $post, $router) {
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
        $this->json_parser = $jsonParser;
        $this->paginator = $paginator;
        $this->post = $post;
        $this->router = $router;
    }

    private function content($username){
        if(!$username) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'mongo/users/' . $username . '/notifications');
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true);
            return array_reverse($result);
        }catch(\Exception $ex){
        }
    }

    private function removeSchema($url) {
        if(empty($url)) { return null; }
        $parsedUrl = parse_url($url);
        $parsedUrl['scheme'] = '//';
        return implode('', $parsedUrl);
    }

    private function parser($results){
        if(!$results) { return null; };

        foreach( $results as &$notification) {
            $author = null;
            $data = null;
            $code = $notification['code'];
            if($code == 6 || $code == 8 || $code == 9) {
                $author = $notification['author'];
                $permlink = $notification['permlink'];
                $post = $this->post->get($author, $permlink);
                if($post) {
                    $urlPermlink = $this->router->generate("app_post", array(
                        "category" => $post['root_url_parser']['category'],
                        "author"   => $post['root_url_parser']['author'],
                        "permlink" => $post['root_url_parser']['permlink']
                    ), UrlGeneratorInterface::ABSOLUTE_URL);
                    $notification['post'] = [
                        'id'       => $post['id'],
                        'created'  => $post['created'],
                        'category' => $post['root_url_parser']['category'],
                        'author'   => $post['root_url_parser']['author'],
                        'permlink' =>  $post['root_url_parser']['permlink'],
                        'body'     => substr($post['body'], 0, 40),
                        'url'      => $this->removeSchema($urlPermlink)
                    ];
                }
            }  elseif ($code == 4) {
                $notification['author'] = $notification['follower'];
                unset($notification['follower']);
            }
        }
        return $results;
    }

    public function get($username) {
        if(!$username) { return null; }
        $content = $this->content($username);
        /* Remove Vote */
        $allow = [4, 6, 8, 9];
        foreach( $content as $key => $notification) {
            $code = $notification['code'];
            if(!in_array($code, $allow)) {
                unset($content[$key]);
            }
        }
        $results = $this->paginator->getPackage(
            $content, 0, 35
        );
        return $this->parser($results);
    }

}