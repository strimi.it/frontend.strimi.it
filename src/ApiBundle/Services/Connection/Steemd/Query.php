<?php

namespace ApiBundle\Services\Connection\Steemd;
use ApiBundle\Services\Connection\Steemd\Connection as SteemdConnection;
use ApiBundle\Services\Connection\Steemd\Method as Method;

class Query extends SteemdConnection {

    protected $posts_defaults;
    public function __construct($posts_defaults){
        $this->posts_defaults = $posts_defaults;
    }

    private function executeRequest($query){
        $client = $this->init();
        $request = $client->request($query['id'], $query['method'], [
            $query['api'], $query['query'], $query['params']
        ]);
        $response = $client->send($request);
        if($response->getStatusCode() != 200) { return null; }
        $content = $response->getBody()->getContents();
        if(empty($content)) { return null; }
        if(empty(json_decode($content, true)['result'])){ return null; }
        $result = json_decode($content, true)['result'];
        if(is_array($result)) {
            foreach ($result as &$singleObject) {
                $singleObject = $this->posts_defaults->parserContent($singleObject);
            }
        }
        return $result;
    }

    public function executeAccounts($accounts){
        $method = new Method();
        $query = $method->getAccounts($accounts);
        return $this->executeRequest($query);
    }

    public function executeDiscussionsBy($name, $tag, $limit){
        $method = new Method();
        $query = $method->getDiscussionsBy($name, $tag, $limit);
        return $this->executeRequest($query);
    }

    public function executeContent($author, $permlink){
        $method = new Method();
        $query = $method->getContent($author, $permlink);
        return $this->executeRequest($query);
    }

}