<?php

namespace ApiBundle\Services\Connection\Steemd;
use Graze\GuzzleHttp\JsonRpc\Client;
use Graze\GuzzleHttp\JsonRpc\Exception\RequestException;

class Connection {

    /**
     * @return string
     */
    private function nodes(){
        $nodes = [
            //'steemd.pevo.science',
            //'steemd.minnowsupportproject.org',
            //'rpc.steemviz.com',
            //'gtg.steem.house:8090',
            'api.steemit.com',
            //'steemd.privex.io'
        ];
        $node = $nodes[array_rand($nodes)];
        //var_dump($node);
        return 'https://' . $node . '/';
    }

    /**
     * @return Client
     */
    public function init(){
        try {
            return Client::factory($this->nodes());
        }catch(\Exception $ex){ }
    }
}