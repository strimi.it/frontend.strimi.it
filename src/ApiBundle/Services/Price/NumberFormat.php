<?php

namespace ApiBundle\Services\Price;

class NumberFormat {


    /**
     * @param $content
     * @param int $decimals
     * @return null|string
     */
    public function parser($content, $decimals = 3){
        if(!$content) { return null; }
        return number_format($content, $decimals);
    }

}
