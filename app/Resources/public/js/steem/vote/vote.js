strimi_app.vote = function () {

    if(!strimi_app.is_login()) { return; }

    var account = strimi_app.authorization();
    var username = account.username,
        wif = account.private_keys.posting_wif;

    var modalVote = document.getElementById('modalVote');
    var sliders = modalVote.getElementsByClassName('slider-horizontal');
    if(sliders) {
        for (var i = 0; i < sliders.length; i++) {

            if(sliders[i] !== sliders.length) {
                sliders[i].remove()
            }

        }
    }

    var removeVote = function (status) {
        var title = document.getElementsByClassName('vote-strong')[0];
        var titleRemove = document.getElementsByClassName('vote-strong-remove')[0];
        var slider = document.getElementsByClassName('slider-content')[0];
        var submit = document.getElementById('submit-vote-lang');
        if(status === 'active') {
            title.className += ' hidden';
            titleRemove.className += ' active';
            slider.className += ' hidden';
            submit.innerText = strimi.languageText('global.post.votes.slider.undo');
        } else if('remove') {
            if(title.classList.contains('hidden')) {
                title.classList.remove('hidden');
                titleRemove.classList.remove('active');
                slider.classList.remove('hidden');
            }
            submit.innerText = strimi.languageText('global.post.votes.slider.submit');
        }

    };

    var slider = new Slider("#vote-slider", {
        formatter: function(value) {
            document.getElementsByClassName('vote-value')[0].innerHTML = value + '%';
        },
        min: 1,
        max: 100,
        ticks: [1, 25, 50, 75, 100],
        ticks_labels: ['0%', '25%', '50%', '75%', '100%'],
        value: 100,
        tooltip: "hide",
        step: 1,
        ticks_snap_bounds: 1
    });

    var submit = document.getElementById('submit-vote');
    var voteStatus = false;

    $('.vote-content').click(function() {
        setTimeout(function(){ slider.refresh(); }, 100);
        submit.setAttribute('data-id', $(this).attr('id'));
        voteStatus = $(this).attr('vote-status');
        if(voteStatus === 'true') {
            removeVote('active');
        } else {
            removeVote('remove');
        }
    });

    var loading = function (submitButton, loadingStatus) {
        var loadingButton = document.getElementById('submit-vote-loading');
        if (loadingStatus === 'start') {
            submitButton.className += ' active-loading';
            loadingButton.classList.remove('hidden');
        } else if (loadingStatus === 'end') {
            submitButton.classList.remove('active-loading');
            loadingButton.className += ' hidden';
        }
    };

    submit.onclick = function () {
        var power = document.getElementsByClassName('vote-power-slider')[0].value;
        var id = this.getAttribute('data-id');
        var voteContent = document.getElementsByClassName('vote-post-' + id)[0];
        var short;
        if(voteContent) {
            short = document.getElementById('post-single-' + id).getAttribute('data-short-permlink');
            short = short.split('/');
        } else {
            voteContent = document.getElementsByClassName('vote-comment-' + id)[0];
            short = document.getElementById('comment-single-' + id).getAttribute('data-short-permlink');
            short = short.split('/');
        }

        var author = short[0];
        var permlink = short[1];
        var weight = strimi_app.voteWeight(power);
        loading(submit, 'start');
        if(voteStatus === 'true') {
            weight = 0;
        }

        strimi_app.broadcast.vote(wif, username, author, permlink, weight, function(error, result) {
            if(result && result !== 'undefined') {
                strimi_app.accountVote();
                if(voteStatus === 'true') {
                    voteContent.classList.remove('active');
                    voteContent.removeAttribute('vote-status');
                } else {
                    voteContent.className += ' active';
                    voteContent.setAttribute('vote-status', true);
                }
                $('#modalVote').modal('hide');
                gtag('event', 'vote-post', { 'event_category': username, 'event_label': power + '% - ' + permlink });
            } else if (error && error !== 'undefined') {
                console.log( error );
            }
            loading(submit, 'end');
        });

    };

    /* Comment */
    $('.vote-comment').click(function() {
        setTimeout(function(){ slider.refresh(); }, 100);
        submit.setAttribute('data-id', $(this).attr('id'));
        voteStatus = $(this).attr('vote-status');
        if(voteStatus === 'true') {
            removeVote('active');
        } else {
            removeVote('remove');
        }
    });

};
strimi_app.vote();