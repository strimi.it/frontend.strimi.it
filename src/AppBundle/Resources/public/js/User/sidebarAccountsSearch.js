strimi.sidebarAccountsSearch = function () {

    var url = function () {
        var uri = document.getElementsByClassName('sidebar-content-users-search')[0].getAttribute('data-url');
        return uri.replace("strimi", "");
    };

    $(".suggest_accounts_autocomplete").on("change keyup paste", function(){

        var name = $(this).val();
        var nameLength = name.length;
        var content = document.getElementsByClassName('sidebar-content-users')[0];
        var contentSearch = document.getElementsByClassName('sidebar-content-users-search-content')[0];

        if(nameLength > 0){

            if(!contentSearch.classList.contains('active')){
                contentSearch.className += ' active';
            }

            $.ajax({
                type: 'GET',
                url: url() + $(this).val(),
                async: true,
                beforeSend: function() {},
                success: function(getResponse) {
                    if (getResponse.code == 200) {
                        var content = document.getElementsByClassName('sidebar-content-users-search-content')[0];
                        content.innerHTML = getResponse.data;
                        strimi.imageLazyload();
                        strimi.language();
                    } else if (getResponse.code == 400) {
                    }
                },
                error: function() {
                }
            });
        } else if (nameLength === 0){
            contentSearch.classList.remove('active');
            contentSearch.innerHTML = ''
        }
    });

};
strimi.sidebarAccountsSearch();