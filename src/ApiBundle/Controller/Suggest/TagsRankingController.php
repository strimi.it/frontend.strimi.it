<?php

namespace ApiBundle\Controller\Suggest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TagsRankingController extends Controller {

    public function resetAction(){
        $code = 200;
        $tags = $this->get('api.tags.lists')->lists();
        $tagsContent = null;
        if($tags) {
            $tagsContent = $this->renderView(
                'AppBundle:Tag/Ranking:trContent.html.twig',
                array(
                    'tags' => $tags
                )
            );
        }
        return $this->get('app.json.response')->renderDefault(
            $tagsContent,
            $code
        );
    }

    public function indexAction($name) {
        if(!$name) { return null; }
        $code = 200;
        $tags = $this->get('api.elastic.suggest.tags')->get($name, 50, true);
        $tagsContent = null;
        if($tags) {
            $tagsContent = $this->renderView(
                'AppBundle:Tag/Ranking:trContent.html.twig',
                array(
                    'tags' => $tags
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            $tagsContent,
            $code
        );
    }
}
