<?php
namespace AppBundle\Services\TwigExtended;

class SanitizeExtension extends \Twig_Extension {

    protected $sanitizer;
    public function __construct( $sanitizer){
        $this->sanitizer = $sanitizer;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('sanitize', array($this, 'content'), array('is_safe' => array('html'))),
        );
    }

    public function content($content) {
        if(!$content) { return null; }
        return $this->sanitizer->sanitize($content);
    }

    public function getName() {
        return 'sanitize';
    }

}