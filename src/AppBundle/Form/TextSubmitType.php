<?php

namespace AppBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TextSubmitType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', TextType::class, array(
                'required' => true,
                'trim' => true,
                'attr' => array(
                    'length_min' => 10,
                    'length_max' => 180,
                    'autocomplete' => 'off'
                )
            ))
            ->add('description', TextareaType::class, array(
                'required' => true,
                'trim' => true,
                'attr' => array(
                    'length_min' => 10,
                    'length_max' => 25000, //66349
                    'autocomplete' => 'off'
                )
            ))
            ->add('tags', TextType::class, array(
                'required' => true,
                'trim' => true,
                'attr' => array(
                    'length_min' => 3,
                    'length_max' => 65,
                    'autocomplete' => 'off'
                )
            ))
            ->add('payout', ChoiceType::class, array(
                'required' => true,
                'choices' => array(
                    '50% / 50%' => 'default',
                    '100% SteemPower' => 'steem_power',
                    'Decline' => 'decline'
                ),
                'choice_attr' => array(
                    'Decline' => array('data-lang' => 'global.post_new.step_second.form.reward.decline')
                )
            ))

            ->add('nsfw', ChoiceType::class, array(
                'required' => true,
                'choices' => array(
                    'No'  => 0,
                    'Yes' => 1
                ),
                'choice_attr' => array(
                    'No' => array('data-lang' => 'global.post_new.step_second.form.nsfw.no'),
                    'Yes' => array('data-lang' => 'global.post_new.step_second.form.nsfw.yes')
                )
            ))
        ;
    }

    public function getBlockPrefix() {
        return '';
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\EntityFake\Text',
            'validation_groups' => 'adding_text',
            'csrf_protection' => true,
            'csrf_field_name' => '_token'
        ));
    }

}