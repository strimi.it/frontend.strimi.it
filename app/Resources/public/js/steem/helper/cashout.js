strimi.cashout = function () {

    var clean = function (item) {
        item.removeAttribute("data-cashout");
        item.removeAttribute("data-pending");
        item.removeAttribute("data-time");
        item.removeAttribute("data-author");
        item.removeAttribute("data-sum");
        item.removeAttribute("data-curator");
    };

    var timeCashout =  document.getElementsByClassName('vote-time');
    if(timeCashout) {
        for (var i = 0; i < timeCashout.length; i++) {
            var time = timeCashout[i].getAttribute('data-time');
            var payout = moment(time + 'Z').add(7, 'days');
            var payoutFormat = moment(time + 'Z').add(7, 'days').format('YYYY/MM/DD HH:mm:ss');
            var payoutFromNow = moment(time + 'Z').add(7, 'days').fromNow();
            var payout_unix = moment(time + 'Z').add(7, 'days').unix();
            var now_unix = moment().unix();
            var trans;
            var pendingText;
            var totalPayout;
            var getUserLang = localStorage.getItem("language");
            if(getUserLang === 'en') {trans = strimi.languageEN()[0] }
            else if(getUserLang === 'pl') { trans = strimi.languagePL()[0] }
            var declined = timeCashout[i].getAttribute('data-declined');
            if(declined === 'declined') {
                pendingText = _.get(trans, 'global.post.votes.declined');
                timeCashout[i].setAttribute('data-content', pendingText);
                if(payout_unix < now_unix){
                    var declinedPayout = timeCashout[i].getAttribute('data-sum');
                    timeCashout[i].innerHTML = '<strike>$' + declinedPayout + '</strike>';
                }
            } else if(payout_unix > now_unix) {
                var pendingValue = timeCashout[i].getAttribute('data-pending');
                var pendingTrans = _.get(trans, 'global.post.votes.potential_payout');
                var pendingWill = _.get(trans, 'global.post.votes.will');
                    pendingText = pendingTrans + ': $' + pendingValue + '</br>' + pendingWill + ' ' + payoutFromNow;
                    timeCashout[i].setAttribute('data-content', pendingText);

            } else {
                var totalAuthor = timeCashout[i].getAttribute('data-author');
                var totalCurator = timeCashout[i].getAttribute('data-curator');
                    totalPayout = timeCashout[i].getAttribute('data-sum');
                var transSum = _.get(trans, 'global.post.votes.sum');
                var transAuthor = _.get(trans, 'global.post.votes.author');
                var transCurrators = _.get(trans, 'global.post.votes.currators');
                pendingText = transSum + ': $' + totalPayout
                                + '</br>' + transAuthor + ': $' + totalAuthor
                                + '</br>' + transCurrators + ': $' + totalCurator;
                timeCashout[i].setAttribute('data-content', pendingText);
                timeCashout[i].innerHTML = '$' + totalPayout;
            }
        }
    }
};
strimi.cashout();