strimi.accountSidebar = function () {
    if(!strimi_app.is_login()) { return; }
    var account = strimi_app.authorization();
    var username = account.username;
    var sidebar = document.getElementById('account-sidebar');
    $.ajax({
        type: 'GET',
        url: sidebar.getAttribute('data-url').replace('strimi', username),
        async: true,
        beforeSend: function() {
        },
        success: function(response) {
            if (response.code == 200) {
                sidebar.innerHTML = response.data;
                strimi.imageLazyload();
                strimi.momentTimezone();
                strimi.language();

            } else if (response.code == 400) {
            }
        },
        error: function() {
        }
    });
};