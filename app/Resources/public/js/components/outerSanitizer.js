strimi.outerSanitizer = function () {
    $('.normal-content p').each(function() {
        if ($(this).text() === '') { $(this).remove(); }
        if ($(this).text() === 'https://') { $(this).remove(); }
        if ($(this).text() === 'http://') { $(this).remove(); }
    });
};
var routeList = [
    'stream_post', 'app_post'
];
if(routeList.includes(_config.route) && strimi_app.is_login()) {
    strimi.outerSanitizer();
}


strimi.outerSanitizerComment = function () {
    $('.comments-single-content').each(function() {
        if ($(this).text() === '<p></p>') { $(this).remove(); }
    });
};