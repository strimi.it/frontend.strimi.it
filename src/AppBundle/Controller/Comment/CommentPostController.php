<?php

namespace AppBundle\Controller\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CommentPostController extends Controller {

    /**
     * @param $response
     * @param Request $request
     * @return mixed
     */
    public function renderFormAction($response, Request $request){
        if(!$request->isXmlHttpRequest()) { die(); };
        return $this->get('app.json.response')->renderDefault(
            array(
                'form' => $this->get('app.comment.form.new')->renderForm($response)
            ),
            200
        );
    }

}
