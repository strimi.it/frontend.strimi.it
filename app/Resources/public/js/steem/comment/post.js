strimi_app.commentPost = function () {

    if (!strimi_app.is_login()) { return; }

    var account = strimi_app.authorization();
    var username = account.username,
        wif = account.private_keys.posting_wif;

    $("#clear-post-comment").click(function () {
        document.getElementsByClassName('comment-post')[0].value = '';
    });

    var loading = function (submitButton, loadingStatus) {
        var loadingButton = document.getElementById('submit-post-comment-loading');
        if (loadingStatus === 'start') {
            submitButton.className += ' active-loading';
            loadingButton.classList.remove('hidden');
        } else if (loadingStatus === 'end') {
            submitButton.classList.remove('active-loading');
            loadingButton.className += ' hidden';
        }
    };

    var render = function (url, username, permlink) {
        url = url.replace("author", username);
        url = url.replace("permlink", permlink);
        $.ajax({
            type: 'GET',
            url: url,
            async: true,
            beforeSend: function() {
            },
            success: function(getResponse) {
                if (getResponse.code == 200) {
                    $("#post-comments-content").prepend(getResponse.data);
                    strimi.imageLazyload();
                    strimi.momentTimezone();
                    strimi.language();
                    strimi_app.reloaded();
                    strimi.postCommentResponseForm();
                    strimi_app.commentPost();
                    document.getElementsByClassName('comment-post')[0].value = '';
                    var commentsNull = document.getElementsByClassName('comments-null')[0];
                    if(commentsNull) {
                        commentsNull.remove();
                    }

                } else if (getResponse.code == 400) {
                }
            },
            error: function() {
            }
        });
    };

    $("#comment-post-form").submit(function (e) {
        e.preventDefault();

        var submitButton = document.getElementById('submit-post-comment');
        if (submitButton.getAttribute('data-status') === '0') {
            submitButton.setAttribute('data-status', 1);

            loading(submitButton, 'start');

            var post = document.getElementsByClassName('post-single')[0];
            if(!post) {
                post = document.getElementsByClassName('comments-single')[0];
            }
            var data = post.getAttribute('data-short-permlink').split("/");
            var parentAuthor = data[0];
            var parentPermlink = data[1];
            var body = document.getElementsByClassName('comment-post')[0];
            var payout = strimi_app.payoutType('steem_power');
            var permlink = strimi_app.createPermlink(body.value);

            var operations = [
                ['comment',
                    {
                        parent_author: parentAuthor,
                        parent_permlink: parentPermlink,
                        author: username,
                        permlink: permlink,
                        title: '',
                        body: body.value,
                        json_metadata : JSON.stringify({
                            tags: '',
                            format: "html",
                            app: strimi_app.config.base.compilation
                        })
                    }
                ],
                ['comment_options', {
                    author: username,
                    permlink: permlink,
                    max_accepted_payout: payout[0],
                    percent_steem_dollars: payout[1],
                    allow_votes: true,
                    allow_curation_rewards: true,
                    extensions: strimi_app.config.beneficiaries
                }]
            ];

            strimi_app.broadcast.sendAsync(
                { operations: operations, extensions: [] },
                { posting: wif }, function(error, result){
                    if (result && result !== 'undefined') {
                        loading(submitButton, 'end');
                        render(submitButton.getAttribute('data-url'), username, permlink);
                        submitButton.setAttribute('data-status', 0);
                        gtag('event', 'submit-comment', { 'event_category': username, 'event_label': permlink });
                        body.value = '';
                    } else if (error && error !== 'undefined') {
                        console.log( error );
                        strimi.prettyNotification('Error!', 'An unexpected error occurred :/');
                        submitButton.setAttribute('data-status', 0);
                        loading(submitButton, 'end');
                        Raven.captureException(error);
                    }
                });

        }

    });

};