<?php

namespace AppBundle\Services\PostNew;
use AppBundle\EntityFake\Text;
use AppBundle\Form\TextSubmitType;
use AppBundle\Form\LinkSubmitPreviewType;

class FormSubmitText {

    protected $form_factory;
    protected $router;
    protected $templating;
    protected $request_stack;
    protected $url_parser;
    public function __construct($form_factory, $router, $templating, $request_stack, $url_parser) {
        $this->form_factory = $form_factory;
        $this->router = $router;
        $this->templating = $templating;
        $this->request_stack = $request_stack;
        $this->url_parser = $url_parser;
    }

    public function createFormSubmit(){
        return $this->form_factory->create(TextSubmitType::class, new Text(), array(
            'action' => $this->router->generate(
                'app_ajax_post_new_link_submit'
            ),
            'method' => 'POST',
        ));
    }

    public function renderFormSubmitText(){
        return $this->templating->render(
            'AppBundle:Post/Add/Text:form.html.twig',
            array(
                'form' => $this->createFormSubmit()->createView(),
            )
        );
    }






//    public function submitFormSubmitLink($form, $request){
//        $form->handleRequest($request);
//        $code = 400;
//        $parser = null;
//        $form_preview = null;
//
//        if ($form->isSubmitted() && $form->isValid()) {
//
//            $link = $form["link"]->getData();
//            $parser = $this->url_parser->prepareResponse($link);
//            if(isset($parser) && !empty($parser)) {
//                $form_preview = $this->createFormPreview();
//                $form_preview['title']->setData($parser['title']);
//                $form_preview['description']->setData($parser['description']);
//                $form_preview = $this->renderFormPreviewLink($form_preview, $parser);
//            }
//
//            $code = 200;
//        }
//
//        return array(
//            'form'   => $form,
//            'parser' => $parser,
//            'form_preview' => $form_preview,
//            'code'   => $code
//        );
//
//    }

}