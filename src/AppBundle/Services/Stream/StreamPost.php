<?php

namespace AppBundle\Services\Stream;
use AppBundle\EntityFake\Stream;
use AppBundle\Form\StreamType;

class StreamPost {

    protected $form_factory;
    protected $router;
    protected $templating;
    protected $request_stack;
    public function __construct($form_factory, $router, $templating, $request_stack) {
        $this->form_factory = $form_factory;
        $this->router = $router;
        $this->templating = $templating;
        $this->request_stack = $request_stack;
    }

    public function renderForm(){
        return $this->templating->render(
            'AppBundle:Stream:form.html.twig',
            array(
                'form' => $this->createForm()->createView()
            )
        );
    }

}