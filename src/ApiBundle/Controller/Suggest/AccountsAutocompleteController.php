<?php

namespace ApiBundle\Controller\Suggest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AccountsAutocompleteController extends Controller {

    /**
     * @param $name
     * @return null
     */
    public function indexAction($name){

        if (!$name) {
            return null;
        }
        $code = 400;
        $accounts = $this->get('api.elastic.suggest.accounts')->get($name, 15);
        $accountsContent = null;
        if ($accounts) {
            $data = [];
            foreach ($accounts as &$account) {
                $data[] = [
                    'name' => $account['name'],
                    'reputation' => $account['reputation']
                ];
            }
            $accountsContent = $data;
            $code = 200;
        }

        return $this->get('app.json.response')->renderDefault(
            $accountsContent,
            $code
        );

    }



}
