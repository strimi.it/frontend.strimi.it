<?php

namespace ApiBundle\Services\Tags;

class Next {

    protected $content;
    protected $json_parser;
    protected $api_connection;
    protected $memcached;
    public function __construct($tagContent, $jsonParser, $apiConnection, $cacheMemcached){
        $this->content = $tagContent;
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
    }

    /**
     * @param $tag
     * @return array|null
     */
    public function get($tag){
        if(!$tag) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'tags', [
                'query' => [
                    'startTag' => $tag,
                    'limit'   => 50
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents())['result'];
            $data = array();

            foreach($result as $tag) {
                if(isset($tag['name']) && $tag['name'] != null) {
                    $data[] = $this->content->content($tag);
                }
            }
            array_shift($data);
            return $data;

        }catch(\Exception $ex){
            //Process the exception
        }
    }



}