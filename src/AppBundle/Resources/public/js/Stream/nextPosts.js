strimi.nextStreamPosts = function () {
    var nextButton = document.getElementById('stream-next-button');
    var loadingButton = document.getElementsByClassName('loading-oval-button')[0];
    if(nextButton){
        nextButton.onclick = function(){
            var status = nextButton.getAttribute('data-status');
            if(status == 0){
                $.ajax({
                    type: 'GET',
                    url: nextButton.getAttribute('data-url'),
                    async: true,
                    beforeSend: function() {
                        nextButton.setAttribute('data-status', 1);
                        nextButton.className += ' active';
                        loadingButton.classList.remove('hidden');
                    },
                    success: function(getResponse) {
                        if (getResponse.code == 200) {

                            $('.stream-post').append(getResponse.data.posts);

                            // var lastPost = document.getElementsByClassName('comments-single');
                            // lastPost = lastPost[lastPost.length - 1];
                            // lastPost.insertAdjacentHTML('afterEnd', getResponse.data.posts);

                            nextButton.setAttribute('data-url', getResponse.data.next_url);

                            nextButton.setAttribute('data-status', 0);
                            nextButton.classList.remove('active');
                            loadingButton.classList.className += ' hidden';

                            strimi.imageLazyload();
                            strimi.momentTimezone();
                            strimi.commentsCount();
                            strimi.language();
                            strimi_app.reloaded();
                        } else if (getResponse.code == 400) {

                            nextButton.setAttribute('data-status', 0);
                            nextButton.classList.remove('active');
                            loadingButton.classList.className += ' hidden';

                        }
                    },
                    error: function() {
                    }
                });
            }
        };
    }
};
if(_config.route === 'stream_homepage') {
    strimi.nextStreamPosts();
}
