<?php

namespace ApiBundle\Services\Elastic\Suggest;

class Tags {

    protected $parser_sbd;
    protected $price_format;
    protected $json_parser;
    protected $api_connection;
    protected $memcached;

    public function __construct($parserSBD, $priceFormat, $jsonParser, $apiConnection, $cacheMemcached){
        $this->parser_sbd = $parserSBD;
        $this->price_format = $priceFormat;
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
    }

    /**
     * @param $tag
     * @return array|null
     */
    public function content($tag){
        if(!$tag) { return null; }
        $total_payouts = $this->parser_sbd->parser($tag['totalPayouts']);
        $data = [
            'name'           => $tag['name'],
            'total_payouts'  => array(
                'number'  => preg_replace('/,/', '', $total_payouts),
                'format' => $total_payouts
            ),
            'count_posts'    => array(
                'number' => $tag['topPosts'],
                'format' => $this->price_format->parser($tag['topPosts'], 0)
            ),
            'count_comments' => array(
                'number' => $tag['comments'],
                'format' => $this->price_format->parser($tag['comments'], 0)
            ),
            'count_votes'    => $this->price_format->parser($tag['netVotes'], 0),
        ];
        return $data;
    }

    /**
     * @param $name
     * @param $limit
     * @param $ranking
     * @param bool $autocomplete
     * @return array|mixed|null
     */
    public function get($name, $limit, $ranking, $autocomplete = false){
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'elastic/tags/suggest', [
                'query' => [
                    'suggestFor' => $name,
                    'limit'   => $limit
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents())['hits']['hits'];
            $data = array();
            if(!$result) { return null; }
            foreach($result as $tag) {
                $source = $tag['_source'];
                $count_posts = $source['topPosts'];
                if($count_posts > 0){
                    $data[] = $this->content($source);
                }
            }

            if(!$ranking) {
                usort($data, function($a, $b) {
                    return $a['count_posts']['number'] - $b['count_posts']['number'];
                });
                return array_reverse($data);
            }

            if($autocomplete == true) {
                return $data['data'];
            }

            return $data;
        }catch(\Exception $ex){
            //Process the exception
        }

    }

}