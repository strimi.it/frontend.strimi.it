<?php

namespace AppBundle\Controller\User\Account;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TransfersController extends Controller {

    /**
     * @param $username
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($username) {
        if(!$username) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $account = $this->get('api.users.account')->get($username);
        if(!$account) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $transfers = $this->get('api.users.account.transfer')->get($account['name']);
        return $this->render(
            'AppBundle:User/Account:index.html.twig',array(
                'account'  => $account,
                'transfers' => $transfers
            )
        );
    }

    /**
     * @param $username
     * @param $package
     * @return null
     */
    public function nextTransferAction($username, $package) {
        if(!$username || !$package) { return null; }

        $code = 400;
        $account = $this->get('api.users.account')->get($username);
        $transfers = $this->get('api.users.account.transfer')->getNext($account['name'], $package);
        $transfersContent = null;
        $nextUri = null;

        if($transfers) {
            $code = 200;

            $nextUri = $this->generateUrl('user_account_ajax_transfers_next', array(
                'username'  => $account['name'],
                'package'   => $package + 35
            ));

            $transfersContent = $this->renderView(
                'AppBundle:User/Account/Transfers:lists.html.twig',
                array(
                    'account'  => $account,
                    'transfers' => $transfers,
                    'appRoute' => null
                )
            );

        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'votes'    => $transfersContent,
                'next_url' => $nextUri
            ),
            $code
        );
    }

}
