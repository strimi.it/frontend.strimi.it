strimi_app.verify = function (name, password) {

    var errorView = function (errorText) {
        var content = document.getElementsByClassName('alert-content')[0];
        var alert = document.getElementById('alert-login');
        content.className += ' active';
        alert.innerText = errorText;
        loading('end');
        return '';
    };

    var loading = function (loadingStatus) {
        var submitButton = document.getElementById('loginButton');
        var loadingButton = document.getElementById('submit-login-loading');
        var loginLan = document.getElementById('login-lan');
        if (loadingStatus === 'start') {
            submitButton.className += ' active-loading';
            loadingButton.classList.remove('hidden');
            loginLan.style.marginRight = '10px';
            loadingButton.style.top = '10px';
            loadingButton.style.right = 'auto';
        } else if (loadingStatus === 'end') {
            submitButton.classList.remove('active-loading');
            loadingButton.className += ' hidden';
        }
    };

    loading('start');

    if((!name || 0 === name.length) || (!password || 0 === password.length)) {
        errorView(strimi.languageText('global.user.login.alert.1'));
        return '';
    }

    name = name.toLowerCase();
    var isValidUsername = strimi_app.utils.validateAccountName(name);
    if(isValidUsername !== null) {
        errorView(strimi.languageText('global.user.login.alert.1'));
        return '';
    }

    strimi_app.api.getAccounts([name], function(error, result) {
        if(error && error !== 'null') {
            errorView(strimi.languageText('global.user.login.alert.0'));
            return '';

        }
        if(result.length === 0){
            errorView(strimi.languageText('global.user.login.alert.0'));
            return '';
        }

        var roleType = strimi_app.keyRoles[2],
            publicPostingKey = result[0].posting.key_auths[0][0],
            isValid,
            setPwd;

        var isWif = strimi_app.auth.isWif(password);
        if(isWif) {
            var genPostingPublic = strimi_app.auth.wifToPublic(password);
            isValid = strimi_app.auth.wifIsValid(password, genPostingPublic);
            setPwd = password;
        } else {
            setPwd = strimi_app.auth.toWif(name, password, roleType);
            isValid = strimi_app.auth.wifIsValid(setPwd, publicPostingKey);
        }

        if(!isValid) {
            errorView(strimi.languageText('global.user.login.alert.1'));
            Raven.captureException( '[info] Account Verification Failed - invalid wif for account: ' + name );
            return '';
        }

        var privatePostingKey;
        var pass;
        var key;
        if(isWif) {
            pass = genPostingPublic;
            key = publicPostingKey;
        } else {
            privatePostingKey = strimi_app.auth.getPrivateKeys(name, password, [roleType]);
            privatePostingKey = privatePostingKey.posting;
            pass = setPwd;
            key = privatePostingKey;
        }

        if(pass !== key){
            errorView(strimi.languageText('global.user.login.alert.1'));
            Raven.captureException( '[alert] Posting key are not compatible for account: ' + name );
            return '';
        }

        var authorization = {
            "username": name,
            "public_keys": {
                "posting_wif": btoa(result[0].posting.key_auths[0][0]),
                "active_wif": btoa(result[0].active.key_auths[0][0])
            },
            "private_keys": {
                "posting_wif": btoa(setPwd)
            },
            "login_time": new Date().getTime()
        };

        localStorage.setItem("authorization", btoa(JSON.stringify(authorization)));
        localStorage.setItem("is_login", true);
        strimi_app.api.getRepliesByLastUpdate(name, '', 1, function(error, result) {
            if(result && result !== 'undefined') {
                if(result[0]) {
                    localStorage.setItem("notification_replies", result[0].id);
                } else {
                    localStorage.setItem("notification_replies", -1);
                }
            }
        });
        strimi_app.active_account();
        gtag('event', 'login', { 'event_category': name });
        $('#modalLogin').modal('hide');
        loading('end');
        location.reload();
        return true;
    });

};

strimi_app.is_login = function () {
    var isLogin = localStorage.getItem("is_login");
    if(!isLogin || isLogin !== 'true'){ return false; }
    return true;
};

strimi_app.active_account = function () {
    var accountMenu = document.getElementsByClassName('menu-account')[0];
    var myStrimi = document.getElementById('my-strimi');

    if(accountMenu && myStrimi) {
        if(strimi_app.is_login()){
            if (!accountMenu.classList.contains('active') && !myStrimi.classList.contains('hidden')) {
                accountMenu.className += ' active';
                myStrimi.className += ' hidden';

                /* Avatar */
                var avatarItem = document.getElementsByClassName('menu-account-avatar')[0];
                var account = strimi_app.authorization();
                var username = account.username;
                avatarItem.setAttribute(
                    'data-src',
                    avatarItem.getAttribute('data-src').replace("strimi", username)
                );

                /* Menu */
                var menuAccount = document.getElementsByClassName('menu-account-li');
                for (var i = 0; i < menuAccount.length; i++) {
                    var name = menuAccount[i];
                    name.innerHTML = name.innerHTML.replace("strimi", username);
                    name.setAttribute( 'href', name.getAttribute('href').replace("strimi", username) );
                }

                /* User Settings */
                var routeList = [
                    'user_account', 'user_account_feed', 'user_account_comments',
                    'user_account_replies', 'user_account_votes', 'user_account_followers', 'user_account_following', 'user_account_transfers', 'user_account_settings'
                ];
                if(routeList.includes(_config.route)) {
                    var isUser = function () {
                        var uri = window.location.href;
                        if(uri.indexOf(username) !== -1) {
                            return true;
                        }
                        return false;
                    };
                    if(isUser()) {
                        var bookmark = document.getElementsByClassName('strimi_app_settings')[0];
                        bookmark.setAttribute('class', bookmark.getAttribute('class').replace('strimi_app_settings', 'strimi_app_active'))
                    }
                    if(!isUser() && _config.route === 'user_account_settings') {
                        window.location.href = '/';
                    }
                }
            }
        } else {
            if (accountMenu.classList.contains('active') && myStrimi.classList.contains('hidden')) {
                accountMenu.classList.remove('active');
                myStrimi.classList.remove('hidden');
            }
        }
    }

};
strimi_app.active_account();

strimi_app.loginSubmit = function () {
    $("#loginForm").submit(function(e){
        e.preventDefault();
        var name = document.getElementById('username').value;
        var password = document.getElementById('password').value;
        strimi_app.verify(name, password);
        return false;
    });
};
strimi_app.loginSubmit();