<?php

namespace AppBundle\Controller\User\Account;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FollowingController extends Controller {

    /**
     * @param $username
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($username) {
        if(!$username) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $account = $this->get('api.users.account')->get($username);
        if(!$account) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $followers = $this->get('api.users.account.following')->get($account['name']);
        return $this->render(
            'AppBundle:User/Account:index.html.twig',array(
                'account' => $account,
                'follows'   => $followers
            )
        );

    }

    /**
     * @param $account
     * @param $author
     * @return null
     */
    public function nextFollowingAction($account, $author) {
        if(!$account || !$author ) { return null; }

        $code = 400;
        $account = $this->get('api.users.account')->get($account);
        $followers = $this->get('api.users.account.following')->following($account['name'], $author, 10);
        $followersContent = null;
        $nextUri = null;

        if($followers) {
            $code = 200;
            $lastFollowers =  end($followers);
            $nextUri = $this->generateUrl('user_account_ajax_following_next', array(
                'account'  => $author,
                'author'   => $lastFollowers['name']
            ));

            $followersContent = $this->renderView(
                'AppBundle:User/Account/Follows:fallowList.html.twig',
                array(
                    'follows' => $followers
                )
            );

        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'follows'    => $followersContent,
                'next_url' => $nextUri
            ),
            $code
        );
    }

}
