<?php

namespace ApiBundle\Services\Users;

class History {

    protected $api_connection;
    protected $json_parser;
    protected $posts;
    public function __construct($apiConnection, $jsonParser, $posts){
        $this->api_connection = $apiConnection;
        $this->json_parser = $jsonParser;
        $this->posts = $posts;
    }

    /**
     * @param $name
     * @return null
     */
    public function get($name){
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'users/' . $name . '/history', [
                'query' => [
                    'limit'   => 35,
                    'index'   => -1
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result'];
            if(is_array($result)) {
                foreach ($result as &$singleObject) {
                    if (isset($singleObject['json_metadata']) && !empty($singleObject['json_metadata'])) {
                        $singleObject = $this->posts->parserContent($singleObject);
                    }
                }
            }
            return $result;
        }catch(\Exception $ex){
            //Process the exception
        }

    }

}