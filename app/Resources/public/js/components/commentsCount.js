strimi.commentsCount = function () {

    if(localStorage.getItem("language") === 'pl') {
        var commentsCount =  document.getElementsByClassName('comments-count');
        if(commentsCount) {
            for (var i = 0; i < commentsCount.length; i++) {
                var count = commentsCount[i].getAttribute('data-count');
                var text = commentsCount[i].querySelector('[data-lang]');
                if(count == 0){
                    text.innerHTML = text.innerHTML + 'y';
                } else if(count >= 2 && count <= 4){
                    text.innerHTML = text.innerHTML + 'e';
                } else if(count >= 5){
                    text.innerHTML = text.innerHTML + 'y';
                }
            }
        }
    }

};
strimi.commentsCount();