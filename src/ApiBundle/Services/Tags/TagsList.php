<?php

namespace ApiBundle\Services\Tags;

class TagsList {

    protected $parser_sbd;
    protected $price_format;
    protected $json_parser;
    protected $api_connection;
    protected $memcached;
    protected $suggest_tags;
    protected $core_domain;
    public function __construct($parserSBD, $priceFormat, $jsonParser, $apiConnection, $cacheMemcached, $suggest_tags, $core_domain){
        $this->parser_sbd = $parserSBD;
        $this->price_format = $priceFormat;
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
        $this->suggest_tags = $suggest_tags;
        $this->core_domain = $core_domain;
    }

    /**
     * @param $tag
     * @return array|null
     */
    public function content($tag){
        if(!$tag) { return null; }
        $total_payouts = $this->parser_sbd->parser($tag['total_payouts']);
        $data = [
            'name'           => $tag['name'],
            'total_payouts'  => array(
                'number'  => preg_replace('/,/', '', $total_payouts),
                'format' => $total_payouts
            ),
            'count_posts'    => array(
                'number' => $tag['top_posts'],
                'format' => $this->price_format->parser($tag['top_posts'], 0)
            ),
            'count_comments' => array(
                'number' => $tag['comments'],
                'format' => $this->price_format->parser($tag['comments'], 0)
            ),
            'count_votes'    => $this->price_format->parser($tag['net_votes'], 0),
        ];
       return $data;
    }

    /**
     * @return array|null
     */
    public function getTagsList(){
        try{
            $response = $this->api_connection->init()->request('GET', 'tags', [
                'query' => [
                    'startTag' => null,
                    'limit'   => 50
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents())['result'];
            $data = array();
            if(!empty($result)) {
                foreach($result as $tag) {
                    if(isset($tag['name']) && $tag['name'] != null) {
                        $data[] = $this->content($tag);
                    }
                }
            }
            return $data;
        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @return array
     */
    public function lists(){

        $key = 'tags_lists';
        $tags = $this->memcached->get($key);
        if(!$tags) {
            $value = $this->getTagsList();
            $this->memcached->set($key, $value, 5, 'minutes');
        }
        $tags = $this->memcached->get($key);
        usort($tags, function($a, $b) {
            return $a['count_posts']['number'] - $b['count_posts']['number'];
        });
        return array_reverse($tags);
    }

    /**
     * @return array
     */
    public function sidebar(){
        $domain = $this->core_domain->currentDomainLocale();
        if($domain == 'en') {
            $key = 'tags_sidebar';
            $value = $this->getTagsList();
        } elseif ($domain == 'pl') {
            $key = 'tags_sidebar_polish';
            //$value = $this->suggest_tags->get('pl-', 50, true);
            $value = [
                [ 'name' => 'strimi-polish', 'count_posts' => [ 'format' => 13, 'number' => 13 ] ],
                [ 'name' => 'strimi-bitcoin', 'count_posts' => [ 'format' => 9, 'number' => 9 ] ],
                [ 'name' => 'strimi-kryptowaluty', 'count_posts' => [ 'format' => 6, 'number' => 6 ] ],
                [ 'name' => 'strimi-btc', 'count_posts' => [ 'format' => 3, 'number' => 3 ] ],
                [ 'name' => 'strimi-adsense', 'count_posts' => [ 'format' => 1, 'number' => 1 ] ],
                [ 'name' => 'strimi-android', 'count_posts' => [ 'format' => 1, 'number' => 1 ] ],
                [ 'name' => 'strimi-dieta', 'count_posts' => [ 'format' => 1, 'number' => 1 ] ],
                [ 'name' => 'strimi-heheszki', 'count_posts' => [ 'format' => 1, 'number' => 1 ] ],
                [ 'name' => 'strimi-motoryzacja', 'count_posts' => [ 'format' => 1, 'number' => 1 ] ],
                [ 'name' => 'strimi-poradnik', 'count_posts' => [ 'format' => 1, 'number' => 1 ] ],
                [ 'name' => 'strimi-security', 'count_posts' => [ 'format' => 1, 'number' => 1 ] ],
                [ 'name' => 'strimi-youtube', 'count_posts' => [ 'format' => 1, 'number' => 1 ] ]
            ];

        }

        $tags = $this->memcached->get($key);
        if(!$tags) {
            $this->memcached->set($key, $value, 5, 'minutes');
        }
        $tags = $this->memcached->get($key);
        if(!empty($tags) && count($tags) > 0) {
            usort($tags, function($a, $b) {
                return $a['count_posts']['number'] - $b['count_posts']['number'];
            });
            return array_reverse($tags);
        }
        return [];
    }

}