<?php

namespace ApiBundle\Services\Users;

class Following {

    protected $api_connection;
    protected $json_parser;
    protected $memcached;
    protected $accounts;
    public function __construct($apiConnection, $jsonParser, $memcached, $accounts){
        $this->api_connection = $apiConnection;
        $this->json_parser = $jsonParser;
        $this->memcached = $memcached;
        $this->accounts = $accounts;
    }

    /**
     * @param $followers
     * @return array|null
     */
    private function parser($followers) {
        $data = [];
        if(!$followers) { return null; }
        if(is_array($followers) && count($followers) > 0) {
            $data = [];
            foreach($followers as $follower){
                $data[] = $follower['following'];
            }

        }
        return $data;
    }

    /**
     * @param $name
     * @param null $startFollowing
     * @param int $limit
     * @return null
     */

    public function following($name, $startFollowing = null, $limit = 30){
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', '/users/follow/following', [
                'query' => [
                    'userName'       => $name,
                    'startFollowing' => $startFollowing,
                    'limit'          => $limit
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true)['result'];

            if(is_array($result) && count($result) > 0) {
                $result = $this->accounts->get(implode(',', $this->parser($result)), true);
            }

            return $result;
        }catch(\Exception $ex){
            //Process the exception
        }

    }

    /**
     * @param $key
     * @param $account
     * @return null
     */
    private function contentCache($key, $account){
        if(!$key || !$account) { return null; }
        $followers = $this->memcached->get($key);
        if(!$followers) {
            $value = $this->following($account, null);
            $this->memcached->set($key, $value, 3, 'minutes');
        }
        return $this->memcached->get($key);
    }

    /**
     * @param $name
     * @return null
     */
    public function get($name){
        if(!$name) { return null; }
        return $this->contentCache('account_' . md5($name) . '_following', $name);
    }

}