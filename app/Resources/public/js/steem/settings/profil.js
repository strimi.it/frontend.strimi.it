strimi_app.settingsProfilUpdate = function () {

    if(!strimi_app.is_login()) { return; }

    var account = strimi_app.authorization();
    var username = account.username,
        wifActive = '';
    var memoKey;

    var loading = function (submitButton, loadingStatus) {
        var loadingButton = document.getElementById('submit-update-account-loading');
        if (loadingStatus === 'start') {
            submitButton.className += ' active-loading';
            loadingButton.classList.remove('hidden');
        } else if (loadingStatus === 'end') {
            submitButton.classList.remove('active-loading');
            loadingButton.className += ' hidden';
        }
    };

    var reloadAccountData = function () {
        steem.api.getAccounts([username], function(error, result) {
            if (result && result !== 'undefined' && result[0]) {
                var account = result[0];
                memoKey =  result[0].memo_key;
                if(_.has(account, 'json_metadata')) {
                    var metadata = JSON.parse(account.json_metadata);
                    if(_.has(metadata, 'profile')) {
                        var profile = metadata.profile;
                        var about;
                        var cover_image;
                        var location;
                        var name;
                        var profile_image;
                        var website;

                        if(_.has(profile, 'name')) {
                            name = profile.name;
                            alias.value = name;
                            document.getElementById('account-alias').innerHTML = name;
                        }
                        var descriptions = document.getElementById('descriptions');
                        if(_.has(profile, 'about')) {
                            about = profile.about;
                            descriptions.value = about;
                            document.getElementById('account-about').innerHTML = about;
                        }
                        var background = document.getElementById('background');
                        if(_.has(profile, 'cover_image')) {
                            cover_image = profile.cover_image;
                            background.value = cover_image;
                        }
                        var locations = document.getElementById('location');
                        if(_.has(profile, 'location')) {
                            location = profile.location;
                            locations.value = location;
                            document.getElementById('account-location').innerHTML = location;
                        }
                        var avatar = document.getElementById('avatar');
                        if(_.has(profile, 'profile_image')) {
                            profile_image = profile.profile_image;
                            avatar.value = profile_image;
                        }
                        var websites = document.getElementById('website');
                        if(_.has(profile, 'website')) {
                            website = profile.website;
                            websites.value = website
                        }
                    }
                }
            } else if (error && error !== 'undefined') {
                console.log(error);
            }
        });
    };
    reloadAccountData();

    $("#profileUpdate").submit(function (e) {
        e.preventDefault();
        var alias = document.getElementById('alias');
        var about = document.getElementById('descriptions');
        var background = document.getElementById('background');
        var locations = document.getElementById('location');
        var avatar = document.getElementById('avatar');
        var websites = document.getElementById('website');
        var hash = document.getElementById('hash-edit-profil');

        var json_metadata = JSON.stringify({
            profile: {
                name: strimi_app.contentLength(alias.value, alias.getAttribute('length_max')),
                about: strimi_app.contentLength(about.value, about.getAttribute('length_max')),
                location: strimi_app.contentLength(locations.value, locations.getAttribute('length_max')),
                website: strimi_app.contentLength(websites.value, websites.getAttribute('length_max')),
                cover_image: strimi_app.contentLength(background.value, background.getAttribute('length_max')),
                profile_image: strimi_app.contentLength(avatar.value, avatar.getAttribute('length_max'))
            }
        });

        var submitButton = document.getElementById('submit-update-account');
        loading(submitButton, 'start');

        strimi_app.broadcast.accountUpdate(hash.value, username, undefined, undefined, undefined, memoKey, json_metadata, function(error, result) {
            if (result && result !== 'undefined') {
                hash.value = '';
                strimi.prettyNotification(
                    strimi.languageText('global.user.account.settings.profile.success.0'),
                    strimi.languageText('global.user.account.settings.profile.success.1'),
                    'success'
                );
                reloadAccountData();
            } else if (error && error !== 'undefined') {
                console.log( error );
            }
            loading(submitButton, 'end');
        });
    });


};
var routeList = [ 'user_account_settings' ];
if(routeList.includes(_config.route)) {
    if(!strimi_app.is_login()) {
        window.location.href = '/';
    } else {
        strimi_app.settingsProfilUpdate();
    }
}