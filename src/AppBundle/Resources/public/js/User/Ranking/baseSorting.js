strimi.AccountRankingSortingBase = function () {

    var nextButton = document.getElementById('ranking-next-button');
    var setTableContent = function (url) {
        $.ajax({
            type: 'GET',
            url: url,
            async: true,
            beforeSend: function() {
            },
            success: function(getResponse) {
                if (getResponse.code == 200) {
                    document.getElementsByTagName('tbody')[0].innerHTML = getResponse.data.accounts;

                    nextButton.setAttribute('data-url', getResponse.data.next_url);
                    nextButton.setAttribute('data-status', 0);
                    nextButton.classList.remove('active');

                    strimi.imageLazyload();
                } else if (getResponse.code == 400) {
                }
            },
            error: function() {
            }
        });
    };

    /* Change Sorting */
    $('.ranking-sorting').click(function() {

        /* Get Comments */
        var order = $(this).attr('data-order');
        var url = $(this).attr('data-url');
        var new_uri;
        if(order === 'asc'){
            new_uri = url.substr(0, url.lastIndexOf("/")) + '/desc';
            $(this).attr('data-order', 'desc');
        } else if(order === 'desc'){
            new_uri = url.substr(0, url.lastIndexOf("/")) + '/asc';
            $(this).attr('data-order', 'asc');
        }
        $(this).attr('data-url', new_uri);
        setTableContent(url);

        /* Clean Active */
        var buttons = document.getElementsByClassName('ranking-sorting');
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].classList.remove('active');
        }

        /* Current Active */
        $(this).addClass('active');

    });

};


if(_config.route === 'user_ranking') {
    strimi.AccountRankingSortingBase();
}