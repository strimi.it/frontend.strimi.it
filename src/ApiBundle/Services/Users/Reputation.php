<?php

namespace ApiBundle\Services\Users;

class Reputation {

    /**
     * @param $reputation
     * @return int
     */
    public function convert($reputation){
        if(!$reputation) { return $reputation; }
        $negative = false;
        if ($reputation[0] == '-') {
            $negative = true;
        }
        $reputation = (int)$reputation;
        $negative ? substr($reputation, 1) : $reputation;
        $leadingDigits = (int)substr((string)$reputation, 0, 4);
        $log = log($leadingDigits) / log(10);
        $n = strlen($reputation) - 1;
        $out = $n + ($log - (int)$log);
        if (is_nan($out)) {
            $out = 0;
        }
        $out = max($out - 9, 0);
        $out = ($negative ? -1 : 1) * $out;
        $out = $out * 9 + 25;
        return (int)$out;
    }



}