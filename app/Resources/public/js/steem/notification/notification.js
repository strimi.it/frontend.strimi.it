strimi_app.notification = function () {
    if(!strimi_app.is_login()) { return; }

    var account = strimi_app.authorization();
    var username = account.username;
    var socket = new WebSocket(strimi_app.config.base.wss + 'websocket/notifications');

    var msg = { code: 0,"username": username, tags: [] };

    socket.onopen = function () {
        socket.send(JSON.stringify(msg));
    };

    var notificationIcon = document.getElementsByClassName('notification')[0];
    var notificationUri = notificationIcon.getAttribute('data-url');
    var notificationCount = notificationIcon.getElementsByClassName('count')[0];
    var notificationClear = document.getElementById('notification-clear');
    var sumCount;
    var codeList = [4, 6, 8, 9, 11];

    $('#notification-modal').slimScroll({
        height: '314px',
        railVisible: true,
        size: '7px'
    });

    notificationIcon.onclick = function () {
        localStorage.setItem("notification_count", 0);
        notificationCount.innerHTML = 0;
        // var title = document.title;
        // document.title = title.substring(title.indexOf(" ") + 1);
        notificationIcon.classList.remove("active");
    };
/*
    notificationClear.onclick = function () {
        localStorage.setItem("notification", "");
        document.getElementById('notification-modal').innerHTML = '';
    };
*/


    var reloadNotification = function () {
        var uri = notificationUri.replace('notiwss', username);
        $.ajax({
            type: 'GET',
            url: uri,
            async: true,
            beforeSend: function() {
            },
            success: function(getResponse) {
                if (getResponse.code === 200) {
                    var content = document.getElementById('notification-modal');
                    content.innerHTML = getResponse.data;
                    strimi.momentTimezone();
                    strimi.language();
                    strimi.imageLazyload();
                } else if (getResponse.code === 400) {
                }
            },
            error: function() {
            }
        });
        var getNotification = localStorage.getItem("notification");
        var content = document.getElementById('notification-modal');
            content.innerHTML = '';
            if(getNotification !== null) {
                content.innerHTML = getNotification;
            }
        strimi.momentTimezone();
        strimi.language();
        $('#modalNotification').on('shown.bs.modal', function() {
            strimi.imageLazyload();
        });
        /* Count */
        var getCount = localStorage.getItem("notification_count");
        if(getCount !== null && getCount > 0) {
            notificationCount.innerHTML = localStorage.getItem("notification_count");
            notificationIcon.className += ' active';
        }
    };
    reloadNotification();

    var infoPostStreamContent = document.getElementById('info-post-stream');
    var renderStreamContent = function () {
        var url = infoPostStreamContent.getAttribute('data-url');
        url = url.substr(0, url.lastIndexOf("/")) + '/' + infoPostStreamContent.getAttribute('data-count');
        $.ajax({
            type: 'GET',
            url: url,
            async: true,
            beforeSend: function() {
            },
            success: function(getResponse) {
                if (getResponse.code == 200) {
                    $('#stream-post-content').prepend( getResponse.data );
                    strimi.imageLazyload();
                    strimi.momentTimezone();
                    strimi.commentsCount();
                    strimi.language();
                    strimi_app.reloaded();
                    infoPostStreamContent.setAttribute('data-count', 0);
                } else if (getResponse.code == 400) {
                }
            },
            error: function() {
            }
        });

    };
    if(infoPostStreamContent) {
        infoPostStreamContent.onclick = function () {
            infoPostStreamContent.classList.remove('active');
            renderStreamContent();
        };
    }

    socket.onmessage = function (response) {
        if(response.data) {
            var data = JSON.parse(response.data);
            var status = document.getElementById('notification-modal').getAttribute('data-status');
            if (data && data !== 'undefined' && codeList.includes(data.code)) {
                var code = Number(data.code);
                console.log( "code: " + code );
                /* Stream */
                if(code === 11) {
                    if(infoPostStreamContent) {
                        if(!infoPostStreamContent.classList.contains('active')){
                            infoPostStreamContent.className += ' active';
                        }
                        var olderCount = infoPostStreamContent.getAttribute('data-count');
                        var counter = Number(olderCount) + 1;
                        var count = document.getElementById('info-post-stream-count');
                        infoPostStreamContent.setAttribute('data-count', counter);
                        count.innerText = counter;
                    }
                } else {
                    var getCount = localStorage.getItem("notification_count");
                    if(getCount !== null) {
                        localStorage.setItem("notification_count", Number(getCount) +  1);
                    } else {
                        localStorage.setItem("notification_count", 1);
                    }
                    notificationCount.innerHTML = localStorage.getItem("notification_count");

                    if(!notificationIcon.classList.contains('active')) {

                        reloadNotification();
                        notificationIcon.className += ' active';
                    }
                }




            }

        }

    };

};
strimi_app.notification();