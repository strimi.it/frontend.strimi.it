strimi_app.linkTags = function (nsfwDomain, contentType) {
    if(!nsfwDomain || !contentType) { return '' };
    var tags = document.getElementsByClassName('link-tags')[0].value;
    if(tags) {
        tags = tags.split(" ");
        tags.forEach(function(value, index) {
            value = value.replace(/#/g, '');
            value = value.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "");
            value = value.replace(/strimi/g, '');
            tags[index] = value;
            if(value) {
                value = value.replace(/^-/, '').replace(/-\s*$/, "");
                value = value.toLowerCase();
                tags[index] = contentType + '-' + value;
            }
        });
        tags = tags.slice(0, 4);
        tags = _.compact(tags);
        tags = ['strimi'].concat( tags.concat(nsfwDomain) );
        //var data =  _.compact(tags);
        return  _.compact(tags);
    }
    return '';
};