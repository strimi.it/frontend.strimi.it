<?php

namespace ApiBundle\Services\Users;

class VoteWorth {

    protected $price_format;
    protected $api_connection;
    protected $reward_found;
    protected $mediana_history_price;

    public function __construct($priceFormat, $apiConnection, $rewardFound, $medianaHistoryPrice){
        $this->price_format = $priceFormat;
        $this->api_connection = $apiConnection;
        $this->reward_found = $rewardFound;
        $this->mediana_history_price = $medianaHistoryPrice;
    }

    /**
     * @return float|int
     */
    private function rewardFundParameter(){
        $rewardFund = $this->reward_found->get();
        $rewardBalance = (float)str_replace(' STEEM', '', $rewardFund['reward_balance']['value']);
        return $rewardBalance / (float)$rewardFund['recent_claims'];
    }

    /**
     * @return float|int
     */
    private function medianHistoryPrice() {
        $medianHistoryPrice = $this->mediana_history_price->get();
        if(!empty($medianHistoryPrice)) {
            $base = str_replace(' SBD', '', $medianHistoryPrice['base']);
            $quote = str_replace(' STEEM', '', $medianHistoryPrice['quote']);
            return (float)$base / (float)$quote;
        }
        return 0;
    }

    /**
     * @param $account
     * @param int $voteWeight
     * @return mixed
     */
    public function get($account, $voteWeight = 100){
        $votingPower = $account['voting_power']['voting_power'];
        $now = new \DateTime();
        $lastVoteTime = new \DateTime($account['last_vote_time'] . 'Z');
        $lastVoteTime = ($now->getTimestamp() - $lastVoteTime->getTimestamp());

        $h = 432e3;
        $v = 1e4;

        $votingPower = round(($votingPower + $v * $lastVoteTime / $h) / 100, 2);
        $checkVotingPower = $votingPower > 100 ? 100 : $votingPower;

        $c = (int)(100 * $checkVotingPower * (100 * $voteWeight) / $v);
        $c = (int)(($c + 49) / 50);

        $vestingShares = (int)str_replace(' VESTS', '', $account['vesting_shares']);
        $receivedVestingShares = (int)str_replace(' VESTS', '', $account['received_vesting_shares']);
        $delegatedVestingShares = (int)str_replace(' VESTS', '', $account['delegated_vesting_shares']);

        $i = $this->rewardFundParameter();
        $l = $this->medianHistoryPrice();

        $voteValue = (int)(($vestingShares + $receivedVestingShares - $delegatedVestingShares) * $c * 100) * $i * $l;

        return $this->price_format->parser($voteValue);
    }

}