<?php

namespace ApiBundle\Services\Connection\Strimi;
use AppBundle\Services\Pushover\Send as sendPushover;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\Client;

class Connection {

    protected $api_key;
    protected $requestStack;
    protected $pushover;
    public function __construct($api_key, RequestStack $requestStack, sendPushover $pushover){
        $this->api_key = $api_key;
        $this->requestStack = $requestStack;
        $this->pushover = $pushover;
    }

    /**
     * @return Client
     */
    public function init(){
        try {
            return new Client([
                'base_uri'        => 'http://api.strimi/',
                'headers' => [
                    'Access-Login' => 'strimi',
                    'Access-Token' =>  $this->api_key
                ],
                'timeout'         => 15,
                'connect_timeout' => 15,

            ]);
            //} catch(BadResponseException $ex){
        }catch(\Exception $ex){
            $requests = $this->requestStack;
            $currentRequest = $requests->getCurrentRequest();
            $type = 'api.strimi.it - FAIL!';
            $uri = $currentRequest->getUri();
            $route = $currentRequest->attributes->get('_route');
            $host = $currentRequest->getHost();
            $this->pushover->send($type, $uri, $route, $host);
        }
    }
}