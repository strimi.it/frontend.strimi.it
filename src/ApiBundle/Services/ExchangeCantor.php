<?php

namespace ApiBundle\Services;
use GuzzleHttp\Client;

class ExchangeCantor {

    protected $price_format;
    protected $json_parser;
    protected $memcached;

    public function __construct($priceFormat, $jsonParser, $cacheMemcached){
        $this->price_format = $priceFormat;
        $this->json_parser = $jsonParser;
        $this->memcached = $cacheMemcached;
    }

    private function connection($url){
        return new Client([
            'base_uri' => $url,
            'timeout'  => 3,
        ]);
    }

    /**
     * @return array|null
     */
    private function bittrex(){
        try{
            $connection = $this->connection('https://bittrex.com/api/v1.1/public/');
            $btcSteem = $connection->request('GET', 'getmarketsummary', [
                'query' => [
                    'market' => 'btc-steem'
                ]
            ]);
            $btcUsd = $connection->request('GET', 'getmarketsummary', [
                'query' => [
                    'market' => 'usdt-btc'
                ]
            ]);

            if(!$btcSteem || !$btcUsd) { return null; }

            $steem = $this->json_parser->singleObject($btcSteem->getBody()->getContents())['result'][0]['Bid'];
            $usd = $this->json_parser->singleObject($btcUsd->getBody()->getContents())['result'][0]['Bid'];

            return array(
                'steemToBTC' => $steem,
                'steemToUSD' => $this->price_format->parser(($steem * $usd)),
                'btcToUSD'   => $this->price_format->parser($usd)
            );


        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @return array|null
     */
    private function bitmarket(){
        try{
            $connection = $this->connection('https://www.bitmarket.pl/json/');
            $btcPLN = $connection->request('GET', 'BTCPLN/ticker.json');
            $btcEUR = $connection->request('GET', 'BTCEUR/ticker.json');

            if(!$btcPLN || !$btcEUR){ return null; }

            $pln = $this->json_parser->singleObject($btcPLN->getBody()->getContents())['bid'];
            $eur = $this->json_parser->singleObject($btcEUR->getBody()->getContents())['bid'];
            $bittrex = $this->bittrex();

            return array(
                'btcToPLN'   => $this->price_format->parser($pln),
                'btcToEUR'   => $this->price_format->parser($eur),
                'steemToPLN' => $this->price_format->parser($pln * $bittrex['steemToBTC']),
                'steemToEUR' => $this->price_format->parser($eur * $bittrex['steemToBTC'])
            );

        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @return array
     */
    public function coinmarketcap(){
        try{

            $connection = $this->connection('https://api.coinmarketcap.com/v1/ticker/steem-dollars/?convert=');
            $pln = $connection->request('GET', '?convert=pln');
            $pln = $this->json_parser->singleObject($pln->getBody()->getContents())[0];

            $connection_bitmarket = $this->connection('https://www.bitmarket.pl/json/');
            $btcEUR = $connection_bitmarket->request('GET', 'BTCEUR/ticker.json');
            $eur = $this->json_parser->singleObject($btcEUR->getBody()->getContents())['bid'];

            return array(
                'sbdToBTC'   => $pln['price_btc'],
                'sbdToUSD'   => $this->price_format->parser($pln['price_usd']),
                'sbdToPLN'   => $this->price_format->parser($pln['price_pln']),
                'sbdToEUR'   => $this->price_format->parser($pln['price_btc'] * $eur),

            );

        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @return array|null
     */
    private function exchange(){
        $bittrex = $this->bittrex();
        $bitmarket = $this->bitmarket($bittrex);
        $coinmarketcap = $this->coinmarketcap();

        if(!$bittrex || !$bitmarket || !$coinmarketcap) { return null; }

        return array(
            'btcToUSD'   => $bittrex['btcToUSD'],
            'btcToPLN'   => $bitmarket['btcToPLN'],
            'btcToEUR'   => $bitmarket['btcToEUR'],
            'steemToBTC' => $bittrex['steemToBTC'],
            'steemToUSD' => $bittrex['steemToUSD'],
            'steemToPLN' => $bitmarket['steemToPLN'],
            'steemToEUR' => $bitmarket['steemToEUR'],
            'sbdToBTC'   => $coinmarketcap['sbdToBTC'],
            'sbdToUSD'   => $coinmarketcap['sbdToUSD'],
            'sbdToPLN'   => $coinmarketcap['sbdToPLN'],
            'sbdToEUR'   => $coinmarketcap['sbdToEUR']
        );
    }

    /**
     * @return mixed
     */
    public function get(){
        $key = 'exchange_cantor';
        $exchange = $this->memcached->get($key);
        if(!$exchange) {
            $value = $this->exchange();
            $exchange = $this->memcached->set($key, $value, 60, 'seconds'); // 60s
        }
        if(!$exchange) {
            return $this->exchange();
        }
        return  $this->memcached->get($key);
    }

}