<?php

namespace ApiBundle\Services\Elastic\User;

class Ranking {

    protected $json_parser;
    protected $api_connection;
    protected $parser_sbd;
    protected $price_format;
    protected $parser_steem;
    public function __construct($jsonParser, $apiConnection, $parserSBD, $priceFormat, $parserSteem){
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->parser_sbd = $parserSBD;
        $this->price_format = $priceFormat;
        $this->parser_steem = $parserSteem;
    }

    /**
     * @param $content
     * @param int $starID
     * @return array|null
     */
    public function content($content, $starID = 1){
        if(!$content) { return null; }
        $data = array();
        foreach($content as &$user) {
            $data[] = [
                'id'                     => $starID++,
                'name'                   => $user['_source']['name'],
                'registration'           => $user['_source']['registrationTimestamp'],
                'last_activity'          => $user['_source']['lastActivityTimestamp'],
                'post_count'             => $this->price_format->parser($user['_source']['postCount'], 0),
                'steem_balance'          => $this->parser_steem->parser($user['_source']['steemBalance']),
                'steem_dolar_balance'    => $this->parser_sbd->parser($user['_source']['steemDolarBalance']),
                'steem_power'            => $this->price_format->parser($user['_source']['steemPower'], 2),
                'estimate_account_value' => $this->price_format->parser($user['_source']['estimateAccountValue'], 2),
                'vote_worth'             => $this->price_format->parser($user['_source']['upvoteWorth'], 2),
                'follower_count'         => $this->price_format->parser($user['_source']['followerCount'], 0),
                'following_count'        => $this->price_format->parser($user['_source']['followingCount'], 0),
                'reputation'             => $user['_source']['reputation'],
                'ranking'                => $this->price_format->parser($user['_source']['ranking'], 0)
            ];
        }
        return $data;
    }

    /**
     * @param string $sortBy
     * @param int $from
     * @param string $sortOrder
     * @param int $starID
     * @return array|null
     */
    public function get($sortBy = 'upvoteWorth', $from = 0, $sortOrder = 'desc', $starID = 1){
        try{
            $response = $this->api_connection->init()->request('GET', '/elastic/users', [
                'query' => [
                    'sortBy'    => $sortBy,
                    'sortOrder' => $sortOrder,
                    'limit'     => 40,
                    'from'      => $from
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents())['hits']['hits'];
            if(!$result) { return null; }

            return $this->content($result, $starID);

        }catch(\Exception $ex){
            //Process the exception
        }

    }

}