<?php

namespace AppBundle\Controller\Tag\Ranking;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RankingNextController extends Controller
{

    /**
     * @param $tag
     * @return null
     */
    public function indexAction($tag) {
        if(!$tag) { return null; }
        $code = 400;
        $tags = $this->get('api.tags.next')->get($tag);
        $tagsContent = null;
        $nextUri = null;
        if($tags) {
            $code = 200;
            $nextUri = $this->generateUrl('tag_ranking_ajax_next', array(
                'tag'      => end($tags)['name']
            ));
            $tagsContent = $this->renderView(
                'AppBundle:Tag/Ranking:trContent.html.twig',
                array(
                    'tags' => $tags
                )
            );
        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'tags'    => $tagsContent,
                'next_url' => $nextUri
            ),
            $code
        );
    }


}
