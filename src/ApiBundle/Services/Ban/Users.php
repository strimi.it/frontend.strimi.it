<?php

namespace ApiBundle\Services\Ban;

class Users  {

    protected $api_connection;
    protected $memcached;
    protected $jsonParser;
    protected $paginator;
    public function __construct($apiConnection, $cacheMemcached, $jsonParser, $paginator) {
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
        $this->json_parser = $jsonParser;
        $this->paginator = $paginator;
    }

    public function content(){
        try{
            $response = $this->api_connection->init()->request('GET', '/mongo/bans/users');
            if($response->getStatusCode() != 200) { return null; }
            $result = json_decode($response->getBody()->getContents(), true);
            return array_reverse($result);
        }catch(\Exception $ex){
        }
    }
    
    public function get($username) {

        $content = $this->content();


    }

}