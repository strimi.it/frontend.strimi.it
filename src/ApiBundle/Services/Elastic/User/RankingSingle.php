<?php

namespace ApiBundle\Services\Elastic\User;

class RankingSingle {

    protected $json_parser;
    protected $api_connection;
    public function __construct($jsonParser, $apiConnection){
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
    }

    /**
     * @param $name
     * @return null
     */
    public function get($name){
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', '/elastic/users/'. $name . '/ranking', [
                'query' => [
                    'userName' => $name
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents());
            if(!$result) { return null; }
            return $result['hits']['hits'][0]['_source']['ranking'];
        }catch(\Exception $ex){
            //Process the exception
        }

    }

}