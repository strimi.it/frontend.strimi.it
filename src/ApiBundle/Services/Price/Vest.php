<?php

namespace ApiBundle\Services\Price;

class Vest {

    /**
     * @param $content
     * @return null|string|string[]
     */
    public function clean($content){
        if(!$content) { return null; }
        return preg_replace('/ VESTS/', '', $content);
    }

}
