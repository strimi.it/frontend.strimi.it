<?php

namespace ApiBundle\Controller\Suggest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TagsAutocompleteController extends Controller {


    /**
     * @param $name
     * @return null
     */
    public function indexAction($name)
    {

        if (!$name) {
            return null;
        }
        $code = 200;
        $tags = $this->get('api.elastic.suggest.tags')->get($name, 35, false, true);
        $tagsContent = null;
        if ($tags) {
            $data = [];
            foreach ($tags as &$tag) {
                $data[] = [
                    'name' => $tag['name'],
                    'count_posts' => $tag['count_posts']['format']
                ];
            }
            $tagsContent = $data;
        }

        return $this->get('app.json.response')->renderDefault(
            $tagsContent,
            $code
        );

    }



}
