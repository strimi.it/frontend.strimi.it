<?php

namespace ApiBundle\Services\Posts;
use ApiBundle\Services\Connection\Strimi\Connection as Strimi;
use ApiBundle\Services\Json\Parser;
use AppBundle\Services\Cache\Memcached;
use ApiBundle\Services\Posts\Defaults as postDefaults;

class Single {

    protected $json_parser;
    protected $api_connection;
    protected $memcached;
    protected $defaults;
    protected $app_blocking;
    public function __construct(Parser $jsonParser, Strimi $apiConnection, Memcached $cacheMemcached, postDefaults $defaults, $app_blocking){
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
        $this->defaults = $defaults;
        $this->app_blocking = $app_blocking;
    }

    /**
     * @param $author
     * @param $permlink
     * @return null
     */
    public function content($author, $permlink){
        if(!$author || !$permlink) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'posts', [
                'query' => [
                    'authorName' => $author,
                    'permlink'   => $permlink
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }

            $result = $this->json_parser->singleObject($response->getBody()->getContents())['result'];
            $result = $this->defaults->parserContent($result);

            return $result;

        }catch(\Exception $ex){
        }
    }

    private function isNSFW($post){
        if(empty($post)) { return null; }
        if($post['category'] == 'nsfw' || $post['category'] == 'strimi-nsfw') {
            return null;
        }

        if(!empty($post['json_metadata']['tags']) && is_array($post['json_metadata']['tags'])) {
            $tags = $post['json_metadata']['tags'];
            if (in_array("nsfw", $tags) || in_array("strimi-nsfw", $tags)) {
                return null;
            }
        }

        return $post;
    }

    /**
     * @param $key
     * @param $author
     * @param $permlink
     * @return mixed|null
     */
    private function contentCache($key, $author, $permlink){
        if(!$key || !$author || !$permlink) { return null; }

        $post = $this->memcached->get($key);
        if(!$post) {
            $value = $this->content($author, $permlink);
            $post = $this->memcached->set($key, $value, 1, 'minutes');
        }
        if(!$post) {
            return $this->content($author, $permlink);
        }
        return $this->memcached->get($key);
    }

    /**
     * @param $author
     * @param $permlink
     * @return mixed|null
     */
    public function get($author, $permlink){
        if(!$author || !$permlink) { return null; }
        $isUserBanned = $this->app_blocking->isUserBanned($author);
        if(!$isUserBanned) {
            $content = $this->contentCache('post_single_' . md5( $author . $permlink ), $author, $permlink);
            $post = $this->isNSFW($content);
            return $post;
        }
        return null;
    }

}