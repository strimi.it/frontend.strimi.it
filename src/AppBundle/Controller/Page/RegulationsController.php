<?php

namespace AppBundle\Controller\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RegulationsController extends Controller {

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        return $this->render(
            'AppBundle:Page:regulations.html.twig'
        );
    }

}
