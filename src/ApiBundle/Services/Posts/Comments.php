<?php

namespace ApiBundle\Services\Posts;

class Comments {

    protected $json_parser;
    protected $api_connection;
    protected $memcached;
    protected $defaults;

    public function __construct($jsonParser, $apiConnection, $cacheMemcached, $defaults){
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
        $this->defaults = $defaults;
    }

    /**
     * @param $author
     * @param $permlink
     * @return null
     */
    private function query($author, $permlink){
        if(!$author || !$permlink) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'comments', [
                'query' => [
                    'authorName' => $author,
                    'permlink'   => $permlink
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents())['result'];

            return $result;

        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @param $author
     * @param $permlink
     * @return null
     */
    private function content($author, $permlink){
        if(!$author || !$permlink) { return null; }
        $result = $this->query($author, $permlink);
        if(is_array($result) && count($result) > 0) {
            foreach($result as &$single){
                $single = $this->defaults->parserContent($single);
                if($single['children'] > 0){
                    $single['comments_children'] = $this->content($single['author'], $single['permlink']);
                } else {
                    $single['comments_children'] = 0;
                };
            }
        }
        return $result;
    }

    /**
     * @param $key
     * @param $author
     * @param $permlink
     * @return null
     */
    private function contentCache($key, $author, $permlink){
        if(!$key || !$author || !$permlink) { return null; }
        $comments = $this->memcached->get($key);
        if(!$comments) {
            $value = $this->content($author, $permlink);
            $comments = $this->memcached->set($key, $value, 60, 'seconds'); // 60s
        }
        if(!$comments) {
            return $this->content($author, $permlink);
        }
        return $this->memcached->get($key);
    }

    /**
     * @param $author
     * @param $permlink
     * @param $sorting
     * @return array|null
     */
    public function get($author, $permlink, $sorting){
        if(!$author || !$permlink || !$sorting) { return null; }
        $result = $this->contentCache(md5( 'post_comments_' . $author . $permlink ), $author, $permlink);
        if(!empty($result) && count($result) > 0) {
            if($sorting == 'price') {
                foreach ($result as $key => $row) {
                    $price[$key] = $row['pending_payout_value'];
                }
                array_multisort($price, SORT_NUMERIC, $result);
                $result = array_reverse($result, true);

            } elseif($sorting == 'new') {
                usort($result, function($a, $b) {
                    $first = new \DateTime($a['created']);
                    $second = new \DateTime($b['created']);
                    if ($first == $second) { return null; }
                    return $first > $second ? -1 : 1;
                });
            } elseif($sorting == 'old') {
                usort($result, function($a, $b) {
                    $first = new \DateTime($a['created']);
                    $second = new \DateTime($b['created']);
                    if ($first == $second) { return null; }
                    return $first < $second ? -1 : 1;
                });
            }
        }
        return $result;
    }

}