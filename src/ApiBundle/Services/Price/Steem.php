<?php

namespace ApiBundle\Services\Price;

class Steem {

    protected $parser;
    public function __construct($parser){
        $this->parser = $parser;
    }

    /**
     * @param $content
     * @return null
     */
    public function parser($content){
        if(!$content) { return null; }
        $price = preg_replace('/ STEEM/', '', $content);
        return $this->parser->parser($price, 3);
    }

    /**
     * @param $content
     * @return null|string|string[]
     */
    public function clean($content){
        if(!$content) { return null; }
        return preg_replace('/ STEEM/', '', $content);
    }

}
