strimi_app.streamSubmit = function () {

    if (!strimi_app.is_login()) { return; }

    var account = strimi_app.authorization();
    var username = account.username,
        wif = account.private_keys.posting_wif;

    $("#clear-stream-post").click(function () {
        document.getElementsByClassName('stream-description')[0].value = '';
    });

    var loading = function (submitButton, loadingStatus) {
        var loadingButton = document.getElementById('submit-stream-post-loading');
        if (loadingStatus === 'start') {
            submitButton.className += ' active-loading';
            loadingButton.classList.remove('hidden');
        } else if (loadingStatus === 'end') {
            submitButton.classList.remove('active-loading');
            loadingButton.className += ' hidden';
        }
    };

    var render = function (url, username, permlink) {
        url = url.replace("author", username);
        url = url.replace("permlink", permlink);
        $.ajax({
            type: 'GET',
            url: url,
            async: true,
            beforeSend: function() {
            },
            success: function(getResponse) {
                if (getResponse.code == 200) {
                    $("#stream-post-content").prepend(getResponse.data);
                    strimi.imageLazyload();
                    strimi.momentTimezone();
                    strimi.language();
                    strimi_app.reloaded();
                    document.getElementsByClassName('stream-description')[0].value = '';
                } else if (getResponse.code == 400) {
                }
            },
            error: function() {
            }
        });
    };

    $("#form-stream").submit(function (e) {
        e.preventDefault();

        var submitButton = document.getElementById('submit-stream-post');
        if (submitButton.getAttribute('data-status') === '0') {
            submitButton.setAttribute('data-status', 1);

            loading(submitButton, 'start');

            /* Live start */
            //https://steemit.com/strimi-stream/@strimi/strimi-stream-live-1

            // var parentAuthor = strimi_app.config.base.name;
            // var parentPermlink = _config.strimi_stream;
            var body = document.getElementsByClassName('stream-description')[0];
            var title = body.value.substring(body.value.length - 80, body.value.length);
            var payout = strimi_app.payoutType('steem_power');
            var permlink = strimi_app.createPermlink(body.value);

            var strimiMetadata = {
                body: body.value,
                type: {
                    type: "stream"
                }
            };

            var operations = [
                ['comment',
                    {
                        parent_author: '',
                        parent_permlink: 'strimi-stream',
                        author: username,
                        permlink: permlink,
                        title: title,
                        body: body.value,
                        json_metadata : JSON.stringify({
                            tags: ['strimi-stream'],
                            format: "html",
                            app: strimi_app.config.base.compilation,
                            strimi_metadata: JSON.stringify(strimiMetadata)
                        })
                    }
                ],
                ['comment_options', {
                    author: username,
                    permlink: permlink,
                    max_accepted_payout: payout[0],
                    percent_steem_dollars: payout[1],
                    allow_votes: true,
                    allow_curation_rewards: true,
                    extensions: strimi_app.config.beneficiaries
                }]
            ];

            strimi_app.broadcast.sendAsync(
                { extensions: [], operations: operations  },
                { posting: wif }, function(error, result){
                    if (result && result !== 'undefined') {
                        loading(submitButton, 'end');
                        render(submitButton.getAttribute('data-url'), username, permlink);
                        submitButton.setAttribute('data-status', 0);
                        gtag('event', 'submit-stream', { 'event_category': username, 'event_label': permlink });
                        body.value = '';
                    } else if (error && error !== 'undefined') {
                        console.log( error );
                        strimi.prettyNotification('Error!', 'An unexpected error occurred :/');
                        submitButton.setAttribute('data-status', 0);
                        loading(submitButton, 'end');
                        Raven.captureException(error);
                    }
                });

        }

    });

};
strimi_app.streamSubmit();