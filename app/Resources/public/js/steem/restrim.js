strimi_app.restrim = function () {
    if(!strimi_app.is_login()) { return; }

    var account = strimi_app.authorization();
    var username = account.username,
        wif = account.private_keys.posting_wif;
    var submit = document.getElementById('submit-restrim');

    $( ".post-restrim" ).click(function() {
        var id = $(this).attr('data-id');
        var post = document.getElementById('post-single-' + id);
        var title = post.querySelector('.short-content');
            title = title.querySelector('h2');
        if(title){
            title = title.querySelector('a').innerHTML;
        } else {
            title = title.querySelector('h1').querySelector('a').innerHTML;
        }
        document.getElementById('reStrimTitle').innerHTML = title;
        submit.setAttribute('data-short-permlink', post.getAttribute('data-short-permlink'));
        submit.setAttribute('data-title', title);
    });

    document.getElementById('submit-restrim').onclick = function () {
        var short = this.getAttribute('data-short-permlink');
            short = short.split('/');
        var author = short[0];
        var permlink = short[1];
        var title = this.getAttribute('data-title');
        var data = JSON.stringify([ 'reblog', { account: username, author: author, permlink: permlink }]);
        strimi_app.broadcast.customJson(wif, [], [username], 'follow', data,  function(error, result) {
            if(result && result !== 'undefined') {
                strimi.prettyNotification(strimi.languageText('global.post.footer.restrim_success.title'), title, 'success');
                gtag('event', 'restrim', { 'event_category': username, 'event_label': permlink });
            } else if (error && result !== 'undefined'){
                console.log( error );
            }
        });
    };

};
strimi_app.restrim();