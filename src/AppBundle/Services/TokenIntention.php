<?php

namespace AppBundle\Services;

class TokenIntention {

    protected $token_manager;
    public function __construct($token_manager){
        $this->token_manager = $token_manager;
    }

    public function get(){
        return $this->token_manager->getToken('intention')->getValue();
    }

}