<?php

namespace ApiBundle\Services\Elastic\Suggest;

class Accounts {

    protected $parser_sbd;
    protected $price_format;
    protected $json_parser;
    protected $api_connection;
    protected $memcached;
    protected $ranking;
    public function __construct($parserSBD, $priceFormat, $jsonParser, $apiConnection, $cacheMemcached, $ranking){
        $this->parser_sbd = $parserSBD;
        $this->price_format = $priceFormat;
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->memcached = $cacheMemcached;
        $this->ranking = $ranking;
    }


    /**
     * @param $name
     * @param int $limit
     * @return null
     */
    public function get($name, $limit = 40){
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', '/elastic/users/suggest', [
                'query' => [
                    'suggestFor' => $name,
                    'limit'      => $limit
                ]
            ]);
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents())['hits']['hits'];
            if(!$result) { return null; }
            return $this->ranking->content($result);
        }catch(\Exception $ex){
        }

    }

}