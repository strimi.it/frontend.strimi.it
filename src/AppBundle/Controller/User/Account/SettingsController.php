<?php

namespace AppBundle\Controller\User\Account;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SettingsController extends Controller {

    /**
     * @param $username
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($username) {
        if(!$username) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $account = $this->get('api.users.account')->get($username);

        return $this->render(
            'AppBundle:User/Account:index.html.twig',array(
                'account' => $account
            )
        );

    }



}
