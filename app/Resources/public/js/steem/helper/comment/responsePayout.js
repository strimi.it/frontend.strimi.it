strimi_app.responsePayout = function () {
    var payout = document.getElementsByClassName("comment-payout-response")[0];
    if(payout) {
        var value = payout.options[payout.selectedIndex].value;
        return strimi_app.payoutType(value);
    }
};
