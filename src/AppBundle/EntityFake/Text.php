<?php

namespace AppBundle\EntityFake;

class Text {
    public $title;
    public $description;
    public $tags;
    public $payout;
    public $nsfw;
}