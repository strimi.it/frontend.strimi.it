strimi.postNewLinkSubmit = function () {
    var loading = function () {
        var content = document.getElementsByClassName('page-content-new-link-form-submit')[0];
        var url = content.getAttribute('data-url');
        $.ajax({
            type: 'GET',
            url: url,
            async: true,
            beforeSend: function () {
            },
            success: function (response) {
                if (response.code == 200) {
                    content.innerHTML = response.data.form;
                    strimi.language();
                    submit();
                } else if (response.code == 400) {
                }
            },
            error: function () {
            }
        });
    };
    loading();

    var submit = function () {

        $("#formLinkSubmit").submit(function(e){
            e.preventDefault();
            var contentLinkNew = document.getElementsByClassName('page-content-new')[0];
            var submitButton = document.getElementById('preview-link-submit');
            var loadingButton = document.getElementById('preview-link-submit-loading');
            if(submitButton.getAttribute('data-status') === '0'){
                submitButton.setAttribute('data-status', 1);
                $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                beforeSend: function() {
                    loadingButton.classList.remove('hidden');
                    submitButton.className += ' active-loading';
                },
                success: function(response) {
                    if(response.code == 200) {

                        contentLinkNew.className += ' hidden';
                        var previewContent = document.getElementsByClassName('page-content-preview')[0];
                            previewContent.className += ' active';
                            previewContent.innerHTML = response.data.form_preview;

                        loadingButton.className += ' hidden';
                        submitButton.classList.remove('active-loading');

                        strimi.imageLazyload();
                        strimi.tagAutocomplete();
                        strimi.language();
                        strimi.inputLimitText();
                        strimi.autosize();
                        strimi_app.postPreview(response.data.form_preview_data);
                        strimi_app.postLinkSubmit();
                        submitButton.setAttribute('data-status', 0);
                    } else if(response.code == 400){
                        submitButton.setAttribute('data-status', 0);
                    }
                },
                error: function() {
                }
            });
            }
            e.preventDefault();
        });

    };
    submit();
};
if(_config.route === 'app_post_new') {
    strimi.postNewLinkSubmit();
}
