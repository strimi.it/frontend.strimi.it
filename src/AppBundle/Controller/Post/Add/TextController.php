<?php

namespace AppBundle\Controller\Post\Add;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TextController extends Controller {

    /**
     * @param Request $request
     * @return mixed
     */
    public function renderFormTextAction(Request $request){
        if(!$request->isXmlHttpRequest()) { die(); };
        return $this->get('app.json.response')->renderDefault(
            array(
                'form' => $this->get('app.content.new.text')->renderFormSubmitText()
            ),
            200
        );
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function formSubmitLinkAction(Request $request){
        if(!$request->isXmlHttpRequest()) { die(); };

        $form = $this->get('app.content.new.text')->submitFormSubmitLink(
            $this->get('app.content.new.text')->createFormSubmit(),
            $request
        );

        $data = array(
            'form' => $this->renderView(
                'AppBundle:Post/Add/Text:form.html.twig',
                array(
                    'form' => $form['form']->createView()
                )
            ),
            'form_preview' => $form['form_preview'],
            'form_preview_data' => json_encode($form['parser'])
        );

        return $this->get('app.json.response')->renderDefault(
            $data,
            $form['code']
        );

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        return $this->render(
            'AppBundle:Post/Add/Text:index.html.twig'
        );
    }

}
