strimi.remarkable = function () {

    var defaultRemarkable = new Remarkable('full', {
        html:         false,        // Enable HTML tags in source
        xhtmlOut:     false,        // Use '/' to close single tags (<br />)
        breaks:       true,        // Convert '\n' in paragraphs into <br>
        langPrefix:   'language-',  // CSS language prefix for fenced blocks
        linkify:      false,         // autoconvert URL-like texts to links
        linkTarget:   '',           // set target to open link in
        typographer:  false,
        quotes: '“”‘’',
        highlight: function (str, lang) {
            if (lang && hljs.getLanguage(lang)) {
                try {
                    return hljs.highlight(lang, str).value;
                } catch (__) {}
            }
            try {
                return hljs.highlightAuto(str).value;
            } catch (__) {}

            return '';
        }
    });

    defaultRemarkable.block.ruler.enable([
        'blockquote',
        'code',
        'fences',
        //'heading',
        'hr',
        'htmlblock',
        'lheading',
        'list',
        'paragraph'
    ]);

    defaultRemarkable.block.ruler.disable([
        'heading'
    ]);

    $('.markdown-content').on('input', function() {
        var value = $(this).val();
        var previewData = $(this).attr('data-markdown-preview-content');
        document.getElementById(previewData).innerHTML = defaultRemarkable.render(value);
    });

};

strimi.remarkable();