strimi.searchSidebarTags = function () {

    var url = function () {
        var uri = document.getElementsByClassName('sidebar-content-tags-search')[0].getAttribute('data-url');
            return uri.replace("strimi", "");
    };

    $(".suggest_tags_autocomplete").on("change keyup paste", function(){

        var name = $(this).val();
        var nameLength = name.length;
        var conent = document.getElementsByClassName('sidebar-content-tags-content')[0];
        var contentSearch = document.getElementsByClassName('sidebar-content-tags-search-content')[0];

        if(nameLength > 0){
            if(!conent.classList.contains('hidden') && !contentSearch.classList.contains('active')){
                conent.className += ' hidden';
                contentSearch.className += ' active';
            }
            $.ajax({
                type: 'GET',
                url: url() + $(this).val(),
                async: true,
                beforeSend: function() {},
                success: function(getResponse) {
                    if (getResponse.code == 200) {
                        var content = document.getElementsByClassName('sidebar-content-tags-search-content')[0];
                        content.innerHTML = getResponse.data;

                    } else if (getResponse.code == 400) {
                    }
                },
                error: function() {
                }
            });
        } else if (nameLength === 0){
            conent.classList.remove('hidden');
            contentSearch.classList.remove('active');
            contentSearch.innerHTML = ''
        }
    });
};
strimi.searchSidebarTags();