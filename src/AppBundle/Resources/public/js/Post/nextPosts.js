strimi.nextPosts = function () {
    var nextButton = document.getElementById('posts-next-button');
    var loadingButton = document.getElementsByClassName('loading-oval-button')[0];
    if(nextButton){
        nextButton.onclick = function(){
            var status = nextButton.getAttribute('data-status');
            if(status == 0){
                $.ajax({
                    type: 'GET',
                    url: nextButton.getAttribute('data-url'),
                    async: true,
                    beforeSend: function() {
                        nextButton.setAttribute('data-status', 1);
                        nextButton.className += ' active';
                        loadingButton.classList.remove('hidden');
                    },
                    success: function(getResponse) {
                        if (getResponse.code == 200) {
                            var lastPost = document.getElementsByClassName('post-single');
                            lastPost = lastPost[lastPost.length - 1];
                            lastPost.insertAdjacentHTML('afterEnd', getResponse.data.posts);
                            nextButton.setAttribute('data-url', getResponse.data.next_url);

                            strimi.imageLazyload();
                            strimi.momentTimezone();
                            strimi.commentsCount();
                            strimi.language();

                            strimi_app.reloaded();

                        } else if (getResponse.code == 400) {
                        }

                        nextButton.setAttribute('data-status', 0);
                        nextButton.classList.remove('active');
                        loadingButton.classList.className += ' hidden';

                    },
                    error: function() {
                    }
                });
            }
        };
    }
};

var routeList = [ 
    'app_homepage',
    'app_created',
    'app_hot',
    'app_promoted',
    'app_posts_by_tag_trending',
    'app_posts_by_tag_created',
    'app_posts_by_tag_hot',
    'app_posts_by_tag_promoted',
    'stream_homepage',
    'user_account',
    'user_account_feed'
];
if(routeList.includes(_config.route)) {
    strimi.nextPosts();
}