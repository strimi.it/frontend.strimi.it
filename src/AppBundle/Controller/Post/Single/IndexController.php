<?php

namespace AppBundle\Controller\Post\Single;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller {

    /**
     * @param $category
     * @param $author
     * @param $permlink
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($category, $author, $permlink) {

        if(!$category || !$author || !$permlink) {
            return $this->redirect($this->generateUrl('app_homepage' ), 301);
        }
        $post = $this->get('api.post.single')->get($author, $permlink);
        if(empty($post)) {
            return $this->redirect($this->generateUrl('app_homepage' ), 301);
        }

        if($post['category'] === 'strimi-stream') {
            return $this->redirect($this->generateUrl('stream_homepage' ), 301);
        }

        $account = $this->get('api.users.account')->get($author);
        return $this->render(
            'AppBundle:Post/Single:index.html.twig', array(
                'post'=> $post,
                'account' => $account,
                'tag_name' => $category
            )
        );
    }

}
