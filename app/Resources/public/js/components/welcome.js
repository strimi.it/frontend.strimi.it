strimi.welcome = function () {
    var name = 'welcome_banner';
    var status = strimi.readCookie(name),
        banner = document.getElementsByClassName('landingPage')[0],
        wrapper = document.getElementsByClassName('wrapper')[0];
    if(banner && (status === null || status === false) ) {
        strimi.createCookie(name, true, 365);
        banner.className += ' active';
        wrapper.className += ' clear-padding';
    }
    var closeWelcome = document.getElementById('closeWelcome');
    if(closeWelcome) {
        closeWelcome.addEventListener('click', function(){
            strimi.createCookie(name, false, 365);
            banner.classList.remove('active');
            wrapper.classList.remove('clear-padding');
        }, false);
    }
};
strimi.welcome();