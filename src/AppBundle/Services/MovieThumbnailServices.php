<?php

namespace AppBundle\Services;
use Sunra\PhpSimple\HtmlDomParser;

class MovieThumbnailServices {

    private $container;
    protected $entityManager;


    public function statusCode($getUrl) {
        if(isset($getUrl) && !empty($getUrl)) {
            $getHeaders = get_headers($getUrl);
            $getHeaders = $getHeaders[0];
            if($getHeaders == 'HTTP/1.1 200 OK' ||$getHeaders == 'HTTP/1.0 200 OK') {
                return true;
            }
        } else {
            return false;
        }
    }

    public function getContent($getUrl){
        if(empty($getUrl)) {
            return false;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $getUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "spider");
        $getContent = curl_exec($ch);
        curl_close($ch);
        return $getContent;
    }

    public function domParser($getUrl){
        if(empty($getUrl)) {
            return false;
        }
        $getContent = $this->getContent($getUrl);
        return HtmlDomParser::str_get_html($getContent);
    }

    public function getBestYoutubeThumbnail($getHash) {
        if(isset($getHash) && !empty($getHash)) {

            /* YouTube Api */
            $api = 'https://img.youtube.com/vi/' . $getHash . '/';

            /* Quality List */
            $max    = 'maxresdefault.jpg';  // 1280x720
            $sd     = 'sddefault.jpg';      // 640x480
            $hq     = 'hqdefault.jpg';      // 480x360
            $mq     = 'mqdefault.jpg';      // 320x180
            $def    = 'default.jpg';        // 120x90

            if(($this->statusCode($api . $max)) == true) {
                return $api . $max;
            } elseif(($this->statusCode($api . $sd)) == true) {
                return $api . $sd;
            } elseif(($this->statusCode($api . $hq)) == true) {
                return $api . $hq;
            } elseif(($this->statusCode($api . $mq)) == true) {
                return $api . $mq;
            } elseif(($this->statusCode($api . $def)) == true) {
                return $api . $def;
            } else {
                return false;
            }
        }
    }

    public function getBestVimeoThumbnail($getHash) {
        if(isset($getHash) && !empty($getHash)) {

            /* Vimeo Api */
            $api = 'https://vimeo.com/api/v2/video/' . $getHash . '.json';

            $large  = 'thumbnail_large';  //640x360
            $medium = 'thumbnail_medium'; //250x150
            $small  = 'thumbnail_small';  //100x75

            if($this->statusCode($api) == true) {

                $getJson = file_get_contents($api);
                $getJsonDecode = json_decode($getJson, true);
                $getJsonDecode = $getJsonDecode[0];

                if(array_key_exists($large, $getJsonDecode)) {
                    return $getJsonDecode[$large];
                } elseif(array_key_exists($medium, $getJsonDecode)) {
                    return $getJsonDecode[$medium];
                } elseif(array_key_exists($small, $getJsonDecode)) {
                    return $getJsonDecode[$small];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function getBestFacebookMovieThumbnail($getHash) {
        if (isset($getHash) && !empty($getHash)) {

            /* Facebook Movie Api */
            $api = 'https://graph.facebook.com/' . $getHash . '/picture';
            $ch = curl_init($api);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $html = curl_exec($ch);
            $redirectURL = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL );
            curl_close($ch);

            if ($redirectURL) {
                return $redirectURL;
            }
            return false;
        }
    }

    public function getOgImage($domParser, $getUrl){
        if(empty($domParser) || empty($getUrl)) {
            return false;
        }
        $getImage = $domParser->find("meta[property='og:image']", 0);
        if(empty($getImage)) {
            return null;
        }
        $validateImage = $this->container->get('image.validation')->validationSize($getImage->content);
        if($validateImage == true) {
            return trim($getImage->content);
        }
        return null;
    }

    public function getLiveleakThumbnail($getUrl) {
        if(empty($getUrl)) {
            return false;
        }

        /* Active Parser */
        $domParser = $this->domParser($getUrl);
        return $this->getOgImage($domParser, $getUrl);
    }

    public function getDailymotionThumbnail($getUrl) {
        if(isset($getUrl) && !empty($getUrl)) {
            if(($this->statusCode($getUrl)) == true) {
                /* Active Parser */
                $domParser = $this->domParser($getUrl);
                return $this->getOgImage($domParser, $getUrl);
            } else {
                return false;
            }
        }
    }

    public function getSoundcloudThumbnail($getUrl) {
        if(isset($getUrl) && !empty($getUrl)) {

            /* Active Parser */
            $domParser = $this->domParser($getUrl);
            return $this->getOgImage($domParser, $getUrl);
/*
            return $this->getOgImage(
                $this->container->get('movie_service')->getHTTPSCurl($getUrl)
            );
*/
        }
    }

    public function getGyfcatThumbnail($getUrl) {
        if(isset($getUrl) && !empty($getUrl)) {

            /* Active Parser */
            $domParser = $this->domParser($getUrl);
            return $this->getOgImage($domParser, $getUrl);
/*
            return $this->getOgImage(
                $this->container->get('movie_service')->getHTTPSCurl($getUrl)
            );
*/
        }
    }

    public function getTwitchThumbnail($getUrl) {
        if(isset($getUrl) && !empty($getUrl)) {

            /* Active Parser */
            $domParser = $this->domParser($getUrl);
            return $this->getOgImage($domParser, $getUrl);
        }
    }

    public function getThumbnailUrl($getHash, $getType) {

        if(empty($getHash) || empty($getType)) {
            return false;
        }

        $imageUrl = false;
        if($getType == 'youtube') {
            $imageUrl = $this->getBestYoutubeThumbnail($getHash);
        } elseif($getType == 'vimeo') {
            $imageUrl = $this->getBestVimeoThumbnail($getHash);
        } elseif($getType == 'facebook') {
            $imageUrl = $this->getBestFacebookMovieThumbnail($getHash);
        } elseif($getType == 'liveleak') {
            $imageUrl = $this->getLiveleakThumbnail($getHash);
        } elseif($getType == 'dailymotion') {
            $imageUrl = $this->getDailymotionThumbnail($getHash);
        } elseif($getType == 'soundcloud') {
            $imageUrl = $this->getSoundcloudThumbnail($getHash);
        } elseif($getType == 'gfycat') {
            $imageUrl = $getHash;
        } elseif($getType == 'twitch') {
            $imageUrl = $this->getTwitchThumbnail($getHash);
        }

        if(empty($imageUrl)){
            return false;
        }

        return $imageUrl;
    }

}