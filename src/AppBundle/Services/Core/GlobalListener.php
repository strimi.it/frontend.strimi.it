<?php

namespace AppBundle\Services\Core;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class GlobalListener {

    protected $requestStack;
    public function __construct(RequestStack $requestStack){
        $this->requestStack = $requestStack;
    }

    public function onKernelResponse(FilterResponseEvent $event) {
        $responseHeaders = $event->getResponse()->headers;
        $responseHeaders->set('Access-Control-Allow-Methods', 'POST, GET');
        $responseHeaders->set('Strict-Transport-Security', 'max-age=15552000; includeSubDomains;');
    }

}