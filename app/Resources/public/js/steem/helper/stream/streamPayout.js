strimi_app.streamPayout = function () {
    var payout = document.getElementsByClassName("stream-payout")[0];
    if(payout) {
        var value = payout.options[payout.selectedIndex].value;
        return strimi_app.payoutType(value);
    }
};
