strimi.aesSet = function (msg, key) {
    return CryptoJS.AES.encrypt(msg, key);
};
strimi.aesGet = function (ciphertext, key) {
    return CryptoJS.AES.decrypt(ciphertext.toString(), key).toString(CryptoJS.enc.Utf8);
};

// var g = strimi.aesSet('strimi hehehehe', '123');
// var h = strimi.aesGet(g, '123');