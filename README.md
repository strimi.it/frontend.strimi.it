Strimi
========================

Strimi is the web interface to the world's first blockchain-based social media platform, strimi.it. It uses STEEM, a blockchain powered by Graphene 2.0 technology to store JSON-based content for a plethora of web applications.


What's inside?
--------------
- Symfony3
- HTML Purifier
- Parsedown
- Yarnpkg
- Gulpjs

Requirements
--------------
- php >= 5.6
- memcached
- varnish
- login & key for api.strimi.it

Installation
--------------
- php composer install
- npm install
- yarn install
- npm install gulp -g
- gulp make_dev

Issues
--------------
To report a non-critical issue, please file an issue on [this gitlab project](https://gitlab.com/strimi.it/frontend.strimi.it/issues) or report to [strimi.freshdesk.com](https://strimi.freshdesk.com)

No Support & No Warranty
--------------

The software is provided "as is", without warranty of any kind, express or
implied, including but not limited to the warranties of merchantability,
fitness for a particular purpose and noninfringement. In no event shall the
authors or copyright holders be liable for any claim, damages or other
liability, whether in an action of contract, tort or otherwise, arising
from, out of or in connection with the software or the use or other dealings in the software.