strimi_app.muteAccount = function () {

    if (!strimi_app.is_login()) { return; }

    var account = strimi_app.authorization();
    var username = account.username,
        wif = account.private_keys.posting_wif;

    var loading = function (submitButton, loadingStatus) {
        var loadingButton = document.getElementById('mute-loading');
        if (loadingStatus === 'start') {
            submitButton.className += ' active-loading';
            loadingButton.classList.remove('hidden');
        } else if (loadingStatus === 'end') {
            submitButton.classList.remove('active-loading');
            loadingButton.className += ' hidden';
        }
    };

    $( ".mute-user" ).click(function() {

        var author = $(this).attr('data-name');
        var status = $(this).attr('data-status');
        var what = false;

        if(status === 1) {
            what = '';
        } else if(status === '0') {
            what = 'ignore';
        }

        var buttonFollow = document.getElementsByClassName('follow-user')[0];
        var spanFollow = document.getElementsByClassName('follow-user-text')[0];
        var buttonMute = document.getElementsByClassName('mute-user')[0];
        var spanMute = document.getElementsByClassName('mute-user-text')[0];
        loading(buttonMute, 'start');

        steem.broadcast.customJson(
            wif, [], [username], 'follow',
            JSON.stringify( ['follow', { follower: username, following: author, what: [what] }] ),
            function(error, result) {
                if (result && result !== 'undefined') {

                    if(status === '1') {
                        buttonMute.setAttribute('data-status', 0);
                        buttonMute.classList.remove('active');
                        spanMute.innerHTML = strimi.languageText( spanMute.getAttribute('data-mute') );
                    } else if(status === '0') {
                        buttonMute.setAttribute('data-status', 1);
                        spanMute.innerHTML = strimi.languageText( spanMute.getAttribute('data-unmute') );
                        buttonMute.className += ' active';
                        if(buttonFollow.getAttribute('data-status') === '1') {
                            buttonFollow.setAttribute('data-status', 0);
                            buttonFollow.classList.remove('active');
                            spanFollow.innerHTML = strimi.languageText( spanFollow.getAttribute('data-follow') );
                        }
                    }
                    console.log(result);
                } else if (error && error !== 'undefined') {
                    console.log(error);
                }
                loading(buttonMute, 'end');
            }
        );

    });

};

var routeList = [
    'user_account', 'user_account_feed', 'user_account_comments',
    'user_account_replies', 'user_account_votes', 'user_account_followers',
    'user_account_following', 'user_account_transfers', 'user_account_settings',
    'stream_post', 'app_post'
];
if(routeList.includes(_config.route) && strimi_app.is_login()) {
    strimi_app.muteAccount();
}

