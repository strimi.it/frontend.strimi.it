<?php

namespace AppBundle\Services;
use Symfony\Component\HttpFoundation\RequestStack;

class PackageJSON {

    protected $requestStack;
    public function __construct(RequestStack $requestStack){
        $this->requestStack = $requestStack;
    }

    public function version(){
        $root = $_SERVER['CONTEXT_DOCUMENT_ROOT'];
        $getJson = file_get_contents(substr($root, 0, strrpos( $root, '/')) . '/' . 'package.json');
        if(!$getJson) {
            return false;
        }
        $getObject = json_decode($getJson);
        if(!$getObject) {
            return false;
        }
        return $getObject->version;
    }

}