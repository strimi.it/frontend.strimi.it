<?php

namespace AppBundle\Services;

class formatBytes {

    public function format($size, $precision = 2){
        if(!$size) { return null; }
        $base = log($size, 1024);
        $suffixes = array('', 'KB', 'MB', 'GB', 'TB');
        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }

    public function formatBandwidth($size){
        if(!$size) { return null; }
        $base = log($size, 1024);
        $precision = 2;
        $suffixes = array('', 'KB', 'MB', 'GB', 'TB');
        $round = round(pow(1024, $base - floor($base)), $precision);
        $key = floor($base);
        if (floor($base) <= 0 ){
            $key = 0;
        }
        $result = $round . $suffixes[$key];
        return $result;
    }

}