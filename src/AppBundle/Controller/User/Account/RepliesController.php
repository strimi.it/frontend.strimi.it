<?php

namespace AppBundle\Controller\User\Account;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RepliesController extends Controller {

    /**
     * @param $username
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($username) {
        if(!$username) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $account = $this->get('api.users.account')->get($username);
        if(!$account) { return $this->redirect($this->generateUrl('app_homepage'), 301); };
        $comments = $this->get('api.users.account.replies')->get($account['name'], null);
        return $this->render(
            'AppBundle:User/Account:index.html.twig',array(
                'account' => $account,
                'comments'   => $comments
            )
        );
    }

    /**
     * @param $account
     * @param $author
     * @param $permlink
     * @param Request $request
     * @return null
     */
    public function nextCommentsAction($account, $author, $permlink, Request $request) {
        if(!$account || !$author || !$permlink) { return null; }

        $code = 400;
        $comments = $this->get('api.users.account.replies')->replies($author, $permlink, 10);
        $appRoute = $request->get('_route');
        $commentsContent = null;
        $nextUri = null;

        if($comments) {
            $code = 200;
            $lastComments =  end($comments);
            $nextUri = $this->generateUrl('user_account_ajax_replies_next', array(
                'account'  => $author,
                'author'   => $lastComments['author'],
                'permlink' => $lastComments['permlink']
            ));

            $commentsContent = $this->renderView(
                'AppBundle:User/Account/Comments:commentsLists.html.twig',
                array(
                    'comments' => $comments,
                    'appRoute' => $appRoute
                )
            );

        }

        return $this->get('app.json.response')->renderDefault(
            array(
                'comments'    => $commentsContent,
                'next_url' => $nextUri
            ),
            $code
        );
    }

}
