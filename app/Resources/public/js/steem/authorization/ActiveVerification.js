strimi_app.ActiveVerification = function (key) {

    if(!strimi_app.is_login()) { return ''; }
    if(!key) { return ''; }

    var errorView = function (errorText) {
        var content = document.getElementsByClassName('alert-active-verification-content')[0];
        var alert = document.getElementById('alert-active-verification');
        content.className += ' active';
        alert.innerText = errorText;
        return '';
    };

    var account = strimi_app.authorization();
    var username = account.username;
    var roleType = strimi_app.keyRoles[1];
    var isWif = strimi_app.auth.isWif(key);
    var isMaster;
    var setActiveKey;
    var isValid;

    if(!isWif) {
        //Master
        var toActive = strimi_app.auth.toWif(username, key, roleType);
        isValid = strimi_app.auth.wifIsValid(toActive, account.public_keys.active_wif);
        isMaster = true;
    } else {
        //Active
        var toActivePublic = strimi_app.auth.wifToPublic(key);
       isValid = strimi_app.auth.wifIsValid(key, toActivePublic);
    }

    if(!isValid) {
        errorView(strimi.languageText('global.user.authorization.alert.0'));
        return false;
    }

    if(isMaster) {
        setActiveKey = strimi_app.auth.getPrivateKeys(username, key, [roleType]);
        setActiveKey = setActiveKey.active;
    } else {
        setActiveKey = key;
    }
    return setActiveKey;
};