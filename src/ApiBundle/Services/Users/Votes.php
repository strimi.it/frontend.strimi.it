<?php

namespace ApiBundle\Services\Users;

class Votes {

    protected $json_parser;
    protected $api_connection;
    protected $defaults;
    protected $paginator;
    protected $memcached;
    public function __construct($jsonParser, $apiConnection, $defaults, $paginator, $memcached){
        $this->json_parser = $jsonParser;
        $this->api_connection = $apiConnection;
        $this->defaults = $defaults;
        $this->paginator = $paginator;
        $this->memcached = $memcached;
    }

    /**
     * @param $authorperm
     * @return array|null
     */
    private function authorperm($authorperm){
        if(!$authorperm) { return null; }
        $authorperm = explode('/', $authorperm);
        return [
            'author' => $authorperm[0],
            'permlink' => $authorperm[1]
        ];
    }

    /**
     * @param $name
     * @return array|null
     */
    private function votes($name){
        if(!$name) { return null; }
        try{
            $response = $this->api_connection->init()->request('GET', 'users/' . $name . '/votes');
            if($response->getStatusCode() != 200) { return null; }
            $result = $this->json_parser->singleObject($response->getBody()->getContents());
            if(!$result) { return null; }
            $votes = $result['result'];
            foreach($votes as &$vote) {
                $vote['percent'] =  $this->defaults->votePercent($vote, true);
                $vote['authorperm'] = $this->authorperm($vote['authorperm']);
            }
            return array_reverse($votes);
        }catch(\Exception $ex){
            //Process the exception
        }
    }

    /**
     * @param $key
     * @param $account
     * @return null
     */
    private function contentCache($key, $account){
        if(!$key || !$account) { return null; }
        $votes = $this->memcached->get($key);
        if(!$votes) {
            $value = $this->votes($account);
            $this->memcached->set($key, $value, 1, 'minutes');
        }
        return $this->memcached->get($key);
    }

    /**
     * @param $name
     * @return null
     */
    public function get($name){
        if(!$name) { return null; }
        return $this->paginator->getPackage(
            $this->contentCache('account_' . md5($name) . '_votes', $name)
        );
    }

    /**
     * @param $name
     * @param $package
     * @return null
     */
    public function getNext($name, $package){
        if(!$name || !$package) { return null; }
        return $this->paginator->getPackageNext(
            $this->contentCache('account_' . md5($name . $package) . '_votes_package', $name), $package
        );
    }

}