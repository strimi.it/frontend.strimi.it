<?php

namespace AppBundle\EntityFake;

class Link {
    public $link;
    public $title;
    public $description;
    public $tags;
    public $payout;
    public $nsfw;
}