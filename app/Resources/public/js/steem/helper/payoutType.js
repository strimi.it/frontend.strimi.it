strimi_app.payoutType = function (type) {
  if(!type) { return false };
  var payout = [];
  if(type === 'default'){
      payout = [
          '1000000.000 SBD', //max_accepted_payout
          10000 //percent_steem_dollars
      ];
  } else if(type === 'steem_power') {
      payout = [
          '1000000.000 SBD',
          0
      ];
  } else if(type === 'decline') {
      payout = [
          '0.000 SBD',
          10000
      ];
  }
  return payout;
};