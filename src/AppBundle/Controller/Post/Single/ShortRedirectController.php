<?php

namespace AppBundle\Controller\Post\Single;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ShortRedirectController extends Controller {

    /**
     * @param $author
     * @param $permlink
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($author, $permlink) {
        if(!$author || !$permlink) {
            return $this->redirect($this->generateUrl('app_homepage'), 301);
        }
        $post = $this->get('api.post.single')->get($author, $permlink);
        if(!empty($post) && !empty($post['root_url_parser'])) {
            $root_url_parser = $post['root_url_parser'];
            $generateUrl = $this->generateUrl('app_post', array(
                'category' => $root_url_parser['category'],
                'author'   => $root_url_parser['author'],
                'permlink' => $root_url_parser['permlink']
            ));
            return $this->redirect($generateUrl, 301);
        }
        return $this->redirect($this->generateUrl('app_homepage'), 301);
    }

}
